//
//  Regex.swift
//  zk
//
//  Created by Jeff Younker on 1/31/20.
//  Copyright © 2020 The Blobshop. All rights reserved.
//

import Foundation

extension NSRegularExpression {
    func capturedStrings(in text: String, range: NSRange) -> Array<String> {
        return capturedSubstrings(in: text, range: range).map{ String($0) }
    }

    func capturedSubstrings(in text: String, range: NSRange) -> Array<Substring> {
        return capturedRanges(in: text, range: range).map{ text[$0] }
    }

    func capturedRanges(in text: String, range: NSRange) -> Array<Range<String.Index>> {
        return capturedNSRanges(in: text, range: range).map{ Range($0, in: text)! }
    }

    func capturedNSRanges(in text: String, range: NSRange) -> Array<NSRange> {
        var ranges = Array<NSRange>()
        for m in self.matches(in: text, options: [], range: range) {
            for i in 1..<m.numberOfRanges {
                ranges.append(m.range(at: i))
            }
        }
        return ranges
    }
}
