//
//  ReviewEngine.swift
//  zk
//
//  Created by Jeff Younker on 1/11/20.
//  Copyright © 2020 The Blobshop. All rights reserved.
//

import CoreData
import Foundation

import Logging

class ReviewEngine {
    static let log = Logger(label: "com.theblobshop.zk.review.review-engine")
}

// MARK: - Selectors and summary
extension ReviewEngine {
    static func reviewSummary(
        context: NSManagedObjectContext, at reviewTime: Timestamp) throws -> ReviewSummary {
        let new = try ReviewEngine.countNew(context: context)
        let learning = try countLearning(context: context, at: reviewTime)
        let reviewable = try totalReviewable(context: context)
        let learned = max(reviewable - new - learning, 0)
        return ReviewSummary(new: UInt(new), learning: UInt(learning), learned: UInt(learned))
    }

    static func countNew(context: NSManagedObjectContext) throws -> Int {
        return try context.count(for: selectNewQuery())
    }

    static func selectNew(context: NSManagedObjectContext) throws -> [Zettel] {
        return try context.fetch(selectNewQuery())
    }

    static func totalReviewable(context: NSManagedObjectContext) throws -> Int {
        let req: NSFetchRequest<Zettel> = Zettel.fetchRequest()
        req.predicate = NSPredicate(format: "ANY tags.name == %@", "review")
        req.returnsObjectsAsFaults = false
        return try context.count(for: req)
    }

    static func countLearning(
        context c: NSManagedObjectContext,
        at reviewTime: Timestamp) throws -> Int {
        return try c.count(for: selectLearningQuery(at: reviewTime))
    }

    static func selectLearning(
        context: NSManagedObjectContext, at reviewTime: Timestamp) throws -> [ReviewTime] {
        let req = selectLearningQuery(at: reviewTime)
        return try context.fetch(req).map() {
            ReviewTime(at: $0.at, zid: $0.zid!) }
    }

    static func selectLearned(
        context: NSManagedObjectContext, at reviewTime: Timestamp) throws -> [ReviewTime] {
        let req = selectLearnedQuery(at: reviewTime)
        return try context.fetch(req).map() {
            ReviewTime(at: $0.at, zid: $0.zid!) }
    }

    static func selectNewQuery() -> NSFetchRequest<Zettel> {
        let req: NSFetchRequest<Zettel> = Zettel.fetchRequest()
        req.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [
            NSPredicate(format: "ANY tags.name == %@", "review"),
            NSPredicate(format: "next_review == nil"),
        ])
        req.returnsObjectsAsFaults = false
        return req
    }

    static func selectLearningQuery(at reviewTime: Timestamp) -> NSFetchRequest<NextReview> {
        let req: NSFetchRequest<NextReview> = NextReview.fetchRequest()
        req.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [
            NSPredicate(format: "next_review_time <= %d", reviewTime),
            NSPredicate(format: "(%@ IN zettel.tags.name)", "review"),
        ])
        req.sortDescriptors = [NSSortDescriptor(key: "next_review_time", ascending: true)]
        req.returnsObjectsAsFaults = false
        return req
    }

    static func selectLearnedQuery(at reviewTime: Timestamp) -> NSFetchRequest<NextReview> {
        let req: NSFetchRequest<NextReview> = NextReview.fetchRequest()
        req.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [
            NSPredicate(format: "next_review_time > %d", reviewTime),
            NSPredicate(format: "(%@ IN zettel.tags.name)", "review"),
        ])
        req.sortDescriptors = [NSSortDescriptor(key: "next_review_time", ascending: true)]
        req.returnsObjectsAsFaults = false
        return req
    }
}

struct ReviewTime: Hashable {
    let at: Timestamp
    let zid: Zid
}

enum ReviewSessionState {
    case sessionActive
    case noSession
}

extension ReviewEngine {
    static func selectReviewRecords(
        context c: NSManagedObjectContext,
        at reviewTime: Timestamp,
        limit n: Int) throws -> [ReviewRecord] {
        assert(n >= 0)
        let new = try ReviewEngine.selectNextNew(context: c, limit: n)
        let learning = try ReviewEngine.selectNextLearning(
            context: c, at: reviewTime, limit: n)
        return take(n, from: zip(new, learning))
    }

    static func selectNextNew(
        context: NSManagedObjectContext,
        limit n: Int) throws -> [ReviewRecord] {
        assert(n >= 0)
        let zetteln = try ReviewEngine.selectNew(
            context: context)
        return zetteln.map{ ReviewRecord.new(zettel: $0) }
    }

    static func selectNextLearning(
        context: NSManagedObjectContext,
        at reviewTime: Timestamp,
        limit n: Int) throws -> [ReviewRecord] {
        assert(n >= 0)
        let reviewTimes = try ReviewEngine.selectLearning(
            context: context, at: reviewTime)
        if reviewTimes.count == 0 {
            return []
        }
        let zetteln = Zettel.select(
            context: context,
            query: ZettelQuery(zids: reviewTimes.map{ $0.zid }))
        return zetteln.map{ ReviewRecord.learning(zettel: $0) }
    }
}

enum ReviewRecord: Hashable {
    case new(zettel: Zettel)
    case learning(zettel: Zettel)
    
    func zettel() -> Zettel {
        switch self {
        case .new(let z): return z
        case .learning(let z): return z
        }
    }
}

@objc class ReviewSummary: NSObject {
    let new: UInt
    let learning: UInt
    let learned: UInt
    
    init(new: UInt, learning: UInt, learned: UInt) {
        self.new = new
        self.learning = learning
        self.learned = learned
    }
    
    static func == (lhs: ReviewSummary, rhs: ReviewSummary) -> Bool {
        return lhs.new == rhs.new && lhs.learning == rhs.learning && lhs.learned == rhs.learned
    }
}


enum ReviewResult {
    case learned
    case notLearned
}

enum RetrievalState {
    case showNew
    case showLearning1
    case showFailed
    case showLearning2
    
    static let all: Set<RetrievalState> = [.showNew, .showLearning1, .showFailed, .showLearning2]
}
