//
//  NewCard.swift
//  zk
//
//  Created by Jeff Younker on 2/3/20.
//  Copyright © 2020 The Blobshop. All rights reserved.
//

import CoreData
import Foundation

enum LearningStatus {
    case LEARNED
    case NOT_LEARNED

    func asDouble() -> Double {
        switch self {
        case .LEARNED: return 1.0
        case .NOT_LEARNED: return 0.0
        }
    }
    
    static func from(double: Double) -> LearningStatus {
        if double == 1.0 {
            return .LEARNED
        } else if double == 0.0 {
            return .NOT_LEARNED
        } else {
            assert(false)
            return .LEARNED
        }
    }
}

let leitnerBins: Array<Timestamp> = [
    4 * 3600,
    24 * 3600,
    4 * 24 * 3600,
    8 * 24 * 3600,
    16 * 24 * 3600,
    32 * 24 * 3600,
    64 * 24 * 3600,
    128 * 24 * 3600,
]

func learn(
    context c: NSManagedObjectContext,
    zettel z: Zettel,
    at t: Timestamp,
    outcome: LearningStatus) throws {
    try learn(
        schedule: leitnerBins,
        context: c,
        zettel: z,
        at: t,
        outcome: outcome)
}

func learn(
    schedule s: Array<Timestamp>,
    context c: NSManagedObjectContext,
    zettel z: Zettel,
    at t: Timestamp,
    outcome x: LearningStatus) throws {

    if let nr = z.next_review {
        let currentBin = nr.leitnerBin
        let scheduledTime = nr.at
        
        let re = ReviewEvent(context: c)
        re.zid = z.zid!
        re.zettel = z
        re.at = t
        re.scheduledTime = scheduledTime
        re.leitnerBin = currentBin
        
        nr.lastReviewTime = t
        nr.leitnerBin = nextBin(schedule: s, outcome: x, currentBin: currentBin)
        nr.at = nextTime(schedule: s, outcome: x, currentBin: currentBin, now: t)
    } else {
        let nr = NextReview(context: c)
        nr.zid = z.zid!
        nr.zettel = z
        nr.lastReviewTime = t
        nr.leitnerBin = nextBin(schedule: s, outcome: x, currentBin: 0)
        nr.at = nextTime(schedule: s, outcome: x, currentBin: 0, now: t)
        
        let re = ReviewEvent(context: c)
        re.zid = z.zid!
        re.zettel = z
        re.at = t
        re.scheduledTime = 0
        re.leitnerBin = 0
    }
}

func nextTime(
    schedule s: Array<Timestamp>,
    outcome x: LearningStatus,
    currentBin i: Int,
    now: Timestamp) -> Timestamp {
    switch x {
    case .LEARNED: return now + s[nextBin(schedule: s, outcome: x, currentBin: i)]
    case .NOT_LEARNED: return now
    }
}

func nextBin(
    schedule: Array<Timestamp>,
    outcome: LearningStatus,
    currentBin: Int) -> Int {
    switch outcome {
    case .LEARNED: return min(currentBin + 1, schedule.count - 1)
    case .NOT_LEARNED: return max(currentBin - 1, 0)
    }
}
