//
//  ReviewSession.swift
//  zk
//
//  Created by Jeff Younker on 1/28/20.
//  Copyright © 2020 The Blobshop. All rights reserved.
//

import CoreData
import Foundation

import Logging

func startSession(
    context c: NSManagedObjectContext,
    timekeeper t: Timekeeper,
    limit n: Int) throws -> ReviewSession {
    let records = try ReviewEngine.selectReviewRecords(context: c, at: t.now(), limit: n)
    return ReviewSession(context: c, timekeeper: t, records: records)
}

/// Handles a session. Builds up records.
class ReviewSession {
    let log = Logger(label: "com.theblobshop.zk.ReviewSession")
    let context: NSManagedObjectContext
    let timekeeper: Timekeeper
    static let defaultSize: Int = 15  // In zetteln
    
    // The active session
    var state: SessionState!
    
    // Initial targets
    var reviewing: Queue<SessionState> = Queue()
    
    init(
        context: NSManagedObjectContext,
        timekeeper: Timekeeper,
        records: Array<ReviewRecord>)  {
        self.context = context
        self.timekeeper = timekeeper
        let states = records.flatMap{ SessionState.from(reviewRecord: $0) }.shuffled()
        self.reviewing = Queue(from: states)
        if reviewing.isEmpty {
            state = .ended
            return
        }
        state = reviewing.pop()
    }
    
    /// Triggered when a zettel has been deleted (or made unreviewable)
    func didDelete(zettel: Zettel) {
    }
}

/// MARK: - High Level API

extension ReviewSession {
    /// Skips the current reviewed zettel.
    func reviewerSkips() {
        log.info("STATE \(String(describing: state)) -SKIP->")
        switch state! {
        case .newNotShownQ(_),
             .newNotShownA(_),
             .newNotShownQA(_),
             .reviewQ(_),
             .reviewA(_),
             .reviewQA(_):
            state = reviewing.pop() ?? .ended
        case .ended:
            fatalError("illegal state transition: cannot skip when review is over")
        }
    }

    func reviewerShowsAnswer(at t: Timestamp) {
        log.info("STATE \(String(describing: state)) -SHOWS-ANSWER->")
        switch state! {
        case .newNotShownQ(let card):
            state = .newNotShownA(card: card)
        case .reviewQ(let card):
            state = .reviewA(card: card)
        case .ended:
            fatalError("illegal state transition: cannot skip when review is over")
        default:
            fatalError("illegal state transition: cannot show answer from answer state")
        }
    }
        
    func reviewerHasSeenAnswer(at t: Timestamp) throws {
        log.info("STATE \(String(describing: state)) -HAS-SEEN-ANSWER->")
        switch state! {
        case .newNotShownA(let reviewCard):
            reviewing.push(SessionState.reviewQ(card: reviewCard))
            state = reviewing.pop() ?? .ended
        case .newNotShownQA(let reviewCard):
            reviewing.push(SessionState.reviewQA(card: reviewCard))
            state = reviewing.pop() ?? .ended
        case .ended:
            fatalError("illegal state transition: session has already ended")
        default:
            fatalError("illegal state transition: can only continue from unshown new zettel answer")
        }
    }
    
    /// The user couldn't learn the current zettel. Mark it so and choose the next.
    func reviewerNotLearns(at t: Timestamp) throws {
        log.info("STATE \(String(describing: state)) -NOT-LEARNS->")
        switch state! {
        case .reviewA(let card):
            appDelegate.unitOfWork().execute(with: .reviewedZettel) { c in
                try learn(
                    context: c, zettel: card.zettel.duplicate(into: c), at: t, outcome: .NOT_LEARNED)
            }
            state = reviewing.pop() ?? .ended
            reviewing.push(.reviewQ(card: card))
        case .reviewQA(let card):
            appDelegate.unitOfWork().execute(with: .reviewedZettel) { c in
                try learn(
                    context: c, zettel: card.zettel.duplicate(into: c), at: t, outcome: .NOT_LEARNED)
            }
            state = reviewing.pop() ?? .ended
            reviewing.push(.reviewQA(card: card))
        case .ended:
            fatalError("illegal state transition: sreview has ended")
        default:
            fatalError("illegal state trasition: can only learn in answer state")
        }
    }
    
    /// The user learned the current zettel. Mark it so and choose the next.
    func reviewerLearns(at t: Timestamp) throws {
        log.info("STATE \(String(describing: state)) -LEARNS->")
        switch state! {
        case .reviewA(let card), .reviewQA(let card):
            appDelegate.unitOfWork().execute(with: .reviewedZettel) { c in
                try learn(
                    context: c, zettel: card.zettel.duplicate(into: c), at: t, outcome: .LEARNED)
            }
            state = reviewing.pop() ?? .ended
        case .ended:
            fatalError("illegal state transision: review has ended")
        default:
            fatalError("illegal state trasition: can only learn in answer state")
        }
    }
    
    /// Forcible end the session.
    func reviwerEnds() throws {
        state = .ended
    }
    
    /// State is accessible
    var card: ReviewCard? {
        get {
            switch state! {
            case .newNotShownQ(let c),
                 .newNotShownA(let c),
                 .newNotShownQA(let c),
//                 .newShownQ(let c, _),
//                 .newShownA(let c, _),
//                 .newShownQA(let c, _),
                 .reviewQ(let c),
                 .reviewA(let c),
                 .reviewQA(let c):
                return c
            case .ended:
                return nil
            }
        }
    }
    
    /// The session is over.
    var hasEnded: Bool {
        get {
            switch state {
            case .ended: return true
            default: return false
            }
        }
    }
}

extension ReviewSession {
    func reviewSummary(timestamp: Timestamp) throws -> ReviewSummary {
        return ReviewSummary(new: 0, learning: 0, learned: 0)
    }
}

extension ReviewSession {
    var  debugDescription: String {
        get {
            return "ReviewSession(state:\(String(describing: state)))"
        }
    }
}

enum SessionState {
    case newNotShownQ(card: ReviewCard)
    case newNotShownA(card: ReviewCard)
    case newNotShownQA(card: ReviewCard)
    case reviewQ(card: ReviewCard)
    case reviewA(card: ReviewCard)
    case reviewQA(card: ReviewCard)
    case ended

    static func from(reviewRecord r: ReviewRecord) -> [SessionState] {
        switch r {
        case .new(let zettel):
            return ReviewCard.from(zettel: zettel).map{
                switch $0 {
                case .single: return .newNotShownQA(card: $0)
                case .double: return .newNotShownQ(card: $0)
                }
            }
        case .learning(let zettel):
            return ReviewCard.from(zettel: zettel).map{
                switch $0 {
                case .single: return .reviewQA(card: $0)
                case .double: return .reviewQ(card: $0)
                }
            }
        }
    }
}
