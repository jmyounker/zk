//
//  NewCard.swift
//  zk
//
//  Created by Jeff Younker on 2/3/20.
//  Copyright © 2020 The Blobshop. All rights reserved.
//

import Foundation

let BEST_DIFFICULTY: Double = 0.9

func learnedNew(zettel: Zettel, failures: Int) {
    assert(failures >= 0)
    let context = zettel.managedObjectContext!
    let zid = zettel.zid!
    let rp = ReviewParams(context: context)
    rp.zid = zid
    rp.alpha = initialAlpha(failures)
    rp.beta = initialBeta(failures)
    rp.n_0 = initialN0(failures)
    rp.q = initialQ(failures)
    zettel.review_params = rp
}

func initialAlpha(_ failures: Int) -> Double {
    return BEST_DIFFICULTY / Double(failures + 1)
}

func initialBeta(_ failures: Int) -> Double {
    return BEST_DIFFICULTY / Double(failures + 1)
}

func initialN0(_ failures: Int) -> Double {
    return BEST_DIFFICULTY / Double(failures + 1)
}

func initialQ(_ failures: Int) -> Double {
    return BEST_DIFFICULTY / Double(failures + 1)
}
