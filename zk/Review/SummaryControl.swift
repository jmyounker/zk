//
//  SummaryControl.swift
//  zk
//
//  Created by Jeff Younker on 1/19/20.
//  Copyright © 2020 The Blobshop. All rights reserved.
//

import Cocoa
import Foundation

protocol SummaryControl {
    var view: NSView { get }
    func update(summary: ReviewSummary)
    func observe<X: NSObject>(target: X, keyPath: KeyPath<X, ReviewSummary>)
}

class TableSummaryControl: BaseSummaryControl {
//    lazy var view: NSView = {
//        new = NSTextField()
//        new.isEditable = false
//
//        learning = NSTextField()
//        learning.isEditable = false
//        learning.textColor = .red
//
//        learned = NSTextField()
//        learned.isEditable = false
//        learning.textColor = .green
//
//        let table = NSGridView(numberOfColumns: 2, rows: 3)
//        table.column(at: 0).xPlacement = .trailing
//
//        table.cell(atColumnIndex: 0, rowIndex: 0).contentView = NSTextField(
//            labelWithString: "new")
//        table.cell(atColumnIndex: 1, rowIndex: 0).contentView = new
//
//        let learningLabel = NSTextField(labelWithString: "learning")
//        learningLabel.textColor = .red
//        table.cell(atColumnIndex: 0, rowIndex: 1).contentView = learningLabel
//        table.cell(atColumnIndex: 1, rowIndex: 1).contentView = learning
//
//        let learnedLabel = NSTextField(labelWithString: "learned")
//        learnedLabel.textColor = .red
//        table.cell(atColumnIndex: 0, rowIndex: 2).contentView = learnedLabel
//        table.cell(atColumnIndex: 1, rowIndex: 2).contentView = learned
//
//        return table
//    }()
    lazy var view: NSView = {
        print("GENERATING VIEW")
        let table = NSGridView(numberOfColumns: 1, rows: 1)
        let label = NSTextField(labelWithString: "foo")
        table.cell(atColumnIndex: 0, rowIndex: 0).contentView = label
        return table
    }()
}

class OneLineSummaryControl: BaseSummaryControl {
    lazy var view: NSView = {
        new = withoutResizeMask(NSTextField())
        new.isEditable = false

        learning = withoutResizeMask(NSTextField())
        learning.isEditable = false
        learning.textColor = .red

        learned = withoutResizeMask(NSTextField())
        learned.isEditable = false
        learning.textColor = .green

        let line = withoutResizeMask(NSStackView())
        line.orientation = .horizontal
        line.alignment = .firstBaseline

        line.addArrangedSubview(new)
        line.addArrangedSubview(withoutResizeMask(NSTextField(labelWithString: "+")))
        line.addArrangedSubview(learning)
        line.addArrangedSubview(withoutResizeMask(NSTextField(labelWithString: "+")))
        line.addArrangedSubview(learned)

        return line
    }()
}

class BaseSummaryControl {
    var new: NSTextField!
    var learning: NSTextField!
    var learned: NSTextField!
    
    func update(summary: ReviewSummary) {
        self.new.stringValue = "\(summary.new)"
        self.learning.stringValue = "\(summary.learning)"
        self.learned.stringValue = "\(summary.learned)"
    }

    func observe<X: NSObject>(target: X, keyPath: KeyPath<X, ReviewSummary>) {
        _ = target.observe(keyPath, options: .new) { target, change in
            self.update(summary: change.newValue ?? ReviewSummary(new: 0, learning: 0, learned: 0))
        }
    }
}

