//
//  NoSessionControlsController.swift
//  zk
//
//  Created by Jeff Younker on 1/17/20.
//  Copyright © 2020 The Blobshop. All rights reserved.
//

import Cocoa
import Foundation

class NoSessionControlsController: NSViewController {
    func createView() {
        let v = withoutResizeMask(View(backgroundColor: NSColor.white))
        let label = withoutResizeMask(NSTextField(labelWithString: "Session"))
        v.addSubview(label)
        center(child: label, within: v)
        self.view = v
    }
}
