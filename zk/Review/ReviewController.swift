//
//  ReviewController.swift
//  zk
//
//  Created by Jeff Younker on 1/7/20.
//  Copyright © 2020 The Blobshop. All rights reserved.
//

import Cocoa
import Foundation

import Logging

class ReviewController: NSViewController {
    let log = Logger(label: "com.theblobshop.zk.ReviewController")
    var screen: Scaler!

    var _state: ReviewPanelState = .noSession
    
    var state: ReviewPanelState {
        get { return _state }
        set(x) { print("\(x)"); _state = x }
    }
    
    @objc dynamic var reviewCard: ObjcReviewCard? = nil
    
    // These are the panels which can contain state-related controls.
    var middlePanel: TabView<ReviewPanelUiState>!
    var bottomPanel: TabView<ReviewPanelUiState>!
    
    func configure(screen: Scaler) {
        self.screen = screen
    }

    
    func nextState() {
        setState(ReviewPanelState.from(session: state.session))
    }
    
    func setState(_ state: ReviewPanelState) {
        self.state = state
        print("==== NEW STATE: \(String(describing: state))")
        reviewCard = ObjcReviewCard.from(card: state.card)
        middlePanel.setTab(index: ReviewPanelUiState.from(panel: state))
        bottomPanel.setTab(index: ReviewPanelUiState.from(panel: state))
        self.view.needsLayout = true
        self.view.needsDisplay = true
    }

    func createView() {
        let view = NSView()
        self.view = view
        
        middlePanel = withoutResizeMask(TabView())
        middlePanel.tabPosition = .none
        middlePanel.tabViewType = .noTabsNoBorder
        
        bottomPanel = withoutResizeMask(TabView())
        bottomPanel.tabPosition = .none
        bottomPanel.tabViewType = .noTabsNoBorder
        
        view.addSubview(middlePanel)
        view.addSubview(bottomPanel)
        
        let metrics = [
            "middleMinX": screen.x(cardSize),
            "middleMinY": screen.y(cardSize),
            "minSpacing": screen.x(5),
        ]
        let views: Dictionary<String, NSView> = [
            "frame": view,
            "middlePanel": middlePanel,
            "bottomPanel": bottomPanel,
        ]

        var constraints = Array<NSLayoutConstraint>()
        constraints += NSLayoutConstraint.constraints(
            withVisualFormat: "H:[middlePanel(>=middleMinX)]",
            options: [],
            metrics: metrics,
            views: views)
        constraints += NSLayoutConstraint.constraints(
            withVisualFormat: "V:[middlePanel(>=middleMinY)]",
            options: [],
            metrics: metrics,
            views: views)
        constraints += NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-[bottomPanel]-|",
            options: [.alignAllCenterY],
            metrics: metrics,
            views: views)
        constraints += NSLayoutConstraint.constraints(
            withVisualFormat: "V:[middlePanel]->=minSpacing-[bottomPanel]-|",
            options: [.alignAllCenterX],
            metrics: metrics,
            views: views)
        view.addConstraints(constraints)
        center(child: middlePanel, within: view)

        createNoSessionViews()
        createReviewingQuestionViews()
        createReviewingAnswerViews()
        createReviewingQuestionAnswerViews()
        createShowingQuestionViews()
        createShowingAnswerViews()
        createShowingQuestionAnswerViews()
        
        middlePanel.setTab(index: .noSession)
    }
    
    func createNoSessionViews() {
        middlePanel.add(index: .noSession, controller: NoSessionStateCenterController())
        bottomPanel.add(index: .noSession, controller: NoSessionStateBottomController())
    }

    func createReviewingQuestionViews() {
        middlePanel.add(index: .reviewingQuestion, controller: ReviewingQuestionStateCenterController(target: self))
        bottomPanel.add(index: .reviewingQuestion, controller: ReviewingQuestionStateBottomController())
    }

    func createReviewingAnswerViews() {
        middlePanel.add(index: .reviewingAnswer, controller: ReviewingAnswerStateCenterController(target: self))
        bottomPanel.add(index: .reviewingAnswer, controller: ReviewingAnswerStateBottomController())
    }

    func createShowingQuestionViews() {
        middlePanel.add(index: .showingQuestion, controller: ShowingQuestionStateCenterController(target: self))
        bottomPanel.add(index: .showingQuestion, controller: ShowingQuestionStateBottomController())
    }

    func createShowingAnswerViews() {
        middlePanel.add(index: .showingAnswer, controller: ShowingAnswerStateCenterController(target: self))
        bottomPanel.add(index: .showingAnswer, controller: ShowingAnswerStateBottomController())
    }
    
    func createShowingQuestionAnswerViews() {
        middlePanel.add(index: .showingQuestionAnswer, controller: ShowingQuestionAnswerStateCenterController(target: self))
        bottomPanel.add(index: .showingQuestionAnswer, controller: ShowingQuestionAnswerStateBottomController())
    }

    func createReviewingQuestionAnswerViews() {
        middlePanel.add(index: .reviewingQuestionAnswer, controller: ReviewingQuestionAnswerStateCenterController(target: self))
        bottomPanel.add(index: .reviewingQuestionAnswer, controller: ReviewingQuestionAnswerStateBottomController())
    }

    override func viewWillLayout() {
        appDelegate.summaryUpdater.updateSummary(context: appDelegate.context)
        super.viewWillLayout()
    }

    @IBAction func actionStartSession(_ sender: Any) {
        assert(state == .noSession)
        do {
            setState(ReviewPanelState.from(
                session: try startSession(
                    context: appDelegate.context,
                    timekeeper: appDelegate.timekeeper,
                    limit: ReviewSession.defaultSize)))
        } catch {
            fatalError("cannot start session: \(error)")
        }
    }

    @IBAction func actionEndSession(_ sender: Any) {
        setState(.noSession)
    }
    
    @IBAction func actionShowAnswer(_ sender: Any) {
        state.session?.reviewerShowsAnswer(at: appDelegate.timekeeper.now())
        nextState()
    }

    @IBAction func actionSkipZettel(_ sender: Any) {
        state.session?.reviewerSkips()
        nextState()
    }
    
    @IBAction func actionHasSeenNewZettel(_ sender: Any) {
        do {
            try state.session?.reviewerHasSeenAnswer(at: appDelegate.timekeeper.now())
        } catch {
            fatalError("cannot mark zettel seen: \(error)")
        }
        nextState()
    }

    @IBAction func actionMarkZettelLearned(_ sender: Any) {
        do {
            try state.session?.reviewerLearns(at: appDelegate.timekeeper.now())
        } catch {
            fatalError("cannot mark learned: \(error)")
        }
        nextState()
    }

    @IBAction func actionMarkZettelNotLearned(_ sender: Any) {
        do {
            try state.session?.reviewerNotLearns(at: appDelegate.timekeeper.now())
        } catch {
            fatalError("cannot mark zettel not learned: \(error)")
        }
        nextState()
    }
    
    @IBAction func actionEditZettel(_ sender: Any) {
        switch state {
        case .noSession:
            fatalError("illegal state transition: cannot edit zettel in NoSession state")
        case .reviewingQuestion(let session),
             .reviewingAnswer(let session),
             .reviewingQuestionAnswer(let session),
             .showingQuestion(let session),
             .showingAnswer(let session),
             .showingQuestionAnswer(let session):
            editZettel(zettel: session.card!.zettel)
        }
    }

    func editZettel(zettel: Zettel) {
        appDelegate.controller.browse.clearZettelDetails()
        appDelegate.controller.browse.setActiveZettel(zettel: zettel)
        appDelegate.controller.browse.setZettelDetails(zettel: zettel)
        appDelegate.controller.actionSelectBrowseTab(self)
    }
}

enum ReviewPanelState: Equatable {
    case noSession
    case reviewingQuestion(session: ReviewSession)
    case reviewingAnswer(session: ReviewSession)
    case reviewingQuestionAnswer(session: ReviewSession)
    case showingQuestion(session: ReviewSession)
    case showingAnswer(session: ReviewSession)
    case showingQuestionAnswer(session: ReviewSession)

    static func == (lhs: ReviewPanelState, rhs: ReviewPanelState) -> Bool {
        return ReviewPanelUiState.from(panel: lhs) == ReviewPanelUiState.from(panel: rhs)
    }
    
    var card: ReviewCard? {
        get { return session?.card }
    }
    
    var session: ReviewSession? {
        get {
            switch self {
            case .noSession: return nil
            case .reviewingQuestion(let session): return session
            case .reviewingAnswer(let session): return session
            case .reviewingQuestionAnswer(let session): return session
            case .showingQuestion(let session): return session
            case .showingAnswer(let session): return session
            case .showingQuestionAnswer(let session): return session
            }
        }
    }
    
    static func from(session s: ReviewSession?) -> ReviewPanelState {
        guard let s = s else {
            return .noSession
        }
        switch s.state! {
        case .ended: return .noSession
        case .newNotShownQ(_): return .showingQuestion(session: s)
        case .newNotShownA(_): return .showingAnswer(session: s)
        case .newNotShownQA(_): return .showingQuestionAnswer(session: s)
        case .reviewQ(_): return .reviewingQuestion(session: s)
        case .reviewA(_): return .reviewingAnswer(session: s)
        case .reviewQA(_): return .reviewingQuestionAnswer(session: s)
        }
    }
}

enum ReviewPanelUiState {
    case noSession
    case reviewingQuestion
    case reviewingAnswer
    case reviewingQuestionAnswer
    case showingQuestion
    case showingAnswer
    case showingQuestionAnswer
    
    static func from(panel state: ReviewPanelState) -> ReviewPanelUiState {
        switch state {
        case .noSession: return .noSession
        case .reviewingQuestion(_): return .reviewingQuestion
        case .reviewingAnswer(_): return .reviewingAnswer
        case .reviewingQuestionAnswer(_): return .reviewingQuestionAnswer
        case .showingQuestion(_): return .showingQuestion
        case .showingAnswer(_): return .showingAnswer
        case .showingQuestionAnswer(_): return .showingQuestionAnswer
        }
    }
}

class View: NSView {
    var backgroundColor: NSColor? = nil

    convenience init(backgroundColor: NSColor) {
        self.init()
        self.backgroundColor = backgroundColor
    }

    override func draw(_ dirty: NSRect) {
        super.draw(dirty)
        if let bgc = backgroundColor {
            bgc.setFill()
            dirty.fill()
        }
    }
}

class StackView: NSStackView {
    var backgroundColor: NSColor? = nil

    convenience init(backgroundColor: NSColor) {
        self.init()
        self.backgroundColor = backgroundColor
    }
    
    override func draw(_ dirty: NSRect) {
        super.draw(dirty)
        if let bgc = backgroundColor {
            bgc.setFill()
            dirty.fill()
        }
    }
}
