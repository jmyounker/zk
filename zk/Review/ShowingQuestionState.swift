//
//  QuestionState.swift
//  zk
//
//  Created by Jeff Younker on 1/19/20.
//  Copyright © 2020 The Blobshop. All rights reserved.
//

import Cocoa
import Foundation

class ShowingQuestionStateCenterController: NSViewController {
    var questionView: LinkingTextView = withoutResizeMask(LinkingTextView())
    // Holds a reference to our observation for the controller's lifetime.
    var questionObservation: Any!

    convenience init(target t: ReviewController) {
        self.init()
        startObserving(target: t)
    }

    override func loadView() {
        questionView.isEditable = false
        let view = NSStackView()
        view.addArrangedSubview(questionView)
        self.view = view
    }
    
    func startObserving(target: ReviewController) {
        questionObservation = target.observe(
        \ReviewController.reviewCard, options: .new) { target, change in
            let new = change.newValue as? ObjcReviewCard
            switch new?.value {
            case .none:
                self.questionView.setContent(to: nil)
                self.questionView.toolTip = Localized("review-no-card-content-tooltip")
            case .single:
                self.questionView.setContent(to: nil)
                self.questionView.toolTip = Localized("review-not-right-card-tooltip")
            case .double(_, let front, _):
                self.questionView.setContent(to: front)
                self.questionView.toolTip = Localized("review-show-question-two-side-card-content-tooltip")
            }
        }
    }
}

class ShowingQuestionStateBottomController: NSViewController {
    override func loadView() {
        self.view = NSView()
        let edit = withoutResizeMask(NSButton(
            title: "Edit",
            target: appDelegate.controller.review,
            action: #selector(ReviewController.actionEditZettel(_:))))
        edit.toolTip = Localized("review-edit-button-tooltip")
        let showAnswer: NSButton = withoutResizeMask(NSButton(
            title: "Show Answer",
            target: appDelegate.controller.review,
            action: #selector(ReviewController.actionShowAnswer(_:))))
        showAnswer.toolTip = Localized("review-show-answer-button-tooltip")
        let skip: NSButton = withoutResizeMask(NSButton(
            title: "Skip",
            target: appDelegate.controller.review,
            action: #selector(ReviewController.actionSkipZettel(_:))))
        skip.toolTip = Localized("review-skip-button-tooltip")
        let endSession: NSButton = withoutResizeMask(NSButton(
            title: "End Review",
            target: appDelegate.controller.review,
            action: #selector(ReviewController.actionEndSession(_:))))
        endSession.toolTip = Localized("review-end-session-button-tooltip")
        let centerControls = withoutResizeMask(
            sideBySideView(leadingToTrailing: [showAnswer, skip]))
        
        view.addSubview(edit)
        view.addSubview(centerControls)
        view.addSubview(endSession)

        let views: Dictionary<String, NSView> = [
            "left": edit,
            "center": centerControls,
            "right": endSession,
        ]

        var constraints = Array<NSLayoutConstraint>()
        constraints += NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-[left]->=0-[center]->=0-[right]-|",
            options: [],
            metrics: nil,
            views: views)
        constraints += NSLayoutConstraint.constraints(
            withVisualFormat: "V:|->=0-[center]-|",
            options: [],
            metrics: nil,
            views: views)
        constraints += NSLayoutConstraint.constraints(
            withVisualFormat: "V:[left]-|",
            options: [],
            metrics: nil,
            views: views)
        constraints += NSLayoutConstraint.constraints(
            withVisualFormat: "V:[right]-|",
            options: [],
            metrics: nil,
            views: views)
        view.addConstraints(constraints)
        centerX(child: centerControls, within: view)
    }
}
