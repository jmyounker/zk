#  Review Application

`Zettel`n with the `review` tag are treated as spaced repetition cards.

## Components

Each `Zettel` has zero or more `ReviewRecord`s associated with it. A review record is either in the state `new` or `learning`.  `new(Zettel)` have never been memorized. `learning(Zettel)` have been memorized at least once. 

Cards are represented by the enum `ReviewCard`.  Cards can either be a combined prompt-answer (`single`) or prompt and answer (`double`). 

Each `ReviewCard` has a set of states that it moves through in the session. The enum `SessionState` represents these states. The initial state depends upon the `ReviewRecord` and the `Card`.

The UI has mutiple states. The enum `ReviewPanelUIState` represents these states.

A review session has a set of `SessionState`s. The current `SessionState` determines the `ReviewPanelUIState`, each state associated with a different view.

The user's actions in each review panel determine what happens to the current session state, and which session state gets chosen next.

## Process

The panel starts in `.noSessionState`. There are some combination of `new`, `learning`, and `learned` review records. We can ignore `learned` records, as nothing needs to be done with them.

From the initial session state the user clicks the `start` button. The program picks some combination of `new` and `learning` zettel. For new ones it synthesizes a `new(Zettel)` review record. For previously learned ones it picks the most recent `learning(Zettel)` record.

It turns these each `new(Zettel)` into one or more `newNotShownQ(ReviewCard)`. It turns each `learning(Zettel)` into one or more `review(ReviewCard)`. These are then shuffled, and the max number for a review session are selected, they are enqueued, and the head becomes the current `SessionState`.

The user takes one action in the `ReviewPanelUIState`, the `SessionState` advances to the next state, that state is put on the end of the state queue, and the new head of the state queue becomes the current queue. When a `SessionState` is complete (learned) it is removed from the queue. This continues until the queue is empty.

### Transformations from `ReviewRecord` to `SessionState`

`ReviewRecord.new` -> `ReviewCard.double` -> `SessionState.newNotShownQ`

`ReviewRecord.learning` -> `ReviewCard.double` -> `SessionState.reviewQ`

`ReviewRecord.new` -> `ReviewCard.single` -> `SessionState.newNotShownQA`

`ReviewRecord.learning` -> `ReviewCard.single` -> `SessionState.reviewQA`

###  `SessionState` transitions

newNotShownQ -> newNotShownA -> [review Q -> review A]+ -> end

[reviewQ -> reviewA]+ -> end

newNotShownQA -> [reviewQA]+ -> end

[reviewQA]+  -> end


