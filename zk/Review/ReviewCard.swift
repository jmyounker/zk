//
//  ReviewCard.swift
//  zk
//
//  Created by Jeff Younker on 1/31/20.
//  Copyright © 2020 The Blobshop. All rights reserved.
//

import Foundation

@objc class ObjcReviewCard: NSObject {
    let value: ReviewCard
    
    init(_ value: ReviewCard) {
        self.value = value
    }
    
    static func from(card: ReviewCard?) -> ObjcReviewCard? {
        guard let card = card else {
            return nil
        }
        return ObjcReviewCard(card)
    }
}

enum ReviewCard {
    case single(zettel: Zettel, content: NSAttributedString)
    case double(
        zettel: Zettel, question: NSAttributedString, answer: NSAttributedString)

    static func from(zettel: Zettel?) -> [ReviewCard] {
        guard let zettel = zettel else {
            return []
        }
        let pages = zettel.pagesByName
        return ReviewCard.from(zettel: zettel, front: pages[.front], back: pages[.back])
    }
    
    static func from(zettel: Zettel?, front: Page?, back: Page?) -> [ReviewCard] {
        guard let zettel = zettel else {
            return []
        }
        guard let frontPage = front else {
            // TOOD(jyounker): Handle this more gracefully
            return []
        }
        if let backPage = back {
            let materializedFront = LinkingTextView()
            materializedFront.setPageContents(page: frontPage)
            let materializedBack = LinkingTextView()
            materializedBack.setPageContents(page: backPage)
            return [ReviewCard.double(
                zettel: zettel,
                question: materializedFront.attributedString(),
                answer: materializedBack.attributedString())]
        }
        let materializedFront = LinkingTextView()
        materializedFront.setPageContents(page: frontPage)

        let (q, a) =  extractQuestionAndAnswer(materializedFront.attributedString())

        if q == nil  || a == nil {
            return [ReviewCard.single(
                zettel: zettel,
                content: materializedFront.attributedString())]
        }

        return [ReviewCard.double(zettel: zettel, question: q!, answer: a!)]
    }
    
    var zettel: Zettel {
        get {
            switch self {
            case .double(let z, _, _): return z
            case .single(let z, _): return z
            }
        }
    }
}

let qAndARegex = staticRegex(
    pattern: #"^[\s\t\n]*(\S[^\n]*)[\n][\s\t\n]*(\S.*)$"#, options: [.dotMatchesLineSeparators])

func extractQuestionAndAnswer(_ x: NSAttributedString) -> (NSAttributedString?, NSAttributedString?) {
    let matches = qAndARegex.capturedNSRanges(in: x.string, range: wholeString(x.string))
    if matches.count == 0 {
        return (nil, nil)
    }
    assert(matches.count == 2)
    return (
        x.attributedSubstring(from: matches[0]),
        x.attributedSubstring(from: matches[1]))
}

