//
//  NoSessionState.swift
//  zk
//
//  Created by Jeff Younker on 1/19/20.
//  Copyright © 2020 The Blobshop. All rights reserved.
//

import Cocoa
import Foundation

import Logging

class NoSessionStateCenterController: NSViewController {
    
    override func loadView() {
        self.view = NSView()
        let stack = withoutResizeMask(NSStackView())
        stack.orientation = .horizontal
        stack.alignment = .centerY
        let summaryControl = TableSummaryControl()
        summaryControl.startObserving(target: appDelegate, keyPath: \AppDelegate.reviewSummary)
        self.view.addSubview(stack)
        stack.addArrangedSubview(summaryControl.view)
        let startButton = withoutResizeMask(NSButton(
            title: "Start",
            target: appDelegate.controller.review,
            action: #selector(ReviewController.actionStartSession(_:))))
        startButton.toolTip = Localized("review-start-session-button-tooltip")
        stack.addArrangedSubview(startButton)
        center(child: stack, within: self.view)
    }
}

class NoSessionStateBottomController: NSViewController {
    
    static let log = Logger(label: "com.theblobshop.zk.review.NoSessionStateBottomController")
    var debug: DebugController!

    override func loadView() {
        self.debug = appDelegate.debug
        self.view = NSView()
        let stack = withoutResizeMask(NSStackView())
        self.view.addSubview(stack)
        centerX(child: stack, within: self.view)
        stack.orientation = .horizontal
        _ = debug.add(stack)
        let dumpNewCardsButton = withoutResizeMask(NSButton(
            title: "Dump New Cards",
            target: self,
            action: #selector(actionDumpNew(_:))))
        let dumpLearningCardsButton = withoutResizeMask(NSButton(
            title: "Dump Learning Cards",
            target: self,
            action: #selector(actionDumpLearning(_:))))
        let dumpLearnedCardsButton = withoutResizeMask(NSButton(
            title: "Dump Learned Cards",
            target: self,
            action: #selector(actionDumpLearned(_:))))
        stack.addArrangedSubview(dumpNewCardsButton)
        stack.addArrangedSubview(dumpLearningCardsButton)
        stack.addArrangedSubview(dumpLearnedCardsButton)
    }

    @IBAction func actionDumpNew(_ sender: Any) {
        NoSessionStateBottomController.log.info("+ DUMPING NEW CARDS")
        do {
            let cards = try ReviewEngine.selectNew(context: appDelegate.context)
            for c in cards {
                NoSessionStateBottomController.log.info("+++ NEW: \(c.title ?? "-no title-") (\(c.zid!))")
            }
        } catch {
            NoSessionStateBottomController.log.error("cannot dump new cards: \(error)")
        }
    }

    @IBAction func actionDumpLearning(_ sender: Any) {
        let c = appDelegate.context
        let now = appDelegate.timekeeper.now()
        NoSessionStateBottomController.log.info("+ DUMPING LEARNING CARDS")
        NoSessionStateBottomController.log.info("++ NOW IS \(now.dump()) (\(now))")
        do {
            let reviewTimes = try ReviewEngine.selectLearning(context: c, at: now)
            for rt in reviewTimes {
                if let z = Zettel.select(context: c, zid: rt.zid) {
                    NoSessionStateBottomController.log.info("+++ LEARNING: \(z.title ?? "-no title-") (\(z.zid!))")
                }
            }
        } catch {
            NoSessionStateBottomController.log.error("cannot dump new cards: \(error)")
        }
    }

    @IBAction func actionDumpLearned(_ sender: Any) {
        let c = appDelegate.context
        let now = appDelegate.timekeeper.now()
        NoSessionStateBottomController.log.info("+ DUMPING LEARNED CARDS")
        NoSessionStateBottomController.log.info("++ NOW IS \(now.dump()) (\(now))")
        do {
            let reviewTimes = try ReviewEngine.selectLearned(context: c, at: now)
            for rt in reviewTimes {
                if let z = Zettel.select(context: c, zid: rt.zid) {
                    NoSessionStateBottomController.log.info("+++ WILL REVIEW AT: \(rt.at.dump()) \(z.title ?? "-no title-") (\(z.zid!))")
                } else {
                    NoSessionStateBottomController.log.info("+++ COULDNT LOAD ZETTEL \(rt.zid)")
                }
            }
        } catch {
            NoSessionStateBottomController.log.error("cannot dump new cards: \(error)")
        }
    }
}
