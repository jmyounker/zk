//
//  SummaryControl.swift
//  zk
//
//  Created by Jeff Younker on 1/19/20.
//  Copyright © 2020 The Blobshop. All rights reserved.
//

import Cocoa
import Foundation

import Logging

protocol SummaryControl {
    var view: NSView { get }
    func update(summary: ReviewSummary)
    func startObserving<X: NSObject>(target: X, keyPath: KeyPath<X, ReviewSummary>)
}

class TableSummaryControl: BaseSummaryControl {
    lazy var view: NSView = {
        new = NSTextField(labelWithString: "0")

        learning = NSTextField(labelWithString: "0")
        learning!.textColor = Color.red

        learned = NSTextField(labelWithString: "0")
        learned!.textColor = Color.green

        let table = withoutResizeMask(NSGridView(numberOfColumns: 2, rows: 3))
        table.column(at: 0).xPlacement = .trailing

        table.cell(atColumnIndex: 0, rowIndex: 0).contentView = NSTextField(
            labelWithString: "new")
        table.cell(atColumnIndex: 1, rowIndex: 0).contentView = new
        table.cell(atColumnIndex: 0, rowIndex: 0).contentView!.toolTip = Localized("review-table-summary-new-tooltip")
        table.cell(atColumnIndex: 1, rowIndex: 0).contentView!.toolTip = Localized("review-table-summary-new-tooltip")

        let learningLabel = NSTextField(labelWithString: "learning")
        learningLabel.textColor = Color.red
        table.cell(atColumnIndex: 0, rowIndex: 1).contentView = learningLabel
        table.cell(atColumnIndex: 1, rowIndex: 1).contentView = learning
        table.cell(atColumnIndex: 0, rowIndex: 1).contentView!.toolTip = Localized("review-table-summary-learning-tooltip")
        table.cell(atColumnIndex: 1, rowIndex: 1).contentView!.toolTip = Localized("review-table-summary-learning-tooltip")

        let learnedLabel = NSTextField(labelWithString: "learned")
        learnedLabel.textColor = Color.green
        table.cell(atColumnIndex: 0, rowIndex: 2).contentView = learnedLabel
        table.cell(atColumnIndex: 1, rowIndex: 2).contentView = learned
        table.cell(atColumnIndex: 0, rowIndex: 2).contentView!.toolTip = Localized("review-table-summary-learned-tooltip")
        table.cell(atColumnIndex: 1, rowIndex: 2).contentView!.toolTip = Localized("review-table-summary-learned-tooltip")
        return table
    }()
}

class OneLineSummaryControl: BaseSummaryControl {
    lazy var view: NSView = {
        new = withoutResizeMask(NSTextField(labelWithString: "0"))
        new!.isEditable = false

        learning = withoutResizeMask(NSTextField(labelWithString: "0"))
        learning!.isEditable = false
        learning!.textColor = Color.red

        learned = withoutResizeMask(NSTextField(labelWithString: "0"))
        learned!.isEditable = false
        learned!.textColor = Color.green

        let line = withoutResizeMask(sideBySideView(leadingToTrailing: [
            new!,
            withoutResizeMask(NSTextField(labelWithString: "+")),
            learning!,
            withoutResizeMask(NSTextField(labelWithString: "+")),
            learned!,
        ]))

        return line
    }()
}

class BaseSummaryControl {
    var new: NSTextField? = nil
    var learning: NSTextField? = nil
    var learned: NSTextField? = nil
    var observation: NSKeyValueObservation!
    
    func update(summary: ReviewSummary) {
        self.new?.stringValue = "\(summary.new)"
        self.learning?.stringValue = "\(summary.learning)"
        self.learned?.stringValue = "\(summary.learned)"
    }

    func startObserving<X: NSObject>(target: X, keyPath: KeyPath<X, ReviewSummary>) {
        if observation != nil {
            fatalError("programmer fuckup: cannot reset summary observations")
        }
        observation = target.observe(keyPath, options: [.new]) { target, change in
            self.update(summary: change.newValue ?? ReviewSummary(new: 0, learning: 0, learned: 0))
        }
    }
}

class SummaryUpdater {
    let log = Logger(label: "com.theblobshop.zk.SummaryUpdater")
    
    func observe(context c: NSManagedObjectContext) {
        let nc = NotificationCenter.default
        nc.addObserver(
            self,
            selector: #selector(contextDidSave(_:)),
            name: NSNotification.Name.NSManagedObjectContextDidSave,
            object: c)
    }
    
    @objc func contextDidSave(_ notification: Notification) {
        guard let c = notification.object as? NSManagedObjectContext else {
            return
        }
        guard let userInfo = notification.userInfo else {
            return
        }
        if !shouldUpdateSummary(userInfo: userInfo) {
            return
        }
        DispatchQueue.main.async {
            self.updateSummary(context: c)
        }
    }
    
    func shouldUpdateSummary(userInfo: [AnyHashable : Any]) -> Bool {
        if let deletes = userInfo[NSDeletedObjectsKey] as? Set<NSManagedObject> {
            for x in deletes {
                switch x {
                case is ReviewEvent: return true
                case is Tag: return true
                case is Zettel: return true
                default: break
                }
            }
        }
        
        if let updates = userInfo[NSUpdatedObjectsKey] as? Set<NSManagedObject> {
            for x in updates {
                switch x {
                case is Tag: return true
                case is Zettel: return true
                default: break
                }
            }
        }

        if let inserts = userInfo[NSInsertedObjectsKey] as? Set<NSManagedObject> {
            for x in inserts {
                switch x {
                case is ReviewEvent: return true
                case is Tag: return true
                default: break
                }
            }
        }
        return false
    }
    
    /// Updates the summary information.
    ///
    /// Must be run on main thread.
    func updateSummary(context c: NSManagedObjectContext) {
        do {
            appDelegate.reviewSummary = try ReviewEngine.reviewSummary(
                context: c, at: appDelegate.timekeeper.now())
        } catch {
            log.warning("cannot update summary: \(error)")
        }
    }
}
