//
//  ShowingQuestionAnswerState.swift
//  zk
//
//  Created by Jeff Younker on 7/28/20.
//  Copyright © 2020 The Blobshop. All rights reserved.
//

import Cocoa
import Foundation

class ShowingQuestionAnswerStateCenterController: NSViewController {
    var answerView: LinkingTextView = withoutResizeMask(LinkingTextView())
    // Holds a reference to our observation for the controller's lifetime.
    var answerObservation: Any!

    convenience init(target t: ReviewController) {
        self.init()
        startObserving(target: t)
    }

    override func loadView() {
        answerView.isEditable = false
        let view = NSStackView()
        view.addArrangedSubview(answerView)
        self.view = view
    }
    
    func startObserving(target: ReviewController) {
        answerObservation = target.observe(
        \ReviewController.reviewCard, options: .new) { target, change in
            let new = change.newValue as? ObjcReviewCard
            switch new?.value {
            case .none:
                self.answerView.setContent(to: nil)
                self.answerView.toolTip = Localized("review-card-no-content-tooltip")
            case .single(_, let content):
                self.answerView.setContent(to: content)
                self.answerView.toolTip = Localized("review-show-answer-one-side-card-content-tooltip")
            case .double:
                self.answerView.setContent(to: nil)
                self.answerView.toolTip = Localized("review-not-right-card-tooltip")
            }
            self.answerView.needsLayout = true
            self.answerView.needsDisplay = true
        }
    }
}

class ShowingQuestionAnswerStateBottomController: NSViewController {
    override func loadView() {
        self.view = NSView()
        let edit = withoutResizeMask(NSButton(
            title: "Edit",
            target: appDelegate.controller.review,
            action: #selector(ReviewController.actionEditZettel(_:))))
        edit.toolTip = Localized("review-edit-button-tooltip")
        let continuing: NSButton = withoutResizeMask(NSButton(
            title: "Continue",
            target: appDelegate.controller.review,
            action: #selector(ReviewController.actionHasSeenNewZettel(_:))))
        continuing.toolTip = Localized("review-continue-button-tooltip")
        let skip: NSButton = withoutResizeMask(NSButton(
            title: "Skip",
            target: appDelegate.controller.review,
            action: #selector(ReviewController.actionSkipZettel(_:))))
        skip.toolTip = Localized("review-skip-button-tooltip")
        let endSession: NSButton = withoutResizeMask(NSButton(
            title: "End Review",
            target: appDelegate.controller.review,
            action: #selector(ReviewController.actionEndSession(_:))))
        endSession.toolTip = Localized("review-end-session-button-tooltip")

        let centerControls = withoutResizeMask(
            sideBySideView(leadingToTrailing: [continuing, skip]))
        
        view.addSubview(edit)
        view.addSubview(centerControls)
        view.addSubview(endSession)

        let views: Dictionary<String, NSView> = [
            "left": edit,
            "center": centerControls,
            "right": endSession,
        ]

        var constraints = Array<NSLayoutConstraint>()
        constraints += NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-[left]->=0-[center]->=0-[right]-|",
            options: [],
            metrics: nil,
            views: views)
        constraints += NSLayoutConstraint.constraints(
            withVisualFormat: "V:|->=0-[center]-|",
            options: [],
            metrics: nil,
            views: views)
        constraints += NSLayoutConstraint.constraints(
            withVisualFormat: "V:[left]-|",
            options: [],
            metrics: nil,
            views: views)
        constraints += NSLayoutConstraint.constraints(
            withVisualFormat: "V:[right]-|",
            options: [],
            metrics: nil,
            views: views)
        view.addConstraints(constraints)
        centerX(child: centerControls, within: view)
    }
}
