//
//  QuestionPresentationControlsController.swift
//  zk
//
//  Created by Jeff Younker on 1/17/20.
//  Copyright © 2020 The Blobshop. All rights reserved.
//

import Cocoa
import Foundation

class QuestionPresentationControlsController: NSViewController {
    func createView() {
        let button = withoutResizeMask(NSButton(
            title: "Start review",
            target: nil,
            action: #selector(ReviewController.actionStartSession(_:))))
        self.view = button
    }
}
