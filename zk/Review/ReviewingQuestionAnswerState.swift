//
//  ReviewingQuestionAnswerState.swift
//  zk
//
//  Created by Jeff Younker on 7/28/20.
//  Copyright © 2020 The Blobshop. All rights reserved.
//

import Cocoa
import Foundation

class ReviewingQuestionAnswerStateCenterController: NSViewController {
    var answerView: LinkingTextView = withoutResizeMask(LinkingTextView())
    // Holds a reference to our observation for the controller's lifetime.
    var answerObservation: Any!

    convenience init(target t: ReviewController) {
        self.init()
        startObserving(target: t)
    }

    override func loadView() {
        answerView.isEditable = false
        let view = NSStackView()
        view.addArrangedSubview(answerView)
        self.view = view
    }
    
    func startObserving(target: ReviewController) {
        answerObservation = target.observe(
        \ReviewController.reviewCard, options: .new) { target, change in
            let new = change.newValue as? ObjcReviewCard
            switch new?.value {
            case .none:
                self.answerView.setContent(to: nil)
                self.answerView.toolTip = Localized("review-no-card-content-tooltip")
            case .single(_, let content):
                self.answerView.setContent(to: content)
                self.answerView.toolTip = Localized("review-review-question-answer-one-side-card-content-tooltip")
            case .double:
                self.answerView.setContent(to: nil)
                self.answerView.toolTip = Localized("review-not-right-card-tooltip")
            }
            self.answerView.needsLayout = true
            self.answerView.needsDisplay = true
        }
    }
}

class ReviewingQuestionAnswerStateBottomController: NSViewController {
    override func loadView() {
        self.view = NSView()
        let edit = withoutResizeMask(NSButton(
            title: "Edit",
            target: appDelegate.controller.review,
            action: #selector(ReviewController.actionEditZettel(_:))))
        edit.toolTip = Localized("review-edit-button-tooltip")
        let learned: NSButton = withoutResizeMask(NSButton(
            title: "Got it!",
            target: appDelegate.controller.review,
            action: #selector(ReviewController.actionMarkZettelLearned(_:))))
        learned.toolTip = Localized("review-learned-button-tooltip")
        let notLearned: NSButton = withoutResizeMask(NSButton(
            title: "Not yet",
            target: appDelegate.controller.review,
            action: #selector(ReviewController.actionMarkZettelNotLearned(_:))))
        notLearned.toolTip = Localized("review-not-learned-button-tooltip")
        let skip: NSButton = withoutResizeMask(NSButton(
            title: "Skip",
            target: appDelegate.controller.review,
            action: #selector(ReviewController.actionSkipZettel(_:))))
        skip.toolTip = Localized("review-skip-button-tooltip")
        let endSession: NSButton = withoutResizeMask(NSButton(
            title: "End Review",
            target: appDelegate.controller.review,
            action: #selector(ReviewController.actionEndSession(_:))))
        endSession.toolTip = Localized("review-end-session-button-tooltip")

        let centerControls = withoutResizeMask(
            sideBySideView(leadingToTrailing: [learned, notLearned, skip]))
        
        view.addSubview(edit)
        view.addSubview(centerControls)
        view.addSubview(endSession)

        let views: Dictionary<String, NSView> = [
            "left": edit,
            "center": centerControls,
            "right": endSession,
        ]

        var constraints = Array<NSLayoutConstraint>()
        constraints += NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-[left]->=0-[center]->=0-[right]-|",
            options: [],
            metrics: nil,
            views: views)
        constraints += NSLayoutConstraint.constraints(
            withVisualFormat: "V:|->=0-[center]-|",
            options: [],
            metrics: nil,
            views: views)
        constraints += NSLayoutConstraint.constraints(
            withVisualFormat: "V:[left]-|",
            options: [],
            metrics: nil,
            views: views)
        constraints += NSLayoutConstraint.constraints(
            withVisualFormat: "V:[right]-|",
            options: [],
            metrics: nil,
            views: views)
        view.addConstraints(constraints)
        centerX(child: centerControls, within: view)
    }
}
