# Summary

Zk: The ultimate in yak shaving.

Zk combines zettelkasten note taking software with spaced repetition software. This allows you to use one set of notes without exporting back and forth.

I considered using exsting zettelkasten software, but the best implementation feature wise is pretty clunky and definitely non-mac native. Also, no spaced repetition.

I considered using spaced repetition software. The obvious choice was Anki, but the author is soley againt adding cross-card linking which is a primary feature needed for the zettelkasten.


## Zettel Kasten

The entire system is based around the idea of 3x5 note cards. They software has front and back sides for each card. The top line is assumed to be a title. The title is unique.

Cards have a numbering system, in which they can be nested one below the other. This manifests itself as a tree in the UI. Cards can have links to other cards.

There are several fixed top-level cards:
  * Inbox
  * Index
  * Authors
  * Bibliography
  * Kasten
  
## Spaced Repetition

Cards with "review" checked become flashcards. 

You have only one mind, and there is only one review category. If you want to forget something, you just untag it.

The spaced repetition algorithm is a based on the leitner box algorithm with a bit of tuning.

Future plans include replacing that with a more adaptive algorithm based on
[Enhancing human learning via spaced repetition optimization](https://www.pnas.org/content/116/10/3988).

## Future Directions

These are really driven by what I need for my own use.

  * In the future I think I'll add outlines, allowing you to take your notes and combine them into independent outlines
  * CloudKit shared application
  * iPhone application
  * iPad application
  * Import-export

##  References

  * [Enhancing human learning via spaced repetition optimization](https://www.pnas.org/content/116/10/3988)
  * [Zettelkasten method introduction](https://www.lesswrong.com/posts/NfdHG6oHBJ8Qxc26s/the-zettelkasten-method-1)
