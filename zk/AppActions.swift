//
//  AppActions.swift
//  zk
//
//  Created by Jeff Younker on 1/6/20.
//  Copyright © 2020 The Blobshop. All rights reserved.
//

import Foundation

// I ended up adding this so I could get selectors working correctly from the link generation
// functions. It seems to provide an anchor point for others to reference the selectors.
@objc protocol AppActions {
    @objc optional func actionSingleClickLink(_ sender: Any)
    @objc optional func actionDoubleClickLink(_ sender: Any)
}
