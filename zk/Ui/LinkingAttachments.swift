//
//  LinkAttachments.swift
//  zk
//
//  Created by Jeff Younker on 11/21/19.
//  Copyright © 2019 The Blobshop. All rights reserved.
//

import Cocoa

import Logging

typealias Attributes = Dictionary<NSAttributedString.Key, NSObject>

func reestablishLinks(context c: NSManagedObjectContext, text: NSAttributedString) throws {
    let linkAttachments = linkTargets(from: attachments(from: text))
    if linkAttachments.count == 0 {
        return
    }
    let zids = Set(linkAttachments.map{ $0.target })
    let zettel = Zettel.select(context: c, query: ZettelQuery(zids: zids.map{ $0 }))
    let zettelByZid = indexByUniqueKey(zettel) { $0.zid! }
    for la in linkAttachments {
        let title = zettelByZid[la.target]?.title! ?? "- deleted -"
        la.attachment.attachmentCell = linkAttachmentCell(label: title, zid: la.target)
    }
}

func linkAttachment(label: String, zid: String, kind: String) -> NSTextAttachment {
    let attachment = NSTextAttachment(data: zid.data(using: String.Encoding.utf8), ofType: kind)
    attachment.attachmentCell = linkAttachmentCell(label: label, zid: UUID(uuidString: zid)!)
    return attachment
}

@objc class ObjectiveZid: NSObject {
    let value: Zid
    
    init(_ value: Zid) {
        self.value = value
    }
}

func linkAttachmentCell(label: String, zid: Zid) -> LabeledAttachmentCell {
    let oz = ObjectiveZid(zid)
    
    return LinkAttachmentCell(
        label: label,
        zid: zid,
        attributes: [
            NSAttributedString.Key.foregroundColor: NSColor.black,
            NSAttributedString.Key.font: NSFont.systemFont(ofSize: 12),
        ],
        singleClick: {
            NSApplication.shared.sendAction(
                #selector(AppActions.actionSingleClickLink(_:)), to: nil, from: oz)
        },
        doubleClick: {
            NSApplication.shared.sendAction(
                #selector(AppActions.actionDoubleClickLink(_:)), to: nil, from: oz)
        }
    )
}

class LinkAttachmentCell: LabeledAttachmentCell {
    let zid: Zid
    var clicker: Clicker!
    let log = Logger(label: "com.theblobshop.zk.LinkAttachment")

    init(
        label: String,
        zid: Zid,
        attributes: Attributes,
        singleClick: @escaping ()->Void,
        doubleClick: @escaping ()->Void) {
        self.zid = zid
        super.init(label: label, attributes: attributes)
        self.clicker = Clicker(
            singleClick: singleClick,
            doubleClick: doubleClick)
    }
    
    required init(coder: NSCoder) {
        guard let zid = coder.decodeObject(forKey: "zid") as? UUID else {
            fatalError("cannot instantiate zid")
        }
        self.zid = zid
        super.init(coder: coder)
    }

    override func trackMouse(
        with theEvent: NSEvent,
        in cellFrame: NSRect,
        of controlView: NSView?,
        untilMouseUp flag: Bool) -> Bool {
        clicker.click()
        return false
    }
}

class LabeledAttachmentCell: NSTextAttachmentCell {
    let label: String
    let attributes: Attributes
    
    init(label: String, attributes: Attributes) {
        self.label = label
        self.attributes = attributes
        super.init()
    }
    
    required init(coder: NSCoder) {
        guard let label = coder.decodeObject(forKey: "label") as? String else {
            fatalError("cannot instantiate label")
        }
        self.label = label
        guard let attributes = coder.decodeObject(forKey: "attributes") as? Attributes else {
            fatalError("cannot instantiate attributes")
        }
        self.attributes = attributes
        super.init(coder: coder)
    }
    
    override func cellSize() -> NSSize {
        let ls = labelSize()
        let ss = surroundingSpace()
        return NSSize(width: ls.width + 2 * ss.width, height: ls.height)
    }

    func labelSize() -> NSSize {
        return label.size(withAttributes: attributes)
    }

    func surroundingSpace() -> NSSize {
        return "x".size(withAttributes: attributes)
    }

    func textCellRect(cellFrame: NSRect) -> NSRect {
        let ss = surroundingSpace()
        return NSRect(
            origin: NSPoint(x: cellFrame.origin.x + ss.width, y: cellFrame.origin.y),
            size: labelSize())
    }

    override func draw(withFrame cellFrame: NSRect, in controlView: NSView?) {
        let gc = NSGraphicsContext.current!
        gc.saveGraphicsState()
        let c = gc.cgContext
        drawRoundedRect(
            rect: cellFrame,
            inContext: c,
            radius: 3,
            borderColor: CGColor(red: 0.7, green: 0.7, blue: 0.9, alpha: 1.0),
            fillColor: CGColor(red: 0.7, green: 0.7, blue: 0.9, alpha: 1.0))
        let opts = NSString.DrawingOptions.init([NSString.DrawingOptions.usesLineFragmentOrigin])
        label.draw(
            with: textCellRect(cellFrame: cellFrame),
            options: opts,
            attributes: attributes)
        gc.restoreGraphicsState()
    }
    
    override func cellBaselineOffset() -> NSPoint {
        // TODO(jmyounker): In the future this will be dynamically calculated.
        return NSPoint(x: 0, y: -3)
    }
    
    func drawRoundedRect(
        rect: CGRect,
        inContext context: CGContext,
        radius: CGFloat,
        borderColor:
        CGColor,
        fillColor: CGColor) {
        let path = CGMutablePath()
        path.move( to: CGPoint(x:  rect.midX, y:rect.minY ))
        path.addArc( tangent1End: CGPoint(x: rect.maxX, y: rect.minY ),
                     tangent2End: CGPoint(x: rect.maxX, y: rect.maxY), radius: radius)
        path.addArc( tangent1End: CGPoint(x: rect.maxX, y: rect.maxY ),
                     tangent2End: CGPoint(x: rect.minX, y: rect.maxY), radius: radius)
        path.addArc( tangent1End: CGPoint(x: rect.minX, y: rect.maxY ),
                     tangent2End: CGPoint(x: rect.minX, y: rect.minY), radius: radius)
        path.addArc( tangent1End: CGPoint(x: rect.minX, y: rect.minY ),
                     tangent2End: CGPoint(x: rect.maxX, y: rect.minY), radius: radius)
        path.closeSubpath()
        context.setLineWidth(1.0)
        context.setFillColor(fillColor)
        context.setStrokeColor(borderColor)
        context.addPath(path)
        context.drawPath(using: .fillStroke)
    }
}

func attachments(from s: NSAttributedString) -> Array<NSTextAttachment> {
    var attachments: Array<NSTextAttachment> = []
    s.enumerateAttribute(.attachment, in: NSRange(0..<s.length)) {
        value, range, stop in
        guard let a = value else {
            return
        }
        attachments.append(a as! NSTextAttachment)
    }
    return attachments
}

let uuidPtrn = staticRegex(pattern: #"^[a-fA-F0-9]{8}(?:-[a-fA-F0-9]{4}){3}-[a-fA-F0-9]{12}$"#)

struct LinkTarget {
    let attachment: NSTextAttachment
    let target: Zid
}

func linkTargets(from attachments: Array<NSTextAttachment>) -> Array<LinkTarget> {
    func dataToZid(_ a: NSTextAttachment) -> LinkTarget? {
        if a.fileType != PAGE_ZETTEL_LINK_KIND {
            return nil
        }
        guard let contents = a.fileWrapper?.regularFileContents else {
            return nil
        }
        // a zid is a UUID and UUIDs are exactly 36 characters long
        if contents.count != 36 {
            return nil
        }
        guard let possibleZid = String(data: contents, encoding: .utf8) else {
            return nil
        }
        // after conversion from utf8 it damn well still better be 36 characters
        if possibleZid.count != 36 {
            return nil
        }
        let n = uuidPtrn.numberOfMatches(
            in: possibleZid, options: [], range: NSRange(location: 0, length: 36))
        if n != 1 {
            return nil
        }
        return LinkTarget(attachment: a, target: Zid(uuidString: possibleZid)!)
    }
    return withoutNil(attachments.map{ dataToZid($0) })
}
