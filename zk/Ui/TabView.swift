//
//  TabView.swift
//  zk
//
//  Created by Jeff Younker on 1/17/20.
//  Copyright © 2020 The Blobshop. All rights reserved.
//

import Cocoa
import Foundation

class TabView<X: Hashable>: NSTabView {
    var tabviewitems: Dictionary<X, NSTabViewItem> = [:]
    
    func add(index: X, controller: NSViewController) {
        let tab = NSTabViewItem()
        tab.label = "\(index)"
        tab.viewController = controller
        self.tabviewitems[index] = tab
        self.addTabViewItem(tab)
    }
    
    func setTab(index: X) {
        let v = tabviewitems[index]!
        self.selectTabViewItem(v)
    }
}
