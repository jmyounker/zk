//
//  KastenComboBox.swift
//  zk
//
//  Created by Jeff Younker on 12/29/19.
//  Copyright © 2019 The Blobshop. All rights reserved.
//

import Cocoa
import Foundation

import Logging

class KastenComboBox: NSComboBox, NSComboBoxDataSource, NSComboBoxDelegate {
    
    let log = Logger(label: "com.theblobshop.zk.combobox")
    @objc dynamic var selection: Zid?

    var defaultSelection: Zid!
    var selectionChangedListner: NSComboBoxDelegate!
    

    // Ensures the first selection attempt works.
    lazy var selected: Array<ZidTitle> = {
        let c = appDelegate.context
        return ZidTitle.from(zetteln: selectingFromDb(context: c, exactMatch: stringValue) + selectingFromDb(context: c, contains: stringValue))
    }()

    override func textDidChange(_ notification: Notification) {
        log.debug("=== textDidChange \(stringValue)")
        let c = appDelegate.context
        let approximateMatches = selectingFromDb(context: c, contains: stringValue)
        let exactMatch = selectingFromDb(context: c, exactMatch: stringValue)
        selected = ZidTitle.from(zetteln: exactMatch + approximateMatches)
        reloadData()
        // If the selection is cleared then we want to inform the upstream that the value has
        // changed. This triggers a child reset.
        if exactMatch.count > 0 {
            selection = exactMatch[0].zid!
            return
        }
        if selected.count == 1 {
            selection = selected[0].zid
        } else if selected.count == 0 {
            selection = nil
        } else {
            selection = selected[0].zid
        }
    }
    
    func selectingFromDb(context c: NSManagedObjectContext, contains: String) -> [Zettel] {
        let trimmedFilter = contains.trimmingCharacters(in: .whitespacesAndNewlines)
        if trimmedFilter == "" {
            return []
        }
            return Zettel.select(context: c, query:
                ZettelQuery(title: trimmedFilter, excludingTitle: trimmedFilter))
    }

    func selectingFromDb(context c: NSManagedObjectContext, exactMatch: String) -> [Zettel] {
        let trimmedFilter = exactMatch.trimmingCharacters(in: .whitespacesAndNewlines)
        return Zettel.select(context: c, query: ZettelQuery(exactTitle: trimmedFilter))
    }

    func comboBox(_ comboBox: NSComboBox, completedString string: String) -> String? {
        log.debug("=== completedString: \(string)")
        let matches = selected.filter{ $0.title.contains(string) }
        if matches.count == 1 {
            selection = matches[0].zid
            delegate?.comboBoxSelectionDidChange?(
                Notification(name: NSComboBox.selectionDidChangeNotification))
            return matches[0].title
        }
        return nil
    }
    
    func numberOfItems(in comboBox: NSComboBox) -> Int {
        log.debug("=== numberOfItems: \(selected.count)")
        return selected.count
    }
    
    func comboBox(_ comboBox: NSComboBox, objectValueForItemAt index: Int) -> Any? {
        log.info("=== objectValueForItemAt \(index)")
        if index < 0 {
            return nil
        }
        if index >= numberOfItems(in: comboBox) {
            return nil
        }
        return selected[index].title
    }
    
    func clear() {
        stringValue = ""
        removeAllItems()
        selected = []
        selection = nil
    }
    
    func set(zettel: Zettel?) {
        guard let z = zettel else {
            clear()
            return
        }
        set(zid: z.zid!, title: z.title!)
    }
    
    func set(zid: Zid, title: String) {
        stringValue = title
        selected = [ZidTitle(zid: zid, title: title)]
        selection = zid
        removeAllItems()
        addItem(withObjectValue: stringValue)
        selectItem(at: 0)
    }
    
    func comboBoxSelectionDidChange(_ notification: Notification) {
        let i = indexOfSelectedItem
        if i >= 0 && i < selected.count {
            selection = selected[i].zid
        } else {
            selection = nil
        }
    }
}

struct ZidTitle {
    // Used to reduce combox box interactions with CoreData

    let zid: Zid
    var title: String

    init(zettel z: Zettel) {
        self.zid = z.zid!
        self.title = z.title ?? "unknown"
    }
    
    init(zid: Zid, title: String) {
        self.zid = zid
        self.title = title
    }

    static func from(zetteln: Array<Zettel>) -> Array<ZidTitle> {
        return zetteln.map{ ZidTitle(zettel: $0) }
    }
}
