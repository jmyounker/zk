//
//  ZettelOutlineView.swift
//  zk
//
//  Created by Jeff Younker on 1/4/20.
//  Copyright © 2020 The Blobshop. All rights reserved.
//

import Cocoa
import Foundation

import Logging

class ZettelOutlineView: NSOutlineView, NSOutlineViewDataSource {
    let log = Logger(label: "com.theblobhop.zk.ZettelOutlineView")
    let debug = DebugController()
    
    static let titleColumnID = NSUserInterfaceItemIdentifier(rawValue: "TitleColumn")
    
    convenience init(screen: Scaler, frame: Dimensions) {
        self.init(frame: screen.rect(frame))
        let titleColumn = NSTableColumn(identifier: ZettelOutlineView.titleColumnID)
        titleColumn.width = screen.x(60)
        titleColumn.title = "Title"
        titleColumn.isEditable = false
        self.addTableColumn(titleColumn)
        self.dataSource = self
        self.outlineTableColumn = titleColumn
        self.registerForDraggedTypes([.URL])
    }
    
    var filterResults: FilterResults = FilterResults.empty
    
    func outlineView(_ outlineView: NSOutlineView, child index: Int, ofItem item: Any?) -> Any {
        log.info(">>>>>>>>>>>>>>>>> GOING TO CALL ITEM()")
        guard let r = self.item(parent: item as? Zid, child: index) else {
            log.info("MESSED UP INDEX!")
            fatalError("found messed up index")
        }
        log.info("CALLED ITEM() SUCCESSFULLY, GETTING ZID FROM ITEM")
        guard let z = r.zid else {
            log.info("MESSED UP ZID!")
            fatalError("found messed up zid")
        }
        log.info(">>>>>>>>>>>>>>>>> RETURNING: zid: \(z) (\(String(describing: r.zettel.title)))")
        // We return NSUUIDs so that outlineView(_acceptDrop:item:childIndex) can do an extra
        // retain as part of the horrible, horrible, not-very-good hack. Yay loose typing.
        return NSUUID(uuidString: z.uuidString)! as Any
    }
    
    func outlineView(_ outlineView: NSOutlineView, isItemExpandable item: Any) -> Bool {
        return count(zid: item as? Zid) != 0
    }
    
    func outlineView(_ outlineView: NSOutlineView, numberOfChildrenOfItem item: Any?) -> Int {
        return count(zid: item as? Zid)
    }

    func count(zid: Zid?) -> Int {
        guard let z = zid else {
            return filterResults.roots.count
        }
        return filterResults.node(zid: z)?.count ?? 0
    }
    
    func item(parent: Zid?, child i: Int) -> Result? {
        log.info(">>>>>>>>>>>>>>>>> CONVERTING INFO FOR parent \(String(describing: parent)) AND child \(i)")
        if i < 0 {
            return nil
        }
        guard let z = parent else {
            if i >= filterResults.roots.count {
                return nil
            }
            return filterResults.roots[i]
        }
        guard let r = filterResults.node(zid: z) else {
            return nil
        }
        if i >= r.count {
            return nil
        }
        return r.children[i]
    }
    
    func outlineView(
        _ outlineView: NSOutlineView,
        objectValueFor tableColumn: NSTableColumn?,
        byItem item: Any?) -> Any? {
        guard let column = tableColumn else {
            return nil
        }
        guard let zid = item as? Zid else {
            return nil
        }
        if column.identifier != NSUserInterfaceItemIdentifier(rawValue: "TitleColumn") {
            return nil
        }
        let t = filterResults.node(zid: zid)?.zettel.title ?? "unknown"
        if debug.isOn {
            let s = filterResults.node(zid: zid)?.zettel.seq ?? -42
            return "\(t): \(s)"
        }
        return t
    }
    
    func selected() -> Zettel? {
        guard let zid = self.item(atRow: self.selectedRow) as? Zid else {
            log.info("cannot get selected zettel: cannot read zid at row \(self.selectedRow)")
            return nil
        }
        return filterResults.node(zid: zid)?.zettel
    }

    func openTo() {
        guard let zettel = self.selected() else {
            return
        }
        openTo(zettel: zettel.parent)
    }
    
    func openTo(zid: Zid) {
        guard let zettel = self.filterResults.node(zid: zid)?.zettel else {
            return
        }
        openTo(zettel: zettel.parent)
    }
    
    func openTo(zettel: Zettel?) {
        guard let z = zettel else {
            return
        }
        expandToRoot(zettel: z.parent)
    }
    
    func expandToRoot(zettel: Zettel?) {
        guard let z = zettel else {
            return
        }
        expandToRoot(zettel: z.parent)
        self.expandItem(z.zid!)
    }
    
    func setSelected(zettel: Zettel?) {
        guard let z = zettel else {
            return
        }
        if filterResults.node(zid: z.zid!) == nil {
            return
        }
        let i = self.row(forItem: z.zid!)
        self.selectRowIndexes(IndexSet(integer: i), byExtendingSelection: false)
    }
}

extension ZettelOutlineView {
    
    override func performDragOperation(_ sender: NSDraggingInfo) -> Bool {
        log.info(">>>>>>>>>>>> PERFORMING DRAG OPERATION")
        let r = super.performDragOperation(sender)
        log.info(">>>>>>>>>>>> DRAG OPERATION COMPLETE")
        return r
    }

    override func canDragRows(with rowIndexes: IndexSet, at mouseDownPoint: NSPoint) -> Bool {
        log.info(">>>>>>>>>>>> CHECKING IF I CAN DRAG")
        // Most things will be draggable. Some top level things will not.
        // If the selected rows contain something undraggable, then nothing will be draggable.
        for row in rowIndexes {
            log.debug(">>>>>>>>>>>> CHECKING IF I CAN DRAG \(row)")
            guard let zid = self.item(atRow: row) as? Zid else {
                fatalError("fatal error: outlines should never return anything other than a Zid")
            }
            guard let r = filterResults.node(zid: zid) else {
                log.error("cannot find zettel: zid \(zid) not in filter results")
                return false
            }
            if !r.zettel.movable {
                log.info(">>>>>>>>>>>> CANNOT DRAG \(String(describing: r.zettel.title))")
                return false
            }
            log.info(">>>>>>>>>>>> CAN DRAG \(String(describing: r.zettel.title))")
        }
        log.info(">>>>>>>>>>>> CAN DRAG ROWS!")
        return true
    }
    
    func outlineView(_ outlineView: NSOutlineView, pasteboardWriterForItem item: Any) -> NSPasteboardWriting? {
        log.info(">>>>>>>>>>>> TRYING TO DRAG \(item)")
        guard let r = filterResults.node(zid: item as! Zid) else {
            log.error("cannot find zettel: zid \(item) not in filter results")
            return nil
        }

        log.info(">>>>>>>>>>>> TRYING TO DRAG WITH URL \(r.zettel.url)")
        return r.zettel.url as NSURL
    }

    func outlineView(_ outlineView: NSOutlineView, draggingSession session: NSDraggingSession, willBeginAt screenPoint: NSPoint, forItems draggedItems: [Any]) {
        log.info(">>>>>>>>>>>> DOING SOME DRAGGING")
    }

    func outlineView(_ outlineView: NSOutlineView, draggingSession session: NSDraggingSession, endedAt screenPoint: NSPoint, operation: NSDragOperation) {
        log.info(">>>>>>>>>>>> DRAGGING HAS ENDED: \(operation)")
    }

    func outlineView(_ outlineView: NSOutlineView, writeItems items: [Any], to pasteboard: NSPasteboard) -> Bool {
        log.info(">>>>>>>>>>>> PUT IMAGES TO PASTEBOARD")
        return true
    }

    func outlineView(_ outlineView: NSOutlineView, updateDraggingItemsForDrag draggingInfo: NSDraggingInfo) {
        log.info(">>>>>>>>>>>> UPDATE DRAGGING")
    }
    
    func outlineView(_ outlineView: NSOutlineView, validateDrop info: NSDraggingInfo, proposedItem item: Any?, proposedChildIndex index: Int) -> NSDragOperation {
        log.info(">>>>>>>>>>>> SELECT OPERATION .MOVE")
        return NSDragOperation([.move])
    }

    // Implement drag accept operation.
    //
    // This code has a HORRRIBLE HACK. The function outlineView.performDragOperation() assumes that it
    // owns `item` and appears to do a release. This results in a zombie. Therefore this function's
    // last action is to increment the the retain count.
    //
    // I obviously do not understand something about how this is supposed to work, but this hack
    // appears to work, although I imagine it might leak elsewhere. ¯\_(ツ)_/¯
    //
    // Also, note to future self: you are entitled to kick past self's ass for this stupidity.
    func outlineView(_ outlineView: NSOutlineView, acceptDrop info: NSDraggingInfo, item: Any?, childIndex index: Int) -> Bool {
        log.info(">>>>>>>>>>>> DO DRAG OPERATION")
        guard let url = NSURL(from: info.draggingPasteboard) else {
            log.info("cannot complete drag: cannot convert pasteboard to URL")
            return false
        }
        let c = appDelegate.context
        let parent = Zettel.select(context: c, zid: item as? Zid);
        let parentZid = parent?.zid
        assert ((parent != nil && parentZid != nil) || (parent == nil && parentZid == nil))
        do {
            let zurl = try ZidURL(url: url as URL)
            guard let z = Zettel.select(context: appDelegate.context, zid: zurl.zid) else {
                log.info("cannot complete drag: cannot find zettel \(zurl.zid)")
                return false
            }
            log.info(">>>>>>>>>>>> DO DRAG OPERATION TO [\(url) (\(String(describing:item))):\(index)]")
            // DEBUG(jyounker): I THINK ITEM IS WHATS GETTING RELEASED
            if z == parent {
                log.info("cannot complete drag: source and dest parent are the same node")
                return false
            }
            if try Zettel.areParentAndChild(parent: z, child: parent) {
                log.info("cannot complete drag: source cannot become it's own child")
                return false
            }
            log.info(">>>>>>>>>>>>>> GOING TO MOVE ZETTEL \(z.zid!)")
            let seq = try insertionSeq(context: c, parent: parent, at: index)
            log.info(">>>>>>>>>>>>>>>>> INDEX: \(index) SEQ: \(seq)")
            appDelegate.unitOfWork().execute(with: .reorganizeZetteln) { c in
                try Zettel.reparent(context: c, zid: z.zid!, parent: parentZid, at: seq)
            }
            log.info(">>>>>>>>>>>>>>>>> DRAG SAVE DONE")
            if let uuid = item as? NSUUID {
                log.info(">>>>>>>>>>>>>>>>> RETAINING NSUUID")
                uuid.retainMe()
            }
            DispatchQueue.main.async {
                self.log.info(">>>>>>>>>>>>>>>>> ABOUT TO DO RELOAD")
                outlineView.reloadData()
                self.log.info(">>>>>>>>>>>>>>>>> RELOAD DONE")
            }
            log.info(">>>>>>>>>>>>>>>>> DONE WITH DRAG OPERATION")
            return true
        } catch {
            log.info("cannot complete drag: \(error)")
            return false
        }
    }
    
    func outlineView(_ outlineView: NSOutlineView, namesOfPromisedFilesDroppedAtDestination dropDestination: URL, forDraggedItems items: [Any]) -> [String] {
        log.info(">>>>>>>>>>>> LOOK MA! I'M A FILE DESTINATION RETURNING []")
        return []
    }

    func insertionSeq(context c: NSManagedObjectContext, parent: Zettel?, at index: Int) throws -> Int64 {
        let children = self.filterResults.children(zid: parent?.zid!)
        if index < 0 {
            let last = try lastChildZettel(context: c, parentZid: parent?.zid!)
            if last == nil {
                return 0
            }
            return last!.seq + 1
        } else if index == 0 && children.count == 0 {
            let last = try lastChildZettel(context: c, parentZid: parent?.zid!)
            if last == nil {
                return 0
            }
            return last!.seq + 1
        } else if index == 0 {
            return max(0, children[index].seq - 1)
        } else if index >= children.count {
            return children[children.count - 1].seq + 1
        } else {
            return children[index].seq
        }
    }
}
