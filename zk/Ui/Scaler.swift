//
//  Scaler.swift
//  zk
//
//  Created by Jeff Younker on 12/25/19.
//  Copyright © 2019 The Blobshop. All rights reserved.
//

import Cocoa
import Foundation

class Scaler {
    // Dimensions of the screen in pixels per mm. Class generates points, sizes, and rects
    // specified in unitx of MM.
    let x_ppmm: CGFloat  // x resolution in Pixels Per Millimeter
    let y_ppmm: CGFloat  // y resolution in Pixels Per Millimeter

    static let mmPerInch: CGFloat = 25.4

    init(x: CGFloat , y: CGFloat) {
        self.x_ppmm = x
        self.y_ppmm = y
    }

    static func from(_ screen: NSScreen) -> Scaler {
        guard let resolution = screen.deviceDescription[NSDeviceDescriptionKey.resolution] as? NSValue else {
            // Default is assumed by system to be 72dpi
            return Scaler(x: 72.0 / mmPerInch, y: 72.0 / mmPerInch)
        }
        return Scaler(
            x: resolution.sizeValue.width / mmPerInch,
            y: resolution.sizeValue.height / mmPerInch)
    }
    
    func unit() -> NSSize {
        return NSMakeSize(x_ppmm, y_ppmm)
    }

    func point(_ x_mm: CGFloat, _ y_mm: CGFloat) -> NSPoint {
        return NSPoint(x: x_ppmm * x_mm, y: y_ppmm * y_mm)
    }
    
    func size(_ w_mm: CGFloat, _ h_mm: CGFloat) -> NSSize{
        return NSMakeSize(x_ppmm * w_mm, y_ppmm * h_mm)
    }
    
    func rect(_ x_mm: CGFloat, _ y_mm: CGFloat, _ w_mm: CGFloat, _ h_mm: CGFloat) -> NSRect {
        return NSMakeRect(x_ppmm * x_mm, y_ppmm * y_mm, x_ppmm * w_mm, y_ppmm * h_mm)
    }
    
    func point(_ point: Dimensions) -> NSPoint {
        return NSPoint(x: x_ppmm * point.x, y: y_ppmm * point.y)
    }

    func size(_ size: Dimensions) -> NSSize {
        return NSMakeSize(x_ppmm * size.x, y_ppmm * size.y)
    }

    func rect(_ rect: Dimensions) -> NSRect {
        return NSMakeRect(0, 0, x_ppmm * rect.x, y_ppmm * rect.y)
    }
    
    func x(_ mm: CGFloat) -> CGFloat {
        return x_ppmm * mm
    }

    func y(_ mm: CGFloat) -> CGFloat {
        return y_ppmm * mm
    }
    
    func x(_ dim: Dimensions) -> CGFloat {
        return x(dim.x)
    }

    func y(_ dim: Dimensions) -> CGFloat {
        return y(dim.y)
    }
    
    func border(width: CGFloat) -> Border {
        return Border(
            left: x(width),
            right: x(width),
            top: y(width),
            bottom: y(width))
    }
}

class Dimensions {
    let x: CGFloat  // In mm
    let y: CGFloat  // In mm
    
    init(x: CGFloat, y: CGFloat) {
        self.x = x
        self.y = y
    }
}

struct Border {
    let left: CGFloat
    let right: CGFloat
    let top: CGFloat
    let bottom: CGFloat
}

