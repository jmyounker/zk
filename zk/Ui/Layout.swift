//
//  Layout.swift
//  zk
//
//  Created by Jeff Younker on 12/30/19.
//  Copyright © 2019 The Blobshop. All rights reserved.
//

import Cocoa
import Foundation

func sideBySideView(left: NSView, right: NSView) -> NSView {
    let view = NSStackView()
    view.orientation = .horizontal
    view.alignment = .firstBaseline
    view.addArrangedSubview(left)
    view.addArrangedSubview(right)
    return view
}

func sideBySideView(leadingToTrailing views: Array<NSView>) -> NSView {
    let view = NSStackView()
    view.orientation = .horizontal
    view.alignment = .firstBaseline
    for v in views {
        view.addArrangedSubview(v)
    }
    return view
}

func fullWidthConstraints(child: NSView, minWidth: CGFloat) -> [NSLayoutConstraint] {
    return NSLayoutConstraint.constraints(
        withVisualFormat: "H:|-[child(>=minWidth)]-|",
        options: [],
        metrics: ["minWidth": minWidth],
        views: ["child": child])
}

func fullWidthConstraints(child: NSView) -> [NSLayoutConstraint] {
    return NSLayoutConstraint.constraints(
        withVisualFormat: "H:|-[child]-|",
        options: [],
        metrics: nil,
        views: ["child": child])
}

func bindToRightConstraints(child: NSView) -> [NSLayoutConstraint] {
    return NSLayoutConstraint.constraints(
        withVisualFormat: "H:[child]-|",
        options: [],
        metrics: nil,
        views: ["child": child])
}

func bindToBottomConstraints(child: NSView) -> [NSLayoutConstraint] {
    return NSLayoutConstraint.constraints(
        withVisualFormat: "V:[child]-|",
        options: [],
        metrics: nil,
        views: ["child": child])
}

func hugContentTightly<X: NSView>(_ view: X) -> X {
    view.setContentHuggingPriority(
        NSLayoutConstraint.Priority(rawValue: 600), for: .horizontal)
    view.setContentHuggingPriority(
        NSLayoutConstraint.Priority(rawValue: 600), for: .vertical)
    return view
}

func fullWidth(child: NSView, parent: NSView) {
    parent.addConstraints(NSLayoutConstraint.constraints(
        withVisualFormat: "H:|-[child]-|",
        options: [],
        metrics: nil,
        views: ["child": child]))
}

func fullWidth(child: NSView, parent: NSView, minWidth width: CGFloat) {
    parent.addConstraints(NSLayoutConstraint.constraints(
        withVisualFormat: "H:|-[child(>=width)]-|",
        options: [],
        metrics: ["width": width],
        views: ["child": child]))
}

func fullHeight(child: NSView, parent: NSView) {
    parent.addConstraints(NSLayoutConstraint.constraints(
        withVisualFormat: "V:|-[child]-|",
        options: [],
        metrics: nil,
        views: ["child": child]))
}

func fullHeight(child: NSView, parent: NSView, minHeight height: CGFloat) {
    parent.addConstraints(NSLayoutConstraint.constraints(
        withVisualFormat: "H:|-[child(>=height)]-|",
        options: [],
        metrics: ["height": height],
        views: ["child": child]))
}

func alignLeading(child: NSView, parent: NSView) {
    child.leadingAnchor.constraint(equalTo: parent.leadingAnchor).isActive = true
}

func alignTrailing(child: NSView, parent: NSView) {
    child.trailingAnchor.constraint(equalTo: parent.trailingAnchor).isActive = true
}

func alignBottom(child: NSView, parent: NSView) {
    child.bottomAnchor.constraint(equalTo: parent.bottomAnchor).isActive = true
}

func withoutResizeMask<X>(_ x: X) -> X where X: NSView {
    x.translatesAutoresizingMaskIntoConstraints = false
    return x
}

func center(child: NSView, within parent: NSView) {
    centerX(child: child, within: parent)
    centerY(child: child, within: parent)
}

func centerX(child: NSView, within parent: NSView) {
    child.centerXAnchor.constraint(equalTo: parent.centerXAnchor).isActive = true
}

func centerY(child: NSView, within parent: NSView) {
    child.centerYAnchor.constraint(equalTo: parent.centerYAnchor).isActive = true
}

func frame(child: NSView, within parent: NSView, border: Border) {
    let views = ["child": child]
    let metrics = [
        "leftBorder": border.left,
        "rightBorder": border.right,
        "topBorder": border.top,
        "bottomBorder": border.bottom,
    ]
    parent.addConstraints(NSLayoutConstraint.constraints(
        withVisualFormat: "H:|-leftBorder-[child]-rightBorder-|",
        options: [],
        metrics: metrics,
        views: views))
    parent.addConstraints(NSLayoutConstraint.constraints(
        withVisualFormat: "V:|-topBorder-[child]-bottomBorder-|",
        options: [],
        metrics: metrics,
        views: views))
}
