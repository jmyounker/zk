//
//  OutlineSearch.swift
//  zk
//
//  Created by Jeff Younker on 12/8/19.
//  Copyright © 2019 The Blobshop. All rights reserved.
//

import CoreData
import Foundation

func performSearch(context c: NSManagedObjectContext, filter: FilterCriteria) throws -> FilterResults {
    let zetteln = Zettel.select(context: c, query: zettelQuery(filter: filter))
    return try completeResults(context: c, zetteln)
}

func zettelQuery(filter: FilterCriteria) -> ZettelQuery {
    let tags = filter.tags ?? []
    if tags == [] && filter.text == "" {
        return ZettelQuery()
    }
    return ZettelQuery(tags: tags, fullText: filter.text)
}

class FilterResults {
    let roots: Array<Result>
    let nodes: Dictionary<UUID, Result>
    
    init(roots: Array<Result>, nodes: Dictionary<UUID, Result>) {
        self.roots = roots
        self.nodes = nodes
    }
    
    static let empty = FilterResults(roots: [], nodes: [:])

    func node(zid: UUID) -> Result? {
        return self.nodes[zid]
    }

    func numberChildren(zid: UUID?) -> Int {
        guard let pz = zid else {
            return roots.count
        }
        return self.nodes[pz]!.children.count
    }
    
    func children(zid: UUID?) -> [Zettel] {
        guard let pz = zid else {
            return roots.map{ $0.zettel }
        }
        guard let r = self.nodes[pz] else {
            return []
        }
        return r.children.map{ $0.zettel }
    }
}

func completeResults(context c: NSManagedObjectContext, _ found: Array<Zettel>) throws -> FilterResults {
    let derived = try missingParents(context: c, found)
    let all = derived.map{ Result(kind: .implied, zettel: $0) } + found.map{ Result(kind: .found, zettel: $0) }
    for x in all {
        if x.zettel.parent != nil && x.zettel.parent_zid == nil {
            Zettel.log.info("PARENT IS BROKEN")
        }
    }
    let (roots, notRoots) = partition(all){ $0.zettel.parent == nil }
    let rootZids = roots.map{ $0.zettel.zid! }
    var treeRoots = indexByUniqueKey(roots){ $0.zettel.zid! }
    let tree = buildTree(&treeRoots, notRoots)
    return FilterResults(roots: rootZids.map{ tree[$0]! }, nodes: tree)

}

func buildTree(_ tree: inout Dictionary<UUID, Result>, _ remainder: Array<Result>) -> Dictionary<UUID, Result> {
    if remainder.count == 0 {
        return tree
    }
    let (thisLayer, deeperLayers) = partition(remainder){ tree.keys.contains($0.zettel.parent_zid!) }
    for x in thisLayer {
        tree[x.zettel.parent_zid!]?.children.append(x)
        tree[x.zettel.zid!] = x
    }
    return buildTree(&tree, deeperLayers)
}

func missingParents(context c: NSManagedObjectContext, _ children: Array<Zettel>, _ previouslyKnown: Set<UUID> = Set()) throws -> Array<Zettel> {
    let parentZids = Set(withoutNil(children.map{ $0.parentZid }))
    let childZids = Set(children.map{ $0.zid! })
    let knownZids = previouslyKnown.union(childZids)
    let missingZids = parentZids.subtracting(knownZids)
    if missingZids.count == 0 {
        return []
    }
    let missing = Zettel.select(context: c, query: ZettelQuery(zids: missingZids.map{$0}))
    return missing + (try missingParents(context: c, missing, knownZids))
}

func rootsOf(_ zetteln: Array<Zettel>) -> Array<Zettel> {
    return zetteln.filter{ $0.parentZid == nil }
}

enum ResultKind {
    case found
    case implied
}

@objc(Result)
class Result: NSObject {
    var kind: ResultKind
    @objc dynamic var children: Array<Result> = []
    @objc dynamic var zettel: Zettel
    
    init(kind: ResultKind, zettel: Zettel) {
        self.kind = kind
        self.zettel = zettel
    }
    
    @objc dynamic var zid: UUID? {
        get {
            return self.zettel.zid
        }
    }
    
    @objc dynamic var isLeaf: Bool {
        get {
            return children.count == 0
        }
    }
    
    @objc dynamic var count: Int {
        get {
            return children.count
        }
    }
    
    override var description: String {
        get {
            return "Result(kind: \(kind), zettel.zettel:\(String(describing: self.zettel.description)))"
        }
    }
}


