//
//  Clicker.swift
//  zk
//
//  Created by Jeff Younker on 11/30/19.
//  Copyright © 2019 The Blobshop. All rights reserved.
//

import Cocoa
import Foundation

enum ClickState {
    case unclicked
    case clicked(timerId: Int64)
}

// The Clicker decides whether mouse clicks represent a single click or a double click.
// When it decides, it calls the appropriate functions.
//
// You set up a clicker to point to your single and double click methods, and then whenever
// you receive a mouse click, you call Clicker.click(). It does the rest.
class Clicker {
    var performSingleClick: ()->Void
    var performDoubleClick: ()->Void
    var state: ClickState = .unclicked
    var mutex = PThreadMutex()
    
    init(singleClick: @escaping ()->Void, doubleClick: @escaping ()->Void ) {
        performSingleClick = singleClick
        performDoubleClick = doubleClick
    }
    
    func click() {
        self.mutex.sync {
            switch state {
            case .unclicked:
                let id = Int64.random(in: 0 ..< Int64.max)
                DispatchQueue.main.asyncAfter(deadline: .now() + NSEvent.doubleClickInterval) {
                    self.timerFired(id)
                }
                state = .clicked(timerId: id)
                ;
            case .clicked:
                state = .unclicked
                // TODO(jyounker): Run on another thread.
                performDoubleClick()
                ;
            }
        }
    }
    
    func timerFired(_ firedTimerId: Int64) {
        mutex.sync {
            switch state {
            case .unclicked:
                // Possibly race with a double click. Do nothing!
                state = .unclicked
                ;
            case .clicked(let activeTimerId):
                // It's possible to have a race between a clicks and timer expirations. If the
                // this isn't the active clikc then we just return as the click has already be
                // handled.
                if firedTimerId != activeTimerId {
                    return
                }
                // The timer expired before we got a second click, so we're processing a double
                // click.
                state = .unclicked
                // TODO(jyounker): Run on another thread.
                performSingleClick()
                ;
            }
        }
    }
}

public class PThreadMutex {
   var mutex: pthread_mutex_t
   
   public init() {
        self.mutex = pthread_mutex_t()
   }
   
   public func sync<R>(execute: () throws -> R) rethrows -> R {
      pthread_mutex_lock(&mutex)
      defer { pthread_mutex_unlock(&mutex) }
      return try execute()
   }
}
