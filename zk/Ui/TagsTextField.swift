//
//  TagsTextField.swift
//  zk
//
//  Created by Jeff Younker on 1/2/20.
//  Copyright © 2020 The Blobshop. All rights reserved.
//

import Cocoa
import Foundation

class TagsTextField: NSTextField {
    var tags: Array<TagName> {
        get {
            return stringValue.components(separatedBy: ",")
                .map{ $0.trimmingCharacters(in: .whitespaces) }
                .filter{ $0 != "" }
                .sorted()
        }
        set(x) {
            stringValue = x.sorted().joined(separator: ", ")
            self.isEditable = true
        }
    }
    
}
