//
//  Color.swift
//  zk
//
//  Created by Jeff Younker on 8/6/20.
//  Copyright © 2020 The Blobshop. All rights reserved.
//

import Cocoa
import Foundation

class Color {
    static let green = NSColor(srgbRed: 0.3, green: 0.7, blue: 0.3, alpha: 1)
    static let red = NSColor(srgbRed: 0.95, green: 0.3, blue: 0.3, alpha: 1)
}
