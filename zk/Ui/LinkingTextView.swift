//
//  LinkingTextView.swift
//  zk
//
//  Created by Jeff Younker on 11/18/19.
//  Copyright © 2019 The Blobshop. All rights reserved.
//

import Cocoa
import Foundation

import Logging

struct PreviousEdit {
    let index: Int
}

class EditSequence {
    var edits = Array<PreviousEdit>()
    init(count: Int) {
        for _ in 0..<count {
            edits.append(PreviousEdit(index: 0))
        }
    }
 
    var count: Int {
        get { edits.count }
    }
 
    func update(_ s: String, position: Int) {
        for i in 1..<edits.count {
            edits[i-1] = edits[i]
        }
        if position == 0 {
            edits[edits.count-1] = PreviousEdit(index: 0)
            return
        }
        edits[edits.count-1] = PreviousEdit(index: max(0, position-1))
    }
    
    func allSequential() -> Bool {
        let first = edits[0].index
        for (i, edit) in edits.enumerated() {
            if edit.index - i != first {
                return false
            }
        }
        return true
    }

    var range: NSRange {
        get {
            return NSRange(location: edits[0].index, length: edits.count)
        }
    }
}

struct LinkCompletion {
    let name: String
    let zid: Zid
}

class LinkingTextView: NSTextView {
    var edits = EditSequence(count: 6)
    var editMode = EditMode.normal
    var log = Logger(label: "com.theblobshop.zk.linkingtextview")
    // This should only be poulated during edit mode == completion
    var completionMode = CompletionMode.normal
    var linkCompletions: [String: Zettel]?
    
    // Change tracking
    var hasChanged = false

    override func didChangeText() {
        self.hasChanged = true
        
        switch editMode {
        case .normal:
            super.didChangeText()
            checkForCompletion()
            ;
        case .completion:
            super.didChangeText()
            ;
        }
    }
    
    func checkForCompletion() {
        let sr = self.selectedRange()
        if sr.length != 0 {
            print("Not insertion point!")
            return
        }
        edits.update(self.string, position: sr.location)
        if !edits.allSequential() {
            return
        }
        guard let lr = linkRange(n: edits.count) else {
            return
        }
        let ls = self.string[Range(lr, in: self.string)!]
        self.complete(self, selection: lr, selected: String(ls))
    }

    func complete(_ sender: Any?, selection: NSRange, selected: String) {
        self.editMode = .completion
        defer{ self.editMode = .normal }
        completionMode = .link(selection: selection, selected: selected)
        defer{
            completionMode = .normal
            linkCompletions = [:]
        }
        super.complete(sender)
    }

    override func complete(_ sender: Any?) {
        self.editMode = .completion
        defer{ self.editMode = .normal }
        completionMode = .normal
        let sr = self.selectedRange()
        if sr.length != 0 {
            super.complete(sender)
            return
        }
        guard let lr = linkRange(n: 20) else {
            super.complete(sender)
            return
        }
        let ls = self.string[Range(lr, in:self.string)!]
        completionMode = .link(selection: lr, selected: String(ls))
        defer{ completionMode = .normal }
        super.complete(sender)
    }
    
    func linkRange(n: Int) -> NSRange? {
        let linkEnd = selectedRange()
        if linkEnd.length != 0 {
            return nil
        }
        let linkStart = max(0, linkEnd.location - n)
        let linkLength = linkEnd.location - linkStart
        let linkRange = NSRange(location: linkStart, length: linkLength)
        return linkPrefix(s: self.string, range: linkRange)
    }
    
    override dynamic var rangeForUserCompletion: NSRange {
        get {
            switch completionMode {
            case .link(let selection, _): return selection ;
            case .normal: return super.rangeForUserCompletion ;
            }
        }
    }

    override func completions(
        forPartialWordRange: NSRange,
        indexOfSelectedItem: UnsafeMutablePointer<Int>) -> [String]? {
        print("forPartialWordRange: \(forPartialWordRange)")
        switch completionMode {
            case .normal: return super.completions(forPartialWordRange: forPartialWordRange, indexOfSelectedItem: indexOfSelectedItem) ;
            case .link:
                return retrieveLinkCompletions(selected: searchPortion(self.string, range: forPartialWordRange))
                ;
        }
    }

    func searchPortion(_ s: String, range: NSRange) -> String {
        let start = s.index(s.startIndex, offsetBy: range.location + 2)
        let end = s.index(start, offsetBy: range.length - 2)
        return String(s[start..<end])
    }
    
    func retrieveLinkCompletions(selected: String) -> Array<String> {
        let zetteln = Zettel.select(
            context: appDelegate.context, query: ZettelQuery(title: selected))
        var zettelByZid = Dictionary<String, Zettel>()
        for z in zetteln {
            zettelByZid[z.title!] = z
        }
        linkCompletions = zettelByZid
        return zettelByZid.keys.map{ $0 }
    }
    
    override func insertCompletion(
        _ word: String,
        forPartialWordRange charRange: NSRange,
        movement: Int,
        isFinal: Bool) {
        switch completionMode {
        case .normal:
            return super.insertCompletion(word, forPartialWordRange: charRange, movement: movement, isFinal: isFinal)
            ;
        case .link(_, let selected):
            if !isFinal {
                return super.insertCompletion("[[\(word)]]", forPartialWordRange: charRange, movement: movement, isFinal: false)
            }
            if word == selected {
                // The user abandoned the selection, so use the original replacement mechanism
                super.insertCompletion(word, forPartialWordRange: charRange, movement: movement, isFinal: true)
            } else {
                // The user has chosen something, so we put in a link.
                guard let zettel = linkCompletions?[word] else {
                    fatalError("should never be possible to get here without setting zbt")
                }
                // Let their logic take care of cleaning up and adjusting the spacing by replacing the
                // expansion with a fixed string. Then we replace that with the tag.
                super.insertCompletion("zid", forPartialWordRange: charRange, movement: movement, isFinal: true)
                let replacementRange = NSRange(location: charRange.location, length: 3)
                insertLink(zettel, at: replacementRange)
            }
            // Finally, when done, we throw away the active set of links
            ;
        }
    }

    func insertLink(_ target: Zettel, at range: NSRange) {
        let link = linkAttachment(
            label: target.title!, zid: target.zid!.uuidString, kind: PAGE_ZETTEL_LINK_KIND)
        let linkString = NSAttributedString(attachment: link)
        self.insertText(linkString, replacementRange: range)
    }
}

extension LinkingTextView {
    func setPageContents(page: Page?) {
        guard let page = page else {
            self.string = ""
            return
        }
        switch page.contentType {
        case .rtfd:
            // TODO(jyounker): This is s a mess of data possibilities. I'm too tired to reason right now.
            guard let data = page.data else {
                log.error("did not update view: page data missing or could not be parsed")
                self.string = ""
                return
            }
            self.replaceCharacters(in: wholeString(self.string), withRTFD: data as Data)
            do {
                try reestablishLinks(context: appDelegate.context, text: self.attributedString())
            } catch {
                log.error("not updating links: \(error)")
                return
            }
        ;
        case .other: self.string = "" ;
        }
        self.hasChanged = false
    }
    
    func setContent(to s: NSAttributedString?) {
        let ts = self.textStorage!
        ts.beginEditing()
        defer { ts.endEditing() }
        _ = ts.replaceCharacters(in: wholeString(ts.string), with: s ?? NSAttributedString(string: ""))
    }
}

let PAGE_ZETTEL_LINK_KIND = "public.plain-text"

enum EditMode {
    case normal
    case completion
}

enum CompletionMode {
    case normal
    case link(selection: NSRange, selected: String)
}

public func linkPrefix(s:String, range: NSRange) -> NSRange? {
    let pattern = #"(\[\[[^\[\]\n\r]+)$"#
    let regex = staticRegex(pattern: pattern, options: [.dotMatchesLineSeparators])
    let matches = regex.matches(in: s, range: range)
    if matches.count == 0 {
        return nil
    }
    let match = matches[0]
    if match.numberOfRanges != 2 {
        return nil
    }
    return match.range(at: 1)
}

extension String {
    var whole: NSRange {
        return NSRange(self.startIndex..., in: self)
    }
}
