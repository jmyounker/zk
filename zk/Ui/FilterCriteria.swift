//
//  FilterCriteria.swift
//  zk
//
//  Created by Jeff Younker on 12/23/19.
//  Copyright © 2019 The Blobshop. All rights reserved.
//

import Foundation

struct FilterCriteria {
    let text : String
    let tags: Array<TagName>?
}

func parseFilterCriteria(search: String) throws -> FilterCriteria {
    // A search string contains free-form text and/or tags. Tags are surrounded by double
    // parenthesis. E.g. "some test to search for ((T1)) ((T2))". This will find all zettel
    // containing "some text to search for" which also have both tags T1 and T2.
    // Whitespace is trimmed from the beginning and end of the search text and also each
    // tag.
    let text = search.trimmingCharacters(in: .whitespaces)
    let wholeText = NSRange(text.startIndex..<text.endIndex, in: text)
    let tags = try NSRegularExpression(
        pattern: #"\(\(([^)]+)\)\)"#, options: []).capturedStrings(in: text, range: wholeText)
    let searchText = text.replacingOccurrences(
        of: #"(\s*\(\([^)]+\)\)\s*)"#,
        with: "",
        options: .regularExpression,
        range: Range(wholeText, in: text))
    return FilterCriteria(text: searchText, tags: tags.map{ TagName($0.trimmingCharacters(in: .whitespaces)) })
}
