//
//  Collection.swift
//  zk
//
//  Created by Jeff Younker on 1/27/20.
//  Copyright © 2020 The Blobshop. All rights reserved.
//

import Foundation

class Queue<X> {
    init(from values: Array<X> = []) {
        self.values = values
    }
    
    var values: Array<X> = []
    
    var count: Int {
        return values.count
    }
    
    var isEmpty: Bool {
        return values.count == 0
    }

    func push(_ x: X) {
        values.append(x)
    }
    
    func pop() -> X? {
        if values.count == 0 {
            return nil
        }
        let head = values[0]
        values.remove(at: 0)
        return head
    }
}

func zip<X>(_ x1: Array<X>, _ x2: Array<X>) -> Array<X> {
    var r = Array<X>()
    var i1 = 0
    var i2 = 0
    while (i1 < x1.count && i2 < x2.count) {
        if i1 < x1.count {
            r.append(x1[i1])
            i1 += 1
        }
        if i2 < x2.count {
            r.append(x2[i2])
            i2 += 1
        }
    }
    while (i1 < x1.count) {
        r.append(x1[i1])
        i1 += 1
    }
    while (i2 < x2.count) {
        r.append(x2[i2])
        i2 += 1
    }
    return r
}

func take<X>(_ n: Int, from seq: Array<X>) -> Array<X> {
    if seq.count == 0 {
        return []
    }
    var r = Array<X>()
    for i in 0..<min(n, seq.count) {
        r.append(seq[i])
    }
    return r
}
