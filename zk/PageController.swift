//
//  PageController.swift
//  zk
//
//  Created by Jeff Younker on 12/29/19.
//  Copyright © 2019 The Blobshop. All rights reserved.
//

import Cocoa
import Foundation

import Logging

class PageView: NSTabView {
    var screen: Scaler!
    var frontView: PageCont!
    var backView: PageCont!
    var tabViews: Dictionary<String, NSTabViewItem> = [:]
    private var editable: Bool = true

    let log = Logger(label: "com.theblobshop.zk.PageController")

    func configure(screen: Scaler) {
        self.screen = screen
        frontView = PageCont()
        backView = PageCont()
    }
    
    func loadView() {
    }

    func createView(_ screen: Scaler) -> NSView {
        // Add front view.
        frontView.createView(screen)
        let frontViewItem = createTabItem(name: "front", controller: frontView)
        frontViewItem.toolTip = Localized("page-front-tab-tooltip")
        self.addTabViewItem(frontViewItem)

        // Add back view.
        backView.createView(screen)
        let backViewItem = createTabItem(name: "back", controller: backView)
        backViewItem.toolTip = Localized("page-back-tab-tooltip")
        self.addTabViewItem(backViewItem)

        return self
    }

    func createTabItem(name: String, controller: NSViewController) -> NSTabViewItem {
        let tab = NSTabViewItem()
        tab.label = name
        tab.viewController = controller
        self.tabViews[name] = tab
        return tab
    }
    
    func title() -> String? {
        return extractTitle(front: frontView.page.string)
    }
    
    func hasFrontPage() -> Bool {
        return frontView.page.string.trimmingCharacters(in: .whitespacesAndNewlines).count > 0
    }
    
    func hasBackPage() -> Bool {
        return backView.page.string.trimmingCharacters(in: .whitespacesAndNewlines).count > 0
    }
    
    func frontPage() -> NSAttributedString {
        return frontView.page.attributedString()
    }

    func backPage() -> NSAttributedString {
        return backView.page.attributedString()
    }

    func backPageOrNil() -> NSAttributedString? {
        if !hasBackPage() {
            return nil
        }
        return backPage()
    }

    func frontPageHasChanged() -> Bool {
        return frontView.page.hasChanged
    }
    
    func backPageHasChanged() -> Bool {
        return backView.page.hasChanged
    }
    
    var isEditable: Bool {
        get {
            return editable
        }
        set(x) {
            frontView.page.isEditable = x
            backView.page.isEditable = x
            editable = x
        }
    }

    func clear() {
        frontView.page.string = ""
        frontView.page.hasChanged = false
        backView.page.string = ""
        backView.page.hasChanged = false
    }

    func setFrontPage(page: Page) {
        frontView.page.setPageContents(page: page)
    }
    
    func setBackPage(page: Page) {
        backView.page.setPageContents(page: page)
    }
    
    func extractTitle(front: String) -> String? {
        let trimmed = front.trimmingCharacters(in: .whitespacesAndNewlines)
        let lines = trimmed.split(maxSplits: 2, omittingEmptySubsequences: true) { c in
            return c == "\n" || c == "\r"
        }
        if lines.count == 0 {
            return nil
        }
        let firstLine = lines[0].trimmingCharacters(in: .whitespaces)
        if firstLine == "" {
            return nil
        }
        return firstLine
    }

    @IBAction func actionSelectFrontPage(_ sender: Any) {
        let tab = tabViews["front"]!
        selectTabViewItem(tab)
        tab.view!.window!.makeFirstResponder(frontView.page)
    }

    @IBAction func actionSelectBackPage(_ sender: Any) {
        let tab = tabViews["back"]!
        selectTabViewItem(tab)
        tab.view!.window!.makeFirstResponder(backView.page)
    }
}

class PageCont: NSViewController {
    var page: LinkingTextView!
    
    override func loadView() {
        
    }
    
    func createView(_ screen: Scaler) {
        let scrollview = NSScrollView(frame: screen.rect(cardSize))
        let page = LinkingTextView()
        page.usesRuler = true
        page.isRulerVisible = true
        page.isVerticallyResizable = true
        page.usesInspectorBar = true
        page.isHorizontallyResizable = true
        page.allowsUndo = true
        page.textContainer?.widthTracksTextView = true
        page.allowsImageEditing = true
        page.allowsDocumentBackgroundColorChange = true
        page.importsGraphics = true
        page.isAutomaticLinkDetectionEnabled = true
        page.displaysLinkToolTips = true
        scrollview.scrollerStyle = .overlay
        scrollview.documentView = page
        scrollview.hasVerticalScroller = true
        scrollview.hasHorizontalScroller = true
        view.translatesAutoresizingMaskIntoConstraints = false
        self.view = scrollview
        self.page = page
    }
}
