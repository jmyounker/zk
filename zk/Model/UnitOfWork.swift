//
//  UnitOfWork.swift
//  zk
//
//  Created by Jeff Younker on 1/2/20.
//  Copyright © 2020 The Blobshop. All rights reserved.
//

import Cocoa
import Foundation

import Logging

enum UnitOfWorkError: Error {
    /// Error raised during `performBlock` of the execute closure.
    case ExecutionError(NSError)

    /// Error raised when the temporaty transactional context cannot be saved.
    case CoreDataTransactionError(NSError)

    /// Error raised when the main context cannot be saved after the transaction.
    case CoreDataError(NSError)
}

class UnitOfWork {
    static let log = Logger(label: "com.theblobshop.zk.UnitOfWork")

    let parentManagedObjectContext: NSManagedObjectContext
    let managedObjectContext: NSManagedObjectContext

    convenience init(parent: NSManagedObjectContext, authorName: String) {
        let c = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        c.parent = parent
        c.mergePolicy = NSOverwriteMergePolicy
        c.transactionAuthor = authorName
        c.automaticallyMergesChangesFromParent = true
        self.init(parent: parent, context: c)
    }

    init(parent: NSManagedObjectContext, context: NSManagedObjectContext) {
        self.parentManagedObjectContext = parent
        self.managedObjectContext = context
    }

    var errorHandler: ((SaveContextInfo, UnitOfWorkError) -> Void) {
        get {
            if let e = customErrorHandler {
                return e
            } else {
                return handleUnitOfWorkError
            }
        }
    }
    
    var customErrorHandler: ((SaveContextInfo, UnitOfWorkError) -> Void)?
    
    func onError(handleWith handler: @escaping ((SaveContextInfo, UnitOfWorkError) -> Void)) -> UnitOfWork {
            customErrorHandler = handler
            return self
    }

    func execute(with info: SaveContextInfo, closure: @escaping (_ context: NSManagedObjectContext) throws -> Void) {
        managedObjectContext.perform {
            do {
                try closure(self.managedObjectContext)
            } catch let error as NSError {
                UnitOfWork.log.error("Failed to perform child context (\(info.rawValue)): \(error)")
                self.errorHandler(info, UnitOfWorkError.ExecutionError(error))
                return
            }

            do {
                try self.managedObjectContext.save()
            } catch let error as NSError {
                UnitOfWork.log.error("Failed to save child context (\(info.rawValue)): \(error)")
                self.errorHandler(info, UnitOfWorkError.ExecutionError(error))
                return
            }

            self.parentManagedObjectContext.perform() {
                do {
                    try self.parentManagedObjectContext.save()
                } catch let error as NSError {
                    UnitOfWork.log.error("Failed to save parent context (\(info.rawValue)): \(error)")
                    self.errorHandler(info, UnitOfWorkError.ExecutionError(error))
                }
            }
        }
    }
}

func handleUnitOfWorkError(_ info: SaveContextInfo, error: UnitOfWorkError) {
    DispatchQueue.main.async {
        let alert = NSAlert()
        alert.messageText = "Core Data Saving Error"
        alert.informativeText = "Failed to save the context (\(info.rawValue)): \(error)"
        alert.alertStyle = .warning
        alert.addButton(withTitle: "OK")
        alert.runModal()
    }
}

enum SaveContextInfo: String {
    case appSave = "explicit application save"
    case changeZettel = "change zettel"
    case createZettel = "create zettel"
    case deleteZettel = "deleted zettel"
    case importKasten = "import kasten"
    case reorganizeZetteln = "reorganize zetteln"
    case repairKasten = "performing repair actions"
    case reviewedZettel = "create review records"
}
