//
//  ZettelQuery.swift
//  zk
//
//  Created by Jeff Younker on 12/19/19.
//  Copyright © 2019 The Blobshop. All rights reserved.
//

import CoreData
import Foundation

struct ZettelQuery {
    var zids: Array<Zid> = Array()
    var tags: Array<TagName> = Array()
    var title: String? = nil
    var fullText: String? = nil
    var parent: Zid? = nil
    var seq: Int64? = nil
    var exactTitle: String? = nil
    var excludingTitle: String? = nil
}

extension ZettelQuery {
    func predicate() -> NSPredicate? {
        var restrictions = Array<Optional<Array<NSPredicate>>>()
        restrictions.append(titleRestrictions())
        restrictions.append(zidRestrictions())
        restrictions.append(tagAndCategoryRestrictions())
        restrictions.append(fullTextRestrictions())
        restrictions.append(parentRestrictions())
        restrictions.append(excludeTitleRestrictions())
        let validRestrictions = withoutNil(restrictions).flatMap{$0}
        switch validRestrictions.count {
        case 0: return nil ;
        case 1: return validRestrictions[0] ;
        default: return NSCompoundPredicate(andPredicateWithSubpredicates: validRestrictions) ;
        }
    }

    func titleRestrictions() -> Array<NSPredicate>? {
        if let t = exactTitle {
            return [NSPredicate(format: "title == %@", t)]
        }
        if let t = title {
            return [NSPredicate(format: "title CONTAINS %@", t)]
        }
        return nil
    }

    func excludeTitleRestrictions() -> Array<NSPredicate>? {
        guard let exclusion = excludingTitle else {
            return nil
        }
        return [NSPredicate(format: "title != %@", exclusion)]
    }
    
    func zidRestrictions() -> Array<NSPredicate>? {
        if zids.count == 0 {
            return nil
        }
        return [NSPredicate(format: "zid IN %@", zids.map{$0 as CVarArg})]
    }

    func tagAndCategoryRestrictions() -> Array<NSPredicate>? {
        if tags.count == 0 {
            return nil
        }
        return [NSCompoundPredicate(
            orPredicateWithSubpredicates: tagRestrictions()! + categoryRestrictions()!)]
    }
    
    func tagRestrictions() -> Array<NSPredicate>? {
        if tags.count == 0 {
            return nil
        }
        return [NSCompoundPredicate(andPredicateWithSubpredicates: tags.map{
            NSPredicate(format: "ANY tags.name == %@", $0)
        })]
    }
    
    func categoryRestrictions() -> Array<NSPredicate>? {
        if tags.count == 0 {
            return nil
        }
        return [NSPredicate(format: "parent.category IN %@", tags)]
    }

    func fullTextRestrictions() -> Array<NSPredicate>? {
        guard let ft = fullText else {
            return nil
        }
        if ft == "" {
            return nil
        }
        let titleContains = NSPredicate(format: "title CONTAINS %@", ft)
        let pagesContain = NSPredicate(format: "ANY pages.content CONTAINS %@", ft)
        return [NSCompoundPredicate(orPredicateWithSubpredicates: [titleContains, pagesContain])]
    }
    
    func parentRestrictions() -> Array<NSPredicate>? {
        guard let p = parent else {
            return nil
        }
        return [NSPredicate(format: "parent_zid == %@", p as CVarArg)]
    }

    func zettelFetchRequest() -> NSFetchRequest<Zettel> {
        let request: NSFetchRequest<Zettel> = Zettel.fetchRequest()
        request.predicate = predicate()
        request.returnsObjectsAsFaults = false
        return request
    }
}
