//
//  NextReview.swift
//  zk
//
//  Created by Jeff Younker on 8/20/20.
//  Copyright © 2020 The Blobshop. All rights reserved.
//

import CoreData
import Foundation

extension NextReview {
    var at: Timestamp {
        get {
            return Timestamp(next_review_time)
        }
        set(x) {
            next_review_time = x
        }
    }

    var leitnerBin: Int {
        get {
            return Int(leitner_bin)
        }
        set(x) {
            leitner_bin = Int32(x)
        }
    }

    var lastReviewTime: Timestamp {
        get {
            return Timestamp(last_review_time)
        }
        set(x) {
            last_review_time = x
        }
    }

    static func select(context: NSManagedObjectContext, zid: Zid) throws -> NextReview? {
        let req: NSFetchRequest<NextReview> = NextReview.fetchRequest()
        req.predicate = NSPredicate(format: "zid == %@", zid as CVarArg)
        req.sortDescriptors = [NSSortDescriptor(key: "next_review_time", ascending: true)]
        req.fetchLimit = 1
        req.returnsObjectsAsFaults = true
        return try context.fetch(req).first
    }
    
    static func select(context: NSManagedObjectContext, atOrBefore t: Timestamp) throws -> Array<NextReview> {
        let req: NSFetchRequest<NextReview> = NextReview.fetchRequest()
        req.predicate = NSPredicate(format: "next_review_time <= %d", t as CVarArg)
        req.sortDescriptors = [NSSortDescriptor(key: "next_review_time", ascending: true)]
        req.returnsObjectsAsFaults = true
        return try context.fetch(req)
    }

    static func select(context: NSManagedObjectContext, after t: Timestamp) throws -> Array<NextReview> {
        let req: NSFetchRequest<NextReview> = NextReview.fetchRequest()
        req.predicate = NSPredicate(format: "next_review_time > %d", t as CVarArg)
        req.sortDescriptors = [NSSortDescriptor(key: "next_review_time", ascending: true)]
        req.returnsObjectsAsFaults = true
        return try context.fetch(req)
    }
}
