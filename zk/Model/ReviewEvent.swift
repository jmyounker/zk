//
//  ReviewEvent.swift
//  zk
//
//  Created by Jeff Younker on 1/21/20.
//  Copyright © 2020 The Blobshop. All rights reserved.
//

import Cocoa

extension ReviewEvent {
    var at: Timestamp {
        get {
            return Timestamp(time)
        }
        set(x) {
            time = x
        }
    }

    var scheduledTime: Timestamp {
        get {
            return Timestamp(scheduled_time)
        }
        set(x) {
            scheduled_time = x
        }
    }

    var leitnerBin: Int {
        get {
            return Int(leitner_bin)
        }
        set(x) {
            leitner_bin = Int32(x)
        }
    }
    
    static func selectLatest(context: NSManagedObjectContext, zid: Zid) throws -> ReviewEvent? {
        let req: NSFetchRequest<ReviewEvent> = ReviewEvent.fetchRequest()
        req.predicate = NSPredicate(format: "zid == %@", zid as CVarArg)
        req.sortDescriptors = [NSSortDescriptor(key: "last_review_time", ascending: false)]
        req.fetchLimit = 1
        req.returnsObjectsAsFaults = true
        return try context.fetch(req).first
    }
    
    static func select(context: NSManagedObjectContext, after reviewTime: Timestamp) throws -> [ReviewEvent] {
        let req: NSFetchRequest<ReviewEvent> = ReviewEvent.fetchRequest()
        req.predicate = NSPredicate(format: "next_review_time > %d", reviewTime)
        req.sortDescriptors = [NSSortDescriptor(key: "next_review_time", ascending: false)]
        req.returnsObjectsAsFaults = true
        return try context.fetch(req)
    }

    static func select(context: NSManagedObjectContext, before reviewTime: Timestamp) throws -> [ReviewEvent] {
        let req: NSFetchRequest<ReviewEvent> = ReviewEvent.fetchRequest()
        req.predicate = NSPredicate(format: "next_review_time <= %d", reviewTime)
        req.sortDescriptors = [NSSortDescriptor(key: "next_review_time", ascending: false)]
        req.returnsObjectsAsFaults = true
        return try context.fetch(req)
    }
}
