//
//  ExportImport.swift
//  zk
//
//  Created by Jeff Younker on 8/20/20.
//  Copyright © 2020 The Blobshop. All rights reserved.
//

import CoreData
import Foundation

// JSON Export format:
//
// {
//   "zid": "ZID",
//   "parent": "ZID" or none,
//   "seq": UINT64,
//   "created": TIMESTAMP,
//   "modified": TIMESTAMP,
//   "tags": ["TAG_NAME"*],
//   "title": "TITLE",
//   "pages": {
//     "PAGE_NAME": {
//       "content-type": "CONTENT_TYPE",
//       "searchable": "SEARCHABLE_CONTENT",
//       "version": "VERSION",
//       "content": "UUENCODED_OBJECT"
//     }
//   }
// }

struct ExportKasten : Codable, Equatable {
    let zetteln: Array<ExportZettel>
    
    static func ==(lhs: ExportKasten, rhs: ExportKasten) -> Bool {
        return lhs.zetteln == rhs.zetteln
    }
}

struct ExportZettel : Codable, Hashable, Equatable {
    let zid: String
    let parentZid: String?
    let seq: Int64
    let createdAt: Int64
    let modifiedAt: Int64
    let title: String
    let tagNames: Array<String>
    let pages: Dictionary<String, ExportPage>

    func hash(into: inout Hasher) {
        into.combine(zid)
    }
    
    static func ==(lhs: ExportZettel, rhs: ExportZettel) -> Bool {
        if lhs.zid != rhs.zid {
            return false
        }
        if lhs.parentZid != rhs.parentZid {
            return false
        }
        if lhs.seq != rhs.seq {
            return false
        }
        if lhs.createdAt != rhs.createdAt {
            return false
        }
        if lhs.modifiedAt != rhs.modifiedAt {
            return false
        }
        if lhs.title != rhs.title {
            return false
        }
        if Set<String>(lhs.tagNames) != Set<String>(rhs.tagNames) {
            return false
        }
        if lhs.pages != rhs.pages {
            return false
        }
        return true
    }
}

struct ExportPage : Codable, Equatable {
    let contentType: String
    let version: String
    let content: String
    let searchable: String
    
    static func ==(lhs: ExportPage, rhs: ExportPage) -> Bool {
        if lhs.contentType != rhs.contentType {
            return false
        }
        if lhs.version != rhs.version {
            return false
        }
        if lhs.content != rhs.content {
            return false
        }
        if lhs.searchable != rhs.searchable {
            return false
        }
        return true
    }
}

func exportKasten(context c: NSManagedObjectContext) throws -> ExportKasten {
    let zetteln = Zettel.select(context: c, query: ZettelQuery())
    return ExportKasten(
        zetteln: zetteln.map{ exportZettel(zettel: $0)}
    )
}

func exportZettel(zettel z: Zettel) -> ExportZettel {
    var pages = Dictionary<String, ExportPage>()
    for (n, p) in z.pagesByName {
        pages[n.rawValue] = exportPage(page: p)
    }
    return ExportZettel(
        zid: z.zid!.uuidString,
        parentZid: z.parentZid?.uuidString,
        seq: z.seq,
        createdAt: z.createdAt,
        modifiedAt: z.modifiedAt,
        title: z.title!,
        tagNames: z.tagNames,
        pages: pages)
}

func exportPage(page p: Page) -> ExportPage {
    return ExportPage(
        contentType: p.content_type!,
        version: p.version!,
        content: p.content_enriched!,
        searchable: p.content!)
}

func importZetteln(context c: NSManagedObjectContext, zetteln: Array<ExportZettel> ) throws {
    // Two pass process. First import all zetteln, then link parents.

    var childZidToParentZid = Dictionary<Zid, Zid>()
    var zidToZettel = Dictionary<Zid, Zettel>()

    for ez in zetteln {
        let z = Zettel(context: c)
        let zid = Zid(uuidString: ez.zid)!
        zidToZettel[zid] = z
        z.zid = zid
        if let pz = ez.parentZid {
            let pZid = Zid(uuidString: pz)!
            childZidToParentZid[zid] = pZid
        }
        z.seq = ez.seq
        z.createdAt = Timestamp(ez.createdAt)
        z.modifiedAt = Timestamp(ez.modifiedAt)
        z.title = ez.title
        z.tagNames = ez.tagNames
        for (n, ep) in ez.pages {
            let p = Page(context: c)
            p.zid = zid
            p.zettel = z
            p.name = n
            p.content_type = ep.contentType
            p.version = ep.version
            p.content = ep.searchable
            p.content_enriched = ep.content
        }
    }
    
    for (zid, pZid) in childZidToParentZid {
        let z = zidToZettel[zid]!
        let pz = zidToZettel[pZid]!
        z.setParentage(zettel: pz)
    }
}
