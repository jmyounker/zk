//
//  Zettel.swift
//  zk
//
//  Created by Jeff Younker on 12/24/19.
//  Copyright © 2019 The Blobshop. All rights reserved.
//

import Cocoa
import CoreData
import Foundation

import Logging

typealias Zid = UUID

// MARK: - Basic Manipulation
extension Zettel {
    static let log = Logger(label: "com.theblobshop.zk.Zettel")

    var parentZid: Zid? {
        get { return parent_zid }
        set(x) { parent_zid = x }
    }

    func create(timekeeper: Timekeeper) throws {
        self.seq = try nextSeq()
        let now = timekeeper.now()
        self.created_at = now
        self.modified_at = now
    }

    var pagesByName: Dictionary<PageName, Page> {
        get {
            let p: Set<Page> = (self.pages ?? NSSet()) as! Set<Page>
            return indexByUniqueKey(p) { PageName(rawValue: $0.name ?? "unkown")! }
        }
    }
    
    static func select(context c: NSManagedObjectContext, query q: ZettelQuery) -> Array<Zettel> {
        do {
            return try Zettel.selectOrThrow(context: c, query: q)
        } catch {
            Zettel.log.warning("cannot load zetteln: \(error)")
            return []
        }
    }

    static func selectOrThrow(context: NSManagedObjectContext, query: ZettelQuery) throws -> Array<Zettel> {
        let req: NSFetchRequest<Zettel> = Zettel.fetchRequest()
        req.predicate = query.predicate()
        req.returnsObjectsAsFaults = false
        req.sortDescriptors = [NSSortDescriptor(key: "seq", ascending: true)]
        return try context.fetch(req)
    }

    static func select(context c: NSManagedObjectContext, zid z: Zid?) -> Zettel? {
        guard let zid = z else {
            return nil
        }
        let zetteln = Zettel.select(context: c, query: ZettelQuery(zids: [zid]))
        if zetteln.count == 0 {
            return nil
        }
        if zetteln.count > 1 {
            Zettel.log.warning("found more than one zetteln for zid \(zid)")
            return nil
        }
        return zetteln[0]
    }

    func duplicate(into c: NSManagedObjectContext) throws -> Zettel {
        guard let z = Zettel.select(context: c, zid: self.zid!) else {
            throw ZettelError.cannotFindSelf
        }
        return z
    }

    static func count(context: NSManagedObjectContext, query: ZettelQuery) throws -> Int {
        let req: NSFetchRequest<Zettel> = Zettel.fetchRequest()
        req.predicate = query.predicate()
        req.returnsObjectsAsFaults = true
        return try context.count(for: req)
    }
}

enum ZettelError: Error {
    case cannotFindSelf
    case cannotFindZettelByZid(zid: Zid)
    case expectedOneZettel(zid: Zid, got: Int)
    case cannotBecomeOwnChild(zid: Zid, parent: Zid?)
    case WTF
}

// MARK: - Create and update times.
extension Zettel {
    var createdAt: Timestamp {
        get { return created_at }
        set(x) { created_at = x }
    }

    var modifiedAt: Timestamp {
        get { return modified_at }
        set(x) { modified_at = x }
    }
    
    func update(timekeeper: Timekeeper) throws {
        self.modified_at = timekeeper.now()
    }
}

// MARK: - Reviews
extension Zettel {
    func latestReviewEvent() throws -> ReviewEvent? {
        return try ReviewEvent.selectLatest(context: self.managedObjectContext!, zid: self.zid!)
    }
}

// MARK: - Tags
extension Zettel {

    /// Manage tags as if they were a set of names.
    ///
    /// Tags are created, added, and removed as necessary. Unlinked
    /// tags are not deleted from the database, so garbage accumulates
    /// and should be occasionally cleaned out.
    ///
    // TODO(jyounker): Change to Set<TagName>
    var tagNames : Array<TagName> {
        // TODO(jyounker): Clean out garbage.
        get {
            let tagSet = tags as? Set<Tag> ?? Set([])
            return tagSet.map{ $0.name! }
        }
        set(desired) {
            let context = self.managedObjectContext!
            let existingTagNames = self.tagNames
            let tagNamesToAdd = Set(desired).subtracting(existingTagNames)
            let tagNamesToRemove = Set(existingTagNames).subtracting(Set(desired))
            let tagsToAddThatExist = Set(Tag.select(context: context, names: tagNamesToAdd))
            let tagNamesToAddThatExist = Set(tagsToAddThatExist.map{ $0.name! })
            let tagNamesToCreate = tagNamesToAdd.subtracting(tagNamesToAddThatExist)
            let tagsCreated = Set(tagNamesToCreate.map{ Tag.create(context: context, name: $0)})
            self.addToTags(tagsToAddThatExist as NSSet)
            self.addToTags(tagsCreated as NSSet)
            let tagsToRemove = Set(Tag.select(context: context, names: tagNamesToRemove))
            self.removeFromTags(tagsToRemove as NSSet)
        }
    }
    
    func set(tag t: String) {
        let tags = self.tagNames
        if tags.contains(t) {
            return
        }
        self.tagNames = tags + [t]
    }

    func unset(tag t: String) {
        let tags = self.tagNames
        if !tags.contains(t) {
            return
        }
        self.tagNames = tags.filter(){ $0 != t }
    }
}

// MARK: - Ordering
extension Zettel {
    // The function is used cross-thread and cross-context so we pass in a zid to avoid
    // threading issues.
    static func remove(context c: NSManagedObjectContext, zettel z: Zettel) throws {
        let p = z.parent
        let seq = z.seq
        z.setParentage(zettel: nil)
        z.seq = 0
        try shift(context: c, parent: p, startingAt: seq+1, shiftedTo: seq)
    }

    // The function is used cross-thread and cross-context so we pass in zids to avoid
    // threading issues.
    static func insert(
        context c: NSManagedObjectContext,
        parentZid pz: Zid?,
        at seq: Int64,
        child: Zettel) throws {
        let p = Zettel.select(context: c, zid: pz)
        assert ((p != nil && pz != nil) || (p == nil && pz == nil))
        try shift(context: c, parent: p, startingAt: seq, shiftedTo: seq+1)
        child.setParentage(zettel: p)
        child.seq = seq
    }
    
    static func shift(context c: NSManagedObjectContext, parent p: Zettel?, startingAt: Int64, shiftedTo: Int64) throws {
        let zetteln = try Zettel.select(context: c, parent: p, from: startingAt)
        var seq = shiftedTo
        for z in zetteln {
            z.seq = seq
            seq += 1
        }
    }
    
    static func select(context: NSManagedObjectContext, parent: Zettel?, from seq: Int64) throws -> [Zettel] {
        let req: NSFetchRequest<Zettel> = Zettel.fetchRequest()
        req.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [
            byParentRestrict(parent: parent),
            NSPredicate(format: "seq >= %d", Int(seq))
        ])
        req.sortDescriptors = [NSSortDescriptor(key: "seq", ascending: true)]
        return try context.fetch(req)
    }

    func nextChildSeq(context c: NSManagedObjectContext) throws -> Int64 {
        guard let last = try zk.lastChildZettel(context: c, parentZid: self.zid!) else {
            return 0
        }
        return last.seq + 1
    }

    // TODO(jyounker): Rename to nextSiblingSeq()
    func nextSeq() throws -> Int64 {
        return try lastChildNumber(
            context: self.managedObjectContext!, parentZid: self.parent_zid!) + 1
    }
}

// MARK: - Parentage and Hierarchy
extension Zettel {
    static func countChildren(context: NSManagedObjectContext, parent p: Zettel?) throws -> Int {
        let req: NSFetchRequest<Zettel> = Zettel.fetchRequest()
        req.predicate = Zettel.byParentRestrict(parent: p)
        return try context.count(for: req)

    }

    static func byParentRestrict(parent: Zettel?) -> NSPredicate {
        return Zettel.byParentRestrict(parentZid: parent?.zid)
    }
    
    static func byParentRestrict(parentZid: Zid?) -> NSPredicate {
        guard let pz = parentZid else {
            return NSPredicate(format: "parent_zid = nil")
        }
        return NSPredicate(format: "parent_zid == %@", pz as CVarArg)
    }
    
    static func areParentAndChild(parent: Zettel?, child: Zettel?) throws -> Bool {
        if parent == nil && child == nil {
            return false
        } else if child == nil {
            return false
        } else if parent == nil {
            return true
        } else if parent?.zid == child?.zid {
            return true
        }
        var current = child
        var i = 10000
        while i > 0 {
            if current == nil {
                return false
            }
            if current == parent {
                return true
            }
            current = current?.parent
            i -= 1
        }
        throw ZettelError.WTF
    }

    func lastChildZettel() throws -> Zettel? {
        guard let context = self.managedObjectContext else {
            return nil
        }
        return try zk.lastChildZettel(context: context, parentZid: zid)
    }
}

// TODO(jyounker): Make these two static functions.
func lastChildNumber(context: NSManagedObjectContext, parentZid: UUID?) throws -> Int64 {
    return try lastChildZettel(context: context, parentZid: parentZid)?.seq ?? 0
}

func lastChildZettel(context: NSManagedObjectContext, parentZid: UUID?) throws -> Zettel? {
    let req: NSFetchRequest<Zettel> = Zettel.fetchRequest()
    req.predicate = Zettel.byParentRestrict(parentZid: parentZid)
    req.sortDescriptors = [NSSortDescriptor(key: "seq", ascending: false)]
    req.fetchLimit = 1
    req.returnsObjectsAsFaults = false
    return try context.fetch(req).first
}

// MARK: - URLs and Pasteboard
extension Zettel {
    var url: URL {
        get {
            return URL(string: "uzkp://localhost:0/zid/\(self.zid!)")!
        }
    }

    static func toZid(url: URL) throws -> Zid {
        guard let scheme = url.scheme else {
            throw KastenError.MalformedURL("no scheme defined")
        }
        if scheme != "uzkp" {
            throw KastenError.MalformedURL("expected scheme uzkp and not \(scheme)")
        }
        guard let host = url.host else {
            throw KastenError.MalformedURL("no host defined")
        }
        if host != "localhost" {
            throw KastenError.MalformedURL("expected host localhost and not \(host)")
        }
        guard let port = url.port else {
            throw KastenError.MalformedURL("no port defined")
        }
        if port != 0 {
            throw KastenError.MalformedURL("expected port 0 and not \(port)")
        }
        if url.pathComponents.count != 2 {
            throw KastenError.MalformedURL("expected path of the form /zid/ZID and not \(url.path)")
        }
        if url.pathComponents[0] != "zid" {
            throw KastenError.MalformedURL("expected path of the form /zid/ZID and not \(url.path)")
        }
        guard let zid = Zid(uuidString: url.lastPathComponent) else {
            throw KastenError.MalformedURL("expected Zid to be a UUID and not \(url.lastPathComponent)")
        }
        return zid
    }

    static func select(context c: NSManagedObjectContext, item: NSPasteboardItem) throws -> [Zettel] {
        return []
    }
    
    func setParentage(zettel: Zettel?) {
        Zettel.log.info("reparenting \(self.zid!) to \(zettel?.zid?.description ?? "none")")
        parentZid = zettel?.zid
        parent = zettel
        Zettel.log.info("reparented \(self.zid!) to \(parent?.zid?.description ?? "none") (\(parentZid?.description ?? "none"))")
    }
}

class ZidURL {
    let url: URL
    let zid: Zid

    init(zid: Zid) {
        self.zid = zid
        self.url = URL(string: "uzkp://localhost:0/zid/\(zid)")!
    }
    
    convenience init(zettel: Zettel) {
        self.init(zid: zettel.zid!)
    }
    
    init(url: URL) throws {
        guard let scheme = url.scheme else {
            throw KastenError.MalformedURL("no scheme defined")
        }
        if scheme != "uzkp" {
            throw KastenError.MalformedURL("expected scheme uzkp and not \(scheme)")
        }
        guard let host = url.host else {
            throw KastenError.MalformedURL("no host defined")
        }
        if host != "localhost" {
            throw KastenError.MalformedURL("expected host localhost and not \(host)")
        }
        guard let port = url.port else {
            throw KastenError.MalformedURL("no port defined")
        }
        if port != 0 {
            throw KastenError.MalformedURL("expected port 0 and not \(port)")
        }
        let path = url.pathComponents
        if path.count != 3 {
            throw KastenError.MalformedURL("expected path of the form /zid/ZID and not \(path): expected path 3 path components but got \(path.count)")
        }
        if path[0] != "/" && path[1] != "zid" {
            throw KastenError.MalformedURL("expected path of the form /zid/ZID and not \(path): expected first component to be 'zid' but got \(path[0]), \(path[1])")
        }
        guard let zid = Zid(uuidString: url.lastPathComponent) else {
            throw KastenError.MalformedURL("expected Zid to be a UUID and not \(url.lastPathComponent)")
        }
        self.url = url
        self.zid = zid
    }
}

extension Zettel {
    static func moveToInbox(context c: NSManagedObjectContext, zetteln: Array<Zettel>) throws {
        if zetteln.count == 0 {
            return
        }
        guard let inbox = Zettel.select(context: c, zid: FixedZettel.Inbox.zid) else {
            Zettel.log.info("cannot move to inbox: inbox missing")
            return
        }
        for z in zetteln {
            Zettel.log.warning("moving zettel to inbox: \(z.zid!)")
            let x = try inbox.nextChildSeq(context: c)
            z.setParentage(zettel: inbox)
            z.seq = x
        }
    }
    
    static func repairMissingZids(context c: NSManagedObjectContext, zetteln: Array<Zettel>) {
        for z in zetteln {
            let zid = Zid()
            log.info("repairing missing zid: setting to \(zid)")
            z.zid = zid
        }
    }

    static func selectMissingParentZids(context: NSManagedObjectContext) -> Array<Zettel>  {
        let req: NSFetchRequest<Zettel> = Zettel.fetchRequest()
        req.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [
            NSPredicate(format: "parent != nil"),
            NSPredicate(format: "parent_zid == nil"),
        ])
        do {
            return try context.fetch(req)
        } catch {
            Zettel.log.info("cannot find missing parent zids: \(error)")
            return []
        }
    }

    static func selectDanglingParentZids(context: NSManagedObjectContext) -> Array<Zettel> {
        let req: NSFetchRequest<Zettel> = Zettel.fetchRequest()
        req.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [
            NSPredicate(format: "parent == nil"),
            NSPredicate(format: "parent_zid != nil"),
        ])
        do {
            return try context.fetch(req)
        } catch {
            Zettel.log.info("cannot find dangling parent zids: \(error)")
            return []
        }
    }
    
    static func selectMissingZids(context: NSManagedObjectContext) -> Array<Zettel> {
        let req: NSFetchRequest<Zettel> = Zettel.fetchRequest()
        req.predicate = NSPredicate(format: "zid == nil")
        do {
            return try context.fetch(req)
        } catch {
            Zettel.log.info("cannot find missing zids: \(error)")
            return []
        }
    }

    static func reparent(context c: NSManagedObjectContext, zid: Zid, parent parentZid: Zid?, at seq: Int64) throws {
        let parent = Zettel.select(context: c, zid: parentZid);
        assert ((parent != nil && parentZid != nil) || (parent == nil && parentZid == nil))
        guard let z = Zettel.select(context: c, zid: zid) else {
            throw ZettelError.cannotFindZettelByZid(zid: zid)
        }
       if z == parent {
            throw ZettelError.cannotBecomeOwnChild(zid: zid, parent: parentZid)
        }
        if try Zettel.areParentAndChild(parent: z, child: parent) {
            throw ZettelError.cannotBecomeOwnChild(zid: zid, parent: parentZid)
        }
        try Zettel.remove(context: c, zettel: z)
        try Zettel.insert(context: c, parentZid: parentZid, at: seq, child: z)
    }
}
