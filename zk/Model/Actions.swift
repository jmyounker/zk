//
//  Actions.swift
//  zk
//
//  Created by Jeff Younker on 8/17/20.
//  Copyright © 2020 The Blobshop. All rights reserved.
//

import Cocoa

enum ZettelAction {
    case create
    case update(zid: Zid)
    case title(value: String)
    case setTag(name: TagName)
    case unsetTag(name: TagName)
    case tags(name: Array<TagName>)
    case setParent(zid: Zid)
    case unsetParent
    case createPage(name: PageName, value: NSAttributedString)
    case updatePage(name: PageName, value: NSAttributedString)
    case deletePage(name: PageName)
    case modified(at: Timestamp)
}

typealias ZettelActions = Array<ZettelAction>

enum ZettelActionError: Error {
    case updateZettelMissing
    case parentNotFound
    case notImplementedYet
}

func performActions(context c: NSManagedObjectContext, actions: ZettelActions) throws {
    var currentZettel: Zettel!
    for action in actions {
        switch action {
        case .create:
            currentZettel = Zettel(context: c)
            currentZettel.zid = UUID()
        case .update(let zid):
            currentZettel = Zettel.select(context: c, zid: zid)
            if currentZettel == nil {
                throw ZettelActionError.updateZettelMissing
            }
        case .title(let value):
            currentZettel.title = value
        case .setTag(let name):
            currentZettel.set(tag: name)
        case .unsetTag(let name):
            currentZettel.unset(tag: name)
        case .tags(let names):
            // This is primarily for debugging
            currentZettel.tagNames = names
        case .setParent(let zid):
            guard let parent = Zettel.select(context: c, zid: zid) else {
                throw ZettelActionError.parentNotFound
            }
            currentZettel.setParentage(zettel: parent)
        case .unsetParent:
            currentZettel.setParentage(zettel: nil)
        case .createPage(let name, let content):
            let p = try Page.from(
                context: c, zid: currentZettel.zid!, name: name, rtfd: content)
            p.zettel = currentZettel
            currentZettel.addToPages(p)
        case .updatePage(let name, let content):
            let p = currentZettel.pagesByName[name]!
            try p.setContent(rtfd: content)
        case .deletePage(let name):
            c.delete(currentZettel.pagesByName[name]!)
        case .modified(let t):
            currentZettel.modifiedAt = t
        }
    }
}
