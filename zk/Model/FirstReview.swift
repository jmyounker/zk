//
//  FirstReview.swift
//  zk
//
//  Created by Jeff Younker on 3/20/20.
//  Copyright © 2020 The Blobshop. All rights reserved.
//

import Cocoa
import Foundation

extension FirstReview {
    var at: Timestamp {
        get {
            return Timestamp(time)
        }
        set(x) {
            time = x
        }
    }
}
