//
//  Container.swift
//  zk
//
//  Created by Jeff Younker on 8/20/20.
//  Copyright © 2020 The Blobshop. All rights reserved.
//

import CoreData
import Foundation

import Logging

class Container {
    let log = Logger(label: "com.theblobshop.zk.Container")

    let name: String
    let authorName: String

    init(name: String, authorName: String) {
        self.name = name
        self.authorName = authorName
    }

    func unitOfWork() -> UnitOfWork {
        return UnitOfWork(parent: context, authorName: self.authorName)
    }

    // MARK: - Core Data stack
    var context: NSManagedObjectContext {
        get { return persistentContainer.viewContext }
    }

    lazy var persistentContainer: NSPersistentCloudKitContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentCloudKitContainer(name: name)

        guard let description = container.persistentStoreDescriptions.first else {
            fatalError("###\(#function): Failed to retrieve a persistent store description.")
        }
        
        description.setOption(true as NSNumber, forKey: NSPersistentHistoryTrackingKey)
        description.setOption(true as NSNumber, forKey: NSPersistentStoreRemoteChangeNotificationPostOptionKey)
        
        container.loadPersistentStores(completionHandler: { (_, error) in
            guard let error = error as NSError? else { return }
            fatalError("###\(#function): Failed to load persistent stores:\(error)")
        })

        log.info("local data stored in \(NSPersistentContainer.defaultDirectoryURL())")
        
        // Local changes win.
        log.info("local changes should win during merge conflicts")
        container.viewContext.mergePolicy = NSOverwriteMergePolicy
        container.viewContext.transactionAuthor = authorName
        
        container.viewContext.automaticallyMergesChangesFromParent = true
        do {
            try container.viewContext.setQueryGenerationFrom(.current)
        } catch {
            fatalError("###\(#function): Failed to pin viewContext to the current generation:\(error)")
        }
        return container
    }()
}

