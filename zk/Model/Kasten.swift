//
//  Kasten.swift
//  zk
//
//  Created by Jeff Younker on 10/26/19.
//  Copyright © 2019 The Blobshop. All rights reserved.
//

import CoreData
import Cocoa
import Foundation
import SQLite3

import Logging

class Kasten {
    static let log = Logger(label: "com.theblobshop.zk.Kasten")
    let timekeeper: Timekeeper

    init(timekeeper: Timekeeper) {
        self.timekeeper = timekeeper
    }
    
    static func getFixed(context c: NSManagedObjectContext, zettel z: FixedZettel) throws -> Zettel {
        let zetteln = Zettel.select(context: c, query: ZettelQuery(zids:[z.zid]))
        if zetteln.count == 0 {
            throw KastenError.DoesNotExist(z)
        } else if zetteln.count > 1 {
            fatalError("cannot find zettel: found more than one record for zettel \(z.zid)")
        }
        return zetteln[0]
    }
    
    func handleSavingError(_ info: SaveContextInfo, _ error: Error) {
        Kasten.log.error("Context saving error: \(error)")
        
        DispatchQueue.main.async {
            let alert = NSAlert()
            alert.messageText = "Core Data Saving Error"
            alert.informativeText = "Failed to save the context: \(error)."
            alert.alertStyle = .warning
            alert.addButton(withTitle: "OK")
            alert.runModal()
        }
    }

    static func initDefaultZettel(context c: NSManagedObjectContext) throws {
        for fz in FixedZettel.All {
            if try Zettel.count(context: c, query: ZettelQuery(zids: [fz.zid])) != 0 {
                continue
            }
            let z = Zettel(context: c)
            z.zid = fz.zid
            z.title = fz.title
            z.category = fz.category
            z.seq = fz.seq
            let front = try Page.from(
                context: c,
                zid: z.zid!,
                name: .front,
                rtfd: fz.page)
            z.addToPages(front)
        }
    }
    
    static func updateDefaultZettel(context c: NSManagedObjectContext) throws {
        for fz in FixedZettel.All {
            guard let z = Zettel.select(context: c, zid: fz.zid) else {
                break;
            }
            log.info("Reinit \(fz.title)")
            z.title = fz.title
            z.category = fz.category
            
            if let oldFront = z.pagesByName[.front] {
                z.removeFromPages(oldFront)
            }
            let front = try Page.from(
                context: c,
                zid: z.zid!,
                name: .front,
                rtfd: fz.page)
            z.addToPages(front)
        }
        log.info("Reinit successful")
    }
    
    static func repair(context c: NSManagedObjectContext) throws {
        Kasten.log.info("repairing missing zids")
        Zettel.repairMissingZids(context: c, zetteln: Zettel.selectMissingZids(context: c))
        Kasten.log.info("repairing missing parent zids")
        let withMissingParentZids = Zettel.selectMissingParentZids(context: c)
        try Zettel.moveToInbox(context: c, zetteln: withMissingParentZids)
        Kasten.log.info("repairing dangling parent zids")
        let withDanglingParentZids = Zettel.selectDanglingParentZids(context: c)
        try Zettel.moveToInbox(context: c, zetteln: withDanglingParentZids)
    }
    
    static func deleteAllZetteln(context c: NSManagedObjectContext) throws {
        let zetteln = try Zettel.selectOrThrow(context: c, query: ZettelQuery())
        for z in zetteln {
            c.delete(z)
        }
        Kasten.log.info("deleted \(zetteln.count) zetteln")
    }
}

enum KastenError: Error {
    case MalformedURL(_ message: String)
    case DoesNotExist(_ zettel: FixedZettel)
}


struct FixedZettel {
    let zid: Zid
    let title: String
    let category: String?
    let body: String
    let seq: Int64

    static let Inbox = FixedZettel(
            zid: Zid(uuidString: "AB595790-4BED-45F3-82F0-D0196363E895")!,
            title: "Inbox",
            category: "inbox",
            body: "Inbox\n\nNew cards which don’t have a parent card are placed here. Putting these cards away will help keep your kasten clean.",
            seq: 0)
    static let Index = FixedZettel (
            zid: Zid(uuidString: "8C88D19C-1D06-4F58-B2DD-538E3183ABF2")!,
            title: "Index",
            category: "index",
            body: "Index\n\nCreate index links here, and in other cards under here.",
            seq: 1)
    static let Authors = FixedZettel(
            zid: Zid(uuidString: "4B77B1AA-E196-499B-A165-E0CC806DD398")!,
            title: "Authors",
            category: "author",
            body: "Authors\n\nAuthors go here, and you can link to them.",
            seq: 2)
    static let Bibliography = FixedZettel(
            zid: Zid(uuidString: "3F4ED0D3-18AE-4670-8939-F91C8D31D43A")!,
            title: "Bibliography",
            category: "bibliography",
            body: "Bibliography\n\nPublications go here, and you can link to them.",
            seq: 3)
    static let Kasten = FixedZettel(
            zid: Zid(uuidString: "4EEBEAED-B811-4BDB-8CB4-6EF1D493FDF9")!,
            title: "Kasten",
            category: nil,
            body: "Kasten\n\nThis is your zettelkasten. Most cards end up here.",
            seq: 4)

    static let All = [Inbox, Index, Authors, Bibliography, Kasten]
    
    var page: NSAttributedString {
        get {
            return NSAttributedString(
                string: body,
                attributes: [
                    .foregroundColor: NSColor.textColor,
                    .backgroundColor: NSColor.textBackgroundColor,
            ])
        }
    }
}
