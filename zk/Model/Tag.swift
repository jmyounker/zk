//
//  Tag.swift
//  zk
//
//  Created by Jeff Younker on 12/24/19.
//  Copyright © 2019 The Blobshop. All rights reserved.
//

import Cocoa
import Foundation

import Logging

typealias TagName = String

extension Tag {
    static let log = Logger(label: "com.theblobshop.zk.Tag")
    
    static func create(context: NSManagedObjectContext, name: TagName) -> Tag {
        let t = Tag(context: context)
        t.name = name
        return t
    }

    static func select(context: NSManagedObjectContext, names: Set<TagName>) -> Array<Tag> {
        if names.count == 0 {
            return []
        }
        let req: NSFetchRequest<Tag> = Tag.fetchRequest()
        req.predicate = NSPredicate(format: "name IN %@", names)
        req.returnsObjectsAsFaults = false
        do {
            return try context.fetch(req)
        } catch {
            Zettel.log.warning("cannot load zetteln: \(error)")
            return []
        }
    }
}
