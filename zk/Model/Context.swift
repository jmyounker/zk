//
//  Context.swift
//  zk
//
//  Lifted whole from saving logic
//  Copyright © 2020 The Blobshop. All rights reserved.
//

import Cocoa
import CoreData
import Foundation

import Logging

extension NSManagedObjectContext {
    static let log = Logger(label: "com.theblobshop.zk.NSManagedObjectContext")

    func save(_ info: SaveContextInfo) throws {
        do {
            try save()
        } catch {
            NSManagedObjectContext.log.error("Failed to save the context (\(info.rawValue)): \(error)")
            handleSavingError(error, info: info)
        }
    }

    /**
     Handles save error by presenting an alert.
     */
    
}

func handleSavingError(_ error: Error, info: SaveContextInfo) {
    DispatchQueue.main.async {
        let alert = NSAlert()
        alert.messageText = "Core Data Saving Error"
        alert.informativeText = "Failed to save the context (\(info.rawValue)): \(error)"
        alert.alertStyle = .warning
        alert.addButton(withTitle: "OK")
        alert.runModal()
    }
}

enum SaveContextInfo: String {
    case appSave = "explicit application save"
    case createZettel = "create zettel"
    case changeZettel = "change zettel"
    case reviewedZettel = "create review records"
    case deleteZettel = "deleted zettel"
}
