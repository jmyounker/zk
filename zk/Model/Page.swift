//
//  Page.swift
//  zk
//
//  Created by Jeff Younker on 12/24/19.
//  Copyright © 2019 The Blobshop. All rights reserved.
//

import Cocoa
import Foundation

extension Page {
    @objc dynamic var data: NSData? {
        get {
            guard let ce = self.content_enriched else {
                return nil
            }
            return uudecode(base64: ce)
        }
        set(data) {
            guard let d = data else {
                self.content_enriched = nil
                return
            }
            self.content_enriched = uuencode(data: d)
        }
    }
    
    var contentType: ContentType {
        get {
            return ContentType.from(string: content_type!)
        }
        set(x) {
            content_type = x.stringValue()
        }
    }

    func create(timekeeper: Timekeeper) {
        zettel!.modified_at = timekeeper.now()
    }

    func update(timekeeper: Timekeeper) {
        zettel!.modified_at = timekeeper.now()
    }
    
    static func from(
        context: NSManagedObjectContext, zid: Zid, name: PageName, rtfd: NSAttributedString) throws -> Page {
        let p = Page(context: context)
        p.zid = zid
        p.name = name.rawValue
        try p.setContent(rtfd: rtfd)
        return p
    }

    func setContent(rtfd: NSAttributedString) throws {
        version = PageVersion.v1.stringValue()
        contentType = .rtfd
        content = rtfd.string
        guard let d = rtfd.rtfd(from: wholeString(rtfd.string)) else {
            throw PageError.CannotConvertToStringToRtfd
        }
        data = d as NSData
    }
}

/// Convert a plain text string into uuencoded rtfd. Mostly used for testing.
func uuencodedRtfd(string x: String) -> String {
    let ax = NSAttributedString(string: x)
    return uuencode(data: ax.rtfd(from: wholeString(ax.string))! as NSData)!
}

func uudecode(base64: String) ->  NSData? {
    return NSData(
        base64Encoded: base64,
        options: [
            NSData.Base64DecodingOptions(rawValue: 0),
            NSData.Base64DecodingOptions.ignoreUnknownCharacters])
}

func uuencode(data: NSData) -> String? {
    return data.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
}

enum PageName: String {
    case front = "front"
    case back = "back"
    case unknown = "unknown"
}

enum ContentType {
    case rtfd
    case other(value: String)

    var rawValue: String {
        get { return self.stringValue() }
    }

    func stringValue() -> String {
        switch self {
        case .rtfd: return "text/rtfd" ;
        case .other(let value): return value ;
        }
    }

    static func from(string: String) -> ContentType {
        switch string {
        case "text/rtfd": return .rtfd ;
        default: return .other(value: string) ;
        }
    }
}

enum PageVersion {
    case v1
    case unknown(version: String)

    var rawValue: String {
        get { return self.stringValue() }
    }
    
    func stringValue() -> String {
        switch self {
        case .v1: return "v1" ;
        case .unknown(let version): return version ;
        }
    }
    
    static func from(string s: String) -> PageVersion {
        if s == "v1" {
            return .v1
        }
        return unknown(version: s)
    }
}

enum PageError: Error {
    case CannotConvertToStringToRtfd
}
