//
//  BrowseController.swift
//  zk
//
//  Created by Jeff Younker on 12/25/19.
//  Copyright © 2019 The Blobshop. All rights reserved.
//

import CoreData
import Cocoa
import Foundation

import Logging

class BrowseController: NSViewController, NSOutlineViewDelegate, AppActions {
    let log = Logger(label: "com.theblobshop.zk.BrowseController")

    var debug: DebugController!
    var screen: Scaler!
    var searchField: NSSearchField!
    var outline: ZettelOutlineView!
    var searchResults: FilterResults = FilterResults.empty
    var tagsField: TagsTextField!
    var zidField: NSTextField!  // strictly for debugging
    var isReviewCheckbox: NSButton!
    var pages: PageView!
    let timekeeper = Timekeeper()

    var activeZettelZid: Zid?
    
    var activeFilterCriteria: FilterCriteria?

    func configure(screen: Scaler) {
        self.debug = appDelegate.debug
        self.screen = screen
        pages = PageView()
        pages.configure(screen: screen)
    }

    func createView() {
        let view = NSStackView(frame: screen.rect(cardSize))
        view.orientation = .horizontal
        view.alignment = .top
        view.distribution = .fillEqually
        let leftView = createLeftView()
        let rightView = createRightView()
        view.addArrangedSubview(leftView)
        view.addArrangedSubview(rightView)
        self.view = view
        disableZettelDetails()
        addContextObservers()
    }
    
    func createLeftView() -> NSView {
        let view = NSStackView(frame: screen.rect(Dimensions(x: 30, y:20)))
        view.orientation = .vertical
        searchField = NSSearchField()
        searchField.action = #selector(actionPerformSearchFromSearchBar(_:))
        searchField.toolTip = Localized("browser-search-field-tooltip")
        let outlineContainer = NSScrollView(frame:NSMakeRect(0, 0, 10, 10))
        outline = ZettelOutlineView(screen: screen, frame: outlineSize)
        outline.delegate = self
        outline.toolTip = Localized("browser-outline-tooltip")
        outlineContainer.documentView = outline
        outlineContainer.hasVerticalScroller = true
        let subjectButton = NSButton(
            title: "subject", target: nil, action: #selector(actionSetParentZettel(_:)))
        subjectButton.toolTip = Localized("browser-subject-button-tooltip")
        view.addArrangedSubview(sideBySideView(left: searchField, right: subjectButton))
        view.addArrangedSubview(outlineContainer)
        let views: Dictionary<String, NSView> = ["search": searchField]
        let metrics = ["width": screen.x(50)]  // about two inches
        let constraints =  NSLayoutConstraint.constraints(
            withVisualFormat: "H:[search(>=width)]",
            options: [],
            metrics: metrics,
            views: views)
        view.addConstraints(constraints)
        return view
    }

    func createRightView() -> NSView {
        let view = NSStackView()
        view.orientation = .vertical
        view.alignment = .left
        view.distribution = .fill
        view.addArrangedSubview(createReviewField())
        view.addArrangedSubview(debug.add(createReinitFixedZettelButton()))
        view.addArrangedSubview(debug.add(createTagsField()))
        view.addArrangedSubview(debug.add(createZidField()))
        view.addArrangedSubview(debug.add(createExportInputButtons()))
        let contentView = pages.createView(screen)
        contentView.toolTip = Localized("browser-content-view-tooltip")
        view.addArrangedSubview(contentView)
        return view
    }

    func createReviewField() -> NSView {
        isReviewCheckbox = NSButton(title: "", target: nil, action: nil)
        isReviewCheckbox.state = .off
        isReviewCheckbox.setButtonType(.switch)
        isReviewCheckbox.setContentHuggingPriority(.init(245), for: .horizontal)
        let v = sideBySideView(left: NSTextField(labelWithString: "reviewable"), right: isReviewCheckbox)
        v.toolTip = Localized("browser-review-card-checkbox-tooltip")
        return v
    }

    func createTagsField() -> NSView {
        tagsField = TagsTextField()
        tagsField.setContentHuggingPriority(.init(245), for: .horizontal)
        let v = sideBySideView(left: NSTextField(labelWithString: "tags"), right: tagsField)
        v.toolTip = Localized("browser-tags-tooltip")
        return v
    }

    func createZidField() -> NSView {
        zidField = NSTextField(labelWithString: "")
        zidField.isSelectable = true
        zidField.setContentHuggingPriority(.init(245), for: .horizontal)
        let v = sideBySideView(left: NSTextField(labelWithString: "zid"), right: zidField)
        v.toolTip = Localized("browser-tags-tooltip")
        return v
    }

    func createExportInputButtons() -> NSView {
        let exportButton = NSButton(
            title: "export",
            target: self,
            action: #selector(actionExportKasten(_:)))
        exportButton.toolTip = Localized("browser-export-button-tooltip")
        
        let importButton = NSButton(
            title: "import",
            target: self,
            action: #selector(actionImportKasten(_:)))
        importButton.toolTip = Localized("browser-import-button-tooltip")
        
        return sideBySideView(left: exportButton, right: importButton)
    }

    func activeZettel(context c: NSManagedObjectContext) -> Zettel? {
        guard let zid = activeZettelZid else {
            return nil
        }
        return Zettel.select(context: c, zid: zid)
    }

    @IBAction func actionPerformSearchFromSearchBar(_ sender: Any) {
        do {
            let c = appDelegate.context
            let filter = try parseFilterCriteria(search: searchField.stringValue)
            outline.filterResults = try performSearch(context: c, filter: filter)
            outline.reloadData()
            // If we have an active zettel then we set to that zettel.
            if let z = activeZettel(context: c) {
                outline.openTo(zettel: z)
                outline.setSelected(zettel: z)
            }
        } catch {
            // TODO(jyounker): Tell user we can't parse the results
            log.warning("connot perform search: \(error)")
        }
    }
    
    /// Refreshes outline from search bar. Does not update
    func refreshOutlineFromSearchBar() {
        do {
            let filter = try parseFilterCriteria(search: searchField.stringValue)
            outline.filterResults = try performSearch(context: appDelegate.context, filter: filter)
            outline.reloadData()
        } catch {
            // TODO(jyounker): Tell user we can't parse the results
            log.warning("cannot refresh search results: \(error)")
        }
    }
    
    @IBAction func delete(_ sender: Any) {
        guard let active = activeZettel(context: appDelegate.context) else {
            return
        }
        if active.children?.count ?? 0 > 0 {
            // TODO(jyounker): Tell user that zettel can't be deleted/
            log.warning("cannot delete zettel: zettel has children.")
            return
        }
        let zid = active.zid!
        appDelegate.unitOfWork().execute(with: .deleteZettel) { c in
            guard let x = Zettel.select(context: c, zid: zid) else {
                return
            }
            c.delete(x)
        }
    }

    @IBAction func actionSetActiveZettelFromOutline(_ sender: Any) {
        actionFlushZettelChanges(sender)
        setActiveZettelFromOutline()
    }

    func setActiveZettelFromOutline() {
        guard let selection = outline.selected() else {
            return
        }
        clearZettelDetails()
        setActiveZettel(zettel: selection)
    }

    func setActiveZettel(zettel: Zettel?) {
        guard let z = zettel else {
            activeZettelZid = nil
            clearZettelDetails()
            disableZettelDetails()
            return
        }
        activeZettelZid = z.zid!
        enableZettelDetails()
        setZettelDetails(zettel: z)
    }

    func enableZettelDetails() {
        tagsField.isEditable = true
        // TODO(jyounker): Horrible hack to address issues with layout.
        if tagsField.stringValue == "" {
            tagsField.stringValue = " "
        }
        pages.isEditable = true
        isReviewCheckbox.isEnabled = true
    }

    func disableZettelDetails() {
        tagsField.isEditable = false
        pages.isEditable = false
        isReviewCheckbox.isEnabled = false
    }

    /// Set details fields for the zettel
    func setZettelDetails(zettel: Zettel) {
        tagsField.tags = zettel.tagNames
        zidField.stringValue = zettel.zid!.uuidString
        isReviewCheckbox.state = checkboxState(zettel.tagNames.contains("review"))
        let pagesByName = zettel.pagesByName
        if let frontPage = pagesByName[PageName.front] {
            pages.setFrontPage(page: frontPage)
        }
        if let backPage = pagesByName[PageName.back] {
            pages.setBackPage(page: backPage)
        }
    }

    func refreshActiveZettelDetails(context c: NSManagedObjectContext) {
        guard let z = activeZettel(context: c) else {
            return
        }
        setZettelDetails(zettel: z)
    }
    
    func clearZettelDetails() {
        tagsField.tags = []
        zidField.stringValue = ""
        isReviewCheckbox.state = .off
        pages.clear()
    }

    func zettelFlushActions(context c: NSManagedObjectContext, at t: Timestamp) throws -> ZettelActions? {
        guard let z = activeZettel(context: c) else {
            return nil
        }

        if !pages.hasFrontPage() {
            log.error("FUCK: no front page found while building list")
            throw BrowserControllerError.FrontPageRequired
        }

        var changes = ZettelActions()
        changes.append(.update(zid: z.zid!))

        let isCurrentlyReviewable = z.tagNames.contains("review")
        let shouldBeReviewable = isReviewCheckbox.state == .on
        if isCurrentlyReviewable && !shouldBeReviewable {
            changes.append(.unsetTag(name: "review"))
        } else if !isCurrentlyReviewable && shouldBeReviewable {
            changes.append(.setTag(name: "review"))
        }

        if z.tagNames != tagsField.tags {
            changes.append(.tags(name: tagsField.tags))
        }

        let title = pages.title() ?? z.title!
        if z.title != title {
            changes.append(.title(value: title))
        }

        let pagesByName = z.pagesByName
        if pages.frontPageHasChanged() {
            log.info("################### FRONT PAGE CHANGED")
            changes.append(.updatePage(name: PageName.front, value: pages.frontPage().copy() as! NSAttributedString))
        }

        switch action(
            existingPage: pagesByName[PageName.back],
            hasChanges: pages.backPageHasChanged(),
            hasPage: pages.hasBackPage()) {
        case .create:
            changes.append(.createPage(name: .back, value: pages.backPage().copy() as! NSAttributedString))
        case .delete:
            changes.append(.deletePage(name: .back))
        case .update:
            changes.append(.updatePage(name: .back, value: pages.backPage().copy() as! NSAttributedString))
        case .none: break
        }

        // If there is only the update record, the we aren't actually
        // changing anything.
        if changes.count == 1 {
            return nil
        }
        
        // We're changing things, so we should update the modification time.
        changes.append(.modified(at: t))

        return changes

    }
    
    func action(existingPage: Page?, hasChanges: Bool, hasPage: Bool) -> PageUpdateAction {
        if existingPage == nil && hasPage && hasChanges {
            return .create
        } else if existingPage != nil && !hasPage {
            return .delete
        } else if existingPage != nil && hasPage && hasChanges {
            return .update
        } else {
            return .none
        }
    }
    
    enum PageUpdateAction {
        case create
        case update
        case delete
        case none
    }

    func outlineViewSelectionDidChange(_ notification: Notification) {
        // When a user selects a new object then we set that new object.
        actionSetActiveZettelFromOutline(notification)
    }

    @IBAction func actionFlushZettelChanges(_ sender: Any) {
        do {
            guard let actions = try zettelFlushActions(context: appDelegate.context, at: appDelegate.timekeeper.now()) else {
                return
            }
            appDelegate.unitOfWork().execute(with: .changeZettel) { c in
                try performActions(context: c, actions: actions)
            }
        } catch {
            log.error("cannot flush zettel: \(error)")
        }
    }
    
    @IBAction func actionSetParentZettel(_ sender: Any) {
        guard let active = activeZettel(context: appDelegate.context) else {
            return
        }
        appDelegate.controller.actionSelectCreateTab(self)
        appDelegate.controller.create.selector.set(zettel: active)
    }

    @IBAction func actionSelectSearchBar(_ sender: Any) {
        view.window?.makeFirstResponder(searchField)
    }

    @IBAction func actionSelectSearchResults(_ sender: Any) {
        view.window?.makeFirstResponder(outline)
    }

    @IBAction func actionSelectTags(_ sender: Any) {
        view.window?.makeFirstResponder(tagsField)
    }

    // TODO(jyounker): Replace these two with proper first responder handling.
    @IBAction func actionSelectFrontPage(_ sender: Any) {
        pages.actionSelectFrontPage(sender)
    }

    @IBAction func actionSelectBackPage(_ sender: Any) {
        pages.actionSelectBackPage(sender)
    }
    
    @IBAction func actionSingleClickLink(_ sender: Any) {
        guard let ozid = sender as? ObjectiveZid else {
            return
        }
        guard let linkTarget = Zettel.select(context: appDelegate.context, zid: ozid.value) else {
            log.error("cannot to to link \(ozid.value): no such zettel found")
            return
        }
        if let az = activeZettelZid {
            if az == linkTarget.zid {
                return
            }
        }
        actionFlushZettelChanges(sender)
        clearZettelDetails()
        setActiveZettel(zettel: linkTarget)
    }

    @IBAction @objc func actionDoubleClickLink(_ sender: Any) {
        print("DOUBLE CLICK LINK (no action implemented yet)")
    }

    @IBAction func actionToggleReview(_ sender: Any) {
        if isReviewCheckbox.state == .on {
            isReviewCheckbox.state = .off
        } else {
            isReviewCheckbox.state = .on
        }
    }
}

extension BrowseController {
    func addContextObservers() {
        let context = appDelegate.context
            // Add Observer
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(
            self,
            selector: #selector(contextDidSave),
            name: NSNotification.Name.NSManagedObjectContextDidSave,
            object: context)
    }

    @objc func contextDidSave(notification: NSNotification) {
        log.info(">>> browser update")
        guard let userInfo = notification.userInfo else {
            return
        }

        var deletions = Set<Zid>()
        var hasZettelUpdates = false
        var hasInserts = false

        if let deletes = userInfo[NSDeletedObjectsKey] as? Set<NSManagedObject> {
            for x in deletes {
                if let z = x as? Zettel {
                    deletions.insert(z.zid!)
                }
            }
        }
        if let updates = userInfo[NSUpdatedObjectsKey] as? Set<NSManagedObject> {
            hasZettelUpdates = hasZettel(updates)
        }

        if let inserts = userInfo[NSInsertedObjectsKey] as? Set<NSManagedObject> {
            hasInserts = hasZettel(inserts)
        }
        
        DispatchQueue.main.async {
            self.doContextUpdate(deletions: deletions, hasZettelUpdates: hasZettelUpdates, hasInserts: hasInserts)
        }
    }

    /// Brings UI up to date. Run this only on the main thread.
    func doContextUpdate(deletions: Set<Zid>, hasZettelUpdates: Bool, hasInserts: Bool) {
        if let az = activeZettelZid {
            if deletions.contains(az) {
                setActiveZettel(zettel: nil)
            }
        }

        let hasDeletes = deletions.count > 0

        if hasZettelUpdates || hasInserts || hasDeletes {
            // I'm not really completely sure if this is going to cause
            // the focus to be lost, but I hope not.
            self.refreshOutlineFromSearchBar()
            self.setActiveZettelFromOutline()
        }
    }
    
    func hasZettel<X>(_ seq: Set<X>) -> Bool {
        for x in seq {
            if let _ = x as? Zettel {
                return true
            }
        }
        return false
    }
}

enum BrowserControllerError: Error {
    case FrontPageRequired
}

extension BrowseController {

    @IBAction func actionExportKasten(_ sender: Any) {
        let kastenFile = downloadsDirectory().appendingPathComponent("kasten.json")
        do {
            let kasten = try exportKasten(context: appDelegate.context)
            let encoder = JSONEncoder()
            encoder.outputFormatting = .prettyPrinted
            let jsonKasten = try encoder.encode(kasten)
            try jsonKasten.write(to: kastenFile)
            log.info("kasten successfully exported to \(kastenFile)")
        } catch {
            log.error("cannot export kasten: \(error)")
        }
    }

    @IBAction func actionImportKasten(_ sender: Any) {
        let kastenFile = downloadsDirectory().appendingPathComponent("kasten.json")
        do {
            let jsonData = try Data(contentsOf: kastenFile)
            let decoder = JSONDecoder()
            let kasten = try decoder.decode(ExportKasten.self, from: jsonData)
            appDelegate.unitOfWork().execute(with: .importKasten) { c in
                try Kasten.deleteAllZetteln(context: c)
                try importZetteln(context: c, zetteln: kasten.zetteln)
                self.log.info("kasten successfully imported from \(kastenFile)")
            }
        } catch {
            log.error("cannot import kasten: \(error)")
        }
    }
}

extension BrowseController {
    func createReinitFixedZettelButton() -> NSView {
        let button = NSButton(
            title: "re-init fixed zettel",
            target: self,
            action: #selector(actionReinitFixedZettel(_:)))
        button.toolTip = Localized("browser-reinit-fixed-zettel-button-tooltip")
        button.setContentHuggingPriority(.init(245), for: .horizontal)
        return button
    }

    @IBAction func actionReinitFixedZettel(_ sender: Any) {
        appDelegate.unitOfWork().execute(with: .repairKasten) { c in
            try Kasten.updateDefaultZettel(context: c)
        }
    }
}

extension BrowseController {
    @IBAction func actionNewZettelSibling(_ sender: Any) {
        // If a node has a children, then use the node as a parent.
        // If a node has no children, then use the node's parent.
        let c = appDelegate.context
        func selectParent() -> Zid {
            guard let active = Zettel.select(context: appDelegate.context, zid: activeZettelZid) else {
                return FixedZettel.Inbox.zid
            }
            do {
                let children = try Zettel.countChildren(context: c, parent: active)
                if children > 0 {
                    return active.zid!
                }
            } catch {
            }
            return active.parentZid ?? FixedZettel.Inbox.zid
            
        }
        
        startNewZettel(sender, context: appDelegate.context, parent: selectParent())
    }

    @IBAction func actionNewZettelChild(_ sender: Any) {
        startNewZettel(sender, context: appDelegate.context, parent: activeZettelZid ?? FixedZettel.Inbox.zid)
    }

    func startNewZettel(_ sender: Any, context c: NSManagedObjectContext, parent: Zid) {
        let create = appDelegate.controller.create!
        let z = Zettel.select(context: c, zid: parent)!
        create.newBlankZettel()
        create.setParentZettelValues(parent: z)
        appDelegate.controller.actionSelectCreateTab(sender)
    }

}
