//
//  Sqlite.swift
//  zk
//
//  Created by Jeff Younker on 10/31/19.
//  Copyright © 2019 The Blobshop. All rights reserved.
//

import Foundation
import SQLite3

import Logging

class SqliteDb {
    var file : URL!
    let log = Logger(label: "com.theblobshop.zk.Sqlite")

    init(_ file: URL) {
        self.file = file
        log.debug("using file \(file)")
    }

    func open() throws -> SqliteConnection {
        return try SqliteConnection(file)
    }
}

class SqliteConnection {
    var db: OpaquePointer?
    let log = Logger(label: "com.theblobshop.zk.Sqlite")

    init(_ file: URL) throws {
       let status = sqlite3_open(file.path, &db)
       if status == SQLITE_OK {
           return
       }
       log.error("error opening database at \(file.path)")
       if db != nil {
           sqlite3_close(db!)
           db = nil
       }
       throw SqliteError.fromStatus(status)
    }
 
    func close() {
       if db != nil {
            sqlite3_close(db!)
            db = nil
       }
    }
 
    deinit {
        log.error("deinit: closing underlaying DB")
        if db != nil {
             sqlite3_close(db!)
             db = nil
        }
    }

    func query(_ sql: String) throws -> SqliteQuery {
        log.error("\(sql)")
        return try SqliteQuery(conn: self, sql: sql)
    }
    
    func prepare(_ sql: String) throws -> SqlitePreparedStatement {
        log.error("\(sql)")
        return try SqlitePreparedStatement(conn: self, sql: sql)
    }
    
    func exec(_ sql: String) throws {
        let s = sqlite3_exec(db!, sql, nil, nil, nil)
        if s == SQLITE_OK {
            return
        }
        log.error("error executing '\(sql)': \(s)")
        throw SqliteError.fromStatus(s)

    }
    
    func begin() throws {
        try exec("begin")
    }

    func rollback() throws {
        try exec("rollback")
    }

    func commit() throws {
        try exec("commit")
    }
}
 
class SqliteStatement {
    var conn: SqliteConnection!
    let sql: String
    var stmt: OpaquePointer? = nil
    let log = Logger(label: "com.theblobshop.zk.Sqlite")

    init(conn: SqliteConnection, sql: String) throws {
        self.conn = conn
        self.sql = sql
        let s = sqlite3_prepare(conn.db, sql, -1, &stmt, nil)
        if s != SQLITE_OK {
            throw SqliteError.fromStatus(s)
        }
    }
    
    func prepare() throws {
        if stmt != nil {
            throw SqliteError.reusedPreparedStatement
        }
        let s = sqlite3_prepare(conn.db, sql, -1, &stmt, nil)
        if s != SQLITE_OK {
            throw SqliteError.fromStatus(s)
        }
    }
    
    func bind(_ params: Array<Any?>) throws {
        log.error("\(params)")
        for (i, p) in params.enumerated() {
            switch p {
            case is Optional<Int64>: try bind(i+1, int64: p as! Optional<Int64>) ;
            case is Optional<String>: try bind(i+1, string: p as! Optional<String>) ;
            case is Optional<Bool>: try bind(i+1, bool: p as! Optional<Bool>) ;
            default: throw SqliteError.unknownBinding(value: p) ;
            }
        }
    }
    
    func bind(_ i: Int, int64: Int64?) throws {
        if let x = int64 {
            let s = sqlite3_bind_int64(stmt, Int32(i), x)
            if s != SQLITE_OK {
                throw SqliteError.fromStatus(s)
            }
        } else {
            let s = sqlite3_bind_null(stmt, Int32(i))
            if s != SQLITE_OK {
                throw SqliteError.fromStatus(s)
            }
        }
    }
    
    func bind(_ i: Int, string: String?) throws {
        if let x = string {
            let s = sqlite3_bind_text(stmt, Int32(i), NSString(string: x).utf8String, -1, nil)
            if s != SQLITE_OK {
                throw SqliteError.fromStatus(s)
            }
        } else {
            let s = sqlite3_bind_null(stmt, Int32(i))
            if s != SQLITE_OK {
                throw SqliteError.fromStatus(s)
            }
        }
    }

    func bind(_ i: Int, bool: Bool?) throws {
        if let x = bool {
            let s = sqlite3_bind_int(stmt, Int32(i), x ? 1 : 0)
            if s != SQLITE_OK {
                throw SqliteError.fromStatus(s)
            }
        } else {
            let s = sqlite3_bind_null(stmt, Int32(i))
            if s != SQLITE_OK {
                throw SqliteError.fromStatus(s)
            }
        }
    }
    
    func reset() throws {
        let s = sqlite3_reset(stmt)
        if s != SQLITE_OK {
            throw SqliteError.fromStatus(s)
        }
    }
    
    deinit {
        log.error("deinit: finalizing and clearing SqliteStatement reference")
        sqlite3_finalize(stmt)
        self.conn = nil
    }

    func finalize() {
        sqlite3_finalize(stmt)
        self.conn = nil
    }
}

class SqlitePreparedStatement: SqliteStatement {
    func run() throws {
        let s = sqlite3_step(stmt)
        if s != SQLITE_DONE {
            throw SqliteError.fromStatus(s)
        }
    }
}
class SqliteQuery : SqliteStatement {
    func run() throws -> SqliteCursor {
        return SqliteCursor(statement: self)
    }
}

class SqliteCursor : Sequence, IteratorProtocol {
    var statement: SqliteStatement!
    let log = Logger(label: "com.theblobshop.zk.Sqlite")

    init(statement: SqliteStatement) {
        self.statement = statement
    }
    
    func next() -> SqliteRow? {
        let s = sqlite3_step(statement.stmt)
        switch s {
        case SQLITE_ROW: return ValidSqliteRow(statement: self.statement) ;
        case SQLITE_DONE: return nil ;
        default: return FailedNextCall(SqliteError.fromStatus(s)) ;
        }
    }
    
    deinit {
        log.error("deinit: clearing SqliteStatement reference")
        statement = nil
    }
}

class SqliteRow {
    // Generic row that is valid or not.
    let log = Logger(label: "com.theblobshop.zk.Sqlite")

    func getString(_ i: Int) throws -> String {
        throw SqliteError.internalError
    }

    func getOptionalString(_ i: Int) throws -> String? {
        throw SqliteError.internalError
    }

    func getInt64(_ i: Int) throws -> Int64 {
        throw SqliteError.internalError
    }
    
    func getBool(_ i: Int) throws -> Bool {
        throw SqliteError.internalError
    }
}

class ValidSqliteRow : SqliteRow {
    var statement: SqliteStatement!
    init(statement: SqliteStatement) {
        self.statement = statement
    }

    override func getString(_ i: Int) throws -> String {
        guard let v = sqlite3_column_text(statement.stmt, Int32(i)) else {
            throw SqliteError.columnIndexProbablyWrong(index: Int32(i))
        }
        return String(cString: v)
    }

    override func getOptionalString(_ i: Int) throws -> String? {
        if sqlite3_column_type(statement.stmt, Int32(i)) == SQLITE_NULL {
            return nil
        }
        guard let v = sqlite3_column_text(statement.stmt, Int32(i)) else {
            throw SqliteError.columnIndexProbablyWrong(index: Int32(i))
        }
        return String(cString: v)
    }

    override func getInt64(_ i: Int) throws -> Int64 {
        return sqlite3_column_int64(statement.stmt, Int32(i))
    }

    override func getBool(_ i: Int) throws -> Bool {
        return sqlite3_column_int(statement.stmt, Int32(i)) != 0
    }

    deinit {
        log.error("deinit: clearing SqliteStatement reference")
        statement = nil
    }
}

class FailedNextCall : SqliteRow {
    let wrappedException: SqliteError
    
    init(_ e: SqliteError) {
        wrappedException = e
    }
    
    override func getString(_ i: Int) throws -> String {
        throw wrappedException
    }

    override func getOptionalString(_ i: Int) throws -> String? {
        throw wrappedException
    }

    override func getInt64(_ i: Int) throws -> Int64 {
        throw wrappedException
    }

    override func getBool(_ i: Int) throws -> Bool {
        throw wrappedException
    }
}

enum SqliteError : Error {
    case ok
    case error
    case internalError
    case perm
    case abort
    case busy
    case locked
    case nomem
    case readonly
    case interrupt
    case ioerr
    case corrupt
    case notfound
    case full
    case cantopen
    case protocolViolation
    case empty
    case schema
    case toobig
    case constraint
    case mismatch
    case misuse
    case nolfs
    case auth
    case format
    case range
    case notadb
    case notice
    case warning
    case row
    case done
    case notOk(status: Int32)
    case reusedPreparedStatement
    case columnIndexProbablyWrong(index: Int32)
    case unknownBinding(value: Any?)
    
    static func fromStatus(_ status: Int32) -> SqliteError {
        switch status {
        case SQLITE_OK: return ok ;
        case SQLITE_ERROR: return error ;
        case SQLITE_INTERNAL: return SqliteError.internalError ;
        case SQLITE_PERM: return SqliteError.perm ;
        case SQLITE_ABORT: return SqliteError.abort ;
        case SQLITE_BUSY: return SqliteError.busy ;
        case SQLITE_LOCKED: return SqliteError.locked ;
        case SQLITE_NOMEM: return SqliteError.nomem ;
        case SQLITE_READONLY: return SqliteError.readonly ;
        case SQLITE_INTERRUPT: return SqliteError.interrupt ;
        case SQLITE_IOERR: return SqliteError.ioerr ;
        case SQLITE_CORRUPT: return SqliteError.corrupt ;
        case SQLITE_NOTFOUND: return SqliteError.notfound ;
        case SQLITE_FULL: return SqliteError.full ;
        case SQLITE_CANTOPEN: return SqliteError.cantopen ;
        case SQLITE_PROTOCOL: return SqliteError.protocolViolation ;
        case SQLITE_EMPTY: return SqliteError.empty ;
        case SQLITE_SCHEMA: return SqliteError.schema ;
        case SQLITE_TOOBIG: return SqliteError.toobig ;
        case SQLITE_CONSTRAINT: return SqliteError.constraint ;
        case SQLITE_MISMATCH: return SqliteError.mismatch ;
        case SQLITE_MISUSE: return SqliteError.misuse ;
        case SQLITE_NOLFS: return SqliteError.nolfs ;
        case SQLITE_AUTH: return SqliteError.auth ;
        case SQLITE_FORMAT: return SqliteError.format ;
        case SQLITE_RANGE: return SqliteError.range ;
        case SQLITE_NOTADB: return SqliteError.notadb ;
        case SQLITE_NOTICE: return SqliteError.notice ;
        case SQLITE_WARNING: return SqliteError.warning ;
        case SQLITE_ROW: return SqliteError.row ;
        case SQLITE_DONE: return SqliteError.done ;
        default: return SqliteError.notOk(status: status) ;
        }
    }
}

class SqlFragment : CustomStringConvertible {
    let sql: String
    let params: Array<Any>

    init() {
        self.sql = ""
        self.params = Array()
    }

    init(sql: String) {
        self.sql = sql
        self.params = Array()
    }

    init(sql: String, param: Any) {
        self.sql = sql
        self.params = [param]
    }

    init(sql: String, params: Array<Any>) {
        self.sql = sql
        self.params = params
    }

    func append(_ sql: String) -> SqlFragment {
        return SqlFragment(sql: self.sql + sql, params: self.params)
    }

    func append(_ sql: String, _ param: Any) -> SqlFragment {
        return SqlFragment(sql: self.sql + sql, params: self.params + [param])
    }
    
    func append(_ param: Any) -> SqlFragment {
        return SqlFragment(sql: self.sql, params: self.params + [param])
    }
    
    func append(_ frag: SqlFragment) -> SqlFragment {
        return SqlFragment(sql: self.sql + frag.sql, params: self.params + frag.params)
    }
 
    func join(with: String, fragments: Array<SqlFragment>) -> SqlFragment {
        return self.append(SqlFragment.join(with: with, fragments: fragments))
    }

    class func join(with: String, fragments unfilteredFragments: Array<SqlFragment>) -> SqlFragment {
        let fragments = unfilteredFragments.filter{ !$0.isEmpty() }
        if fragments.count == 0 {
            return SqlFragment()
        }
        var joined = SqlFragment(sql: fragments[0].sql, params: fragments[0].params)
        for f in fragments[1...] {
            joined = joined.append(with).append(f)
        }
        return joined
    }
    
    func isEmpty() -> Bool {
        return sql == "" && params.count == 0
    }
    
    public var description: String {
        return """
        SqlFragment(
            sql: \"\(self.sql)\",
            params: \(self.params))
        """
    }
}
