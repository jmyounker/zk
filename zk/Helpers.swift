//
//  Helpers.swift
//  zk
//
//  Created by Jeff Younker on 11/29/19.
//  Copyright © 2019 The Blobshop. All rights reserved.
//

import Cocoa
import Foundation

var appDelegate: AppDelegate {
    return NSApplication.shared.delegate as! AppDelegate
}
    
func withoutNil<X>(_ seq: Array<Optional<X>>) -> Array<X> {
    return seq.filter{ $0 != nil }.map{ $0! }
}

func indexByUniqueKey<K: Hashable, X, C: Sequence>(_ c: C, f: (X)->K) -> [K: X] where C.Element == X {
    var r = [K: X]()
    for x in c {
        r[f(x)] = x
    }
    return r
}

func applyValues<K, X, Y>(_ c: Dictionary<K, X>, f: (X)->Y) -> Dictionary<K, Y> {
    var r = [K: Y]()
    for (k, x) in c {
        r[k] = f(x)
    }
    return r
}

func filterKeys<K, X>(_ c: Dictionary<K, X>, f: (K, X)->Bool) -> Dictionary<K, X> {
    var r = [K: X]()
    for (k, x) in c {
        if f(k, x) {
            r[k] = x
        }
    }
    return r
}

func partition<X>(_ seq: Array<X>, _ f: (X)->Bool) -> (Array<X>, Array<X>) {
    var left: Array<X> = []
    var right: Array<X> = []
    for x in seq {
        if f(x) {
            left.append(x)
        } else {
            right.append(x)
        }
    }
    return (left, right)
}

func staticRegex(pattern: String, options: Array<NSRegularExpression.Options>) -> NSRegularExpression {
    do {
        return try NSRegularExpression(pattern: pattern, options: [.dotMatchesLineSeparators])
    } catch {
        fatalError("cannot parse fixed regular expression")
    }
}

func staticRegex(pattern: String) -> NSRegularExpression {
    staticRegex(pattern: pattern, options: [])
}

func wholeString(_ s: String) -> NSRange {
    return NSRange(location: 0, length: NSString(string: s).length)
}

extension NSObjectProtocol {
    @discardableResult
    func retainMe() -> Self {
      _ = Unmanaged.passRetained(self)
      return self
    }
}

// I'm a lazy MF, and this is better than nothing. Screw you future self. (Also, I'm mostly
// trying to avoid typing the same string repeatedly, so I'm not really worry about German
// right now, but I figure I should use the existing mechanism so the transition won't be
// so brutal in the future.)
func Localized(_ key: String) -> String {
    return NSLocalizedString(key, comment: key)
}

func checkboxState(_ x: Bool) -> NSControl.StateValue {
    if x {
        return .on
    } else {
        return .off
    }
}

func downloadsDirectory() -> URL {
    let paths = FileManager.default.urls(for: .downloadsDirectory, in: .userDomainMask)
    return paths[0]
}
