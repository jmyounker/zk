//
//  DebuggableView.swift
//  zk
//
//  Created by Jeff Younker on 7/29/20.
//  Copyright © 2020 The Blobshop. All rights reserved.
//

import Foundation
import Cocoa

/// DebugController facitates turning views on and off based on a debug flag in AppDelegate.
class DebugController {
    
    var isOn = false

    var debugViews = Array<NSView>()
    
    // Mark a view as being debuggable.
    func add(_ v: NSView) -> NSView {
        v.isHidden = !isOn
        self.debugViews.append(v)
        return v
    }
    
    // Show debug views if true, and hides them if false.
    func show() {
        for x in debugViews {
            x.isHidden = !isOn
        }
    }
}
