//
//  CreateController.swift
//  zk
//
//  Created by Jeff Younker on 12/26/19.
//  Copyright © 2019 The Blobshop. All rights reserved.
//

import CloudKit
import Cocoa
import Foundation

import Logging

class CreateController: NSViewController, NSComboBoxDelegate, AppActions {
    let log = Logger(label: "com.theblobshop.zk.CreateController")

    let defaultZettelZid = FixedZettel.Inbox.zid
    var selectionObserver: NSKeyValueObservation?
    
    var debug: DebugController!
    var screen: Scaler!
    @objc dynamic var selector: KastenComboBox!
    var parentZid: Zid?
    var previousZettelTitle: NSTextField!
    var previousZid: Zid?
    var isReviewCheckbox: NSButton!
    var pages: PageView!

    override var debugDescription: String {
        var x = "CreateController("
        x.append("parentZid: \(String(describing: parentZid))")
        x.append(", ")
        x.append("previousZid: \(String(describing: previousZid))")
        x.append(", ")
        x.append("previousZettelTitle.stringValue: \(String(describing: previousZettelTitle.stringValue))")
        x.append(")")
        return x
    }

    // Used for tracking who made changes to the tags most recently. This allows automatic
    // tag setting to avoid stomping on user changes.
    var tagsLastSetProgrammatically: Array<TagName> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func configure(screen: Scaler) {
        self.debug = appDelegate.debug
        self.screen = screen
        self.pages = PageView()
        self.pages.configure(screen: screen)
    }
    
    func createView() {
        // Create containing view.
        let view = NSStackView()
        view.orientation = .vertical
        view.alignment = .left
        self.view = view
        
        // Debug Controls
        let dumpButton = NSButton(
            title: "dump",
            target: self,
            action: #selector(actionDump(_:))
        )
        view.addArrangedSubview(debug.add(dumpButton))
        
        // Create top grid view
        let topControls = NSGridView(numberOfColumns: 3, rows: 4)
        view.addArrangedSubview(topControls)
        topControls.column(at: 0).xPlacement = .trailing

        // Create subject select combo box
        topControls.cell(atColumnIndex: 0, rowIndex: 0).contentView = NSTextField(
            labelWithString: "parent card")
        selector = KastenComboBox(string: "")
        selector.defaultSelection = defaultZettelZid
        selector.usesDataSource = true
        selector.dataSource = selector
        selector.completes = true
        selector.delegate = selector
        selector.setContentHuggingPriority(.init(245), for: .horizontal)
        topControls.cell(atColumnIndex: 1, rowIndex: 0).contentView = selector
        topControls.cell(atColumnIndex: 0, rowIndex: 0).contentView!.toolTip = Localized("create-parent-card-selector-tooltip")
        topControls.cell(atColumnIndex: 1, rowIndex: 0).contentView!.toolTip = Localized("create-parent-card-selector-tooltip")

        // Add clear button to side of combobox
        let setParentToInboxButton = NSButton(
            title: "inbox",
            target: nil,
            action: #selector(actionSetParentToDefault(_:)))
        topControls.cell(atColumnIndex: 2, rowIndex: 0).contentView = setParentToInboxButton
        topControls.cell(atColumnIndex: 2, rowIndex: 0).contentView!.toolTip = Localized("create-clear-parent-card-button-tooltip")
        let constraints =  NSLayoutConstraint.constraints(
            withVisualFormat: "H:[selector(>=width)]",
            options: [],
            metrics: ["width": screen.x(20)],
            views: ["selector": selector] as Dictionary<String, NSView>)
        selector.addConstraints(constraints)

        // Put in previous tag
        topControls.cell(atColumnIndex: 0, rowIndex: 1).contentView = NSTextField(
            labelWithString: "last card")
        previousZettelTitle = NSTextField(labelWithString: "")
        previousZettelTitle.setContentHuggingPriority(.init(245), for: .horizontal)
        topControls.cell(atColumnIndex: 1, rowIndex: 1).contentView = previousZettelTitle
        topControls.cell(atColumnIndex: 0, rowIndex: 1).contentView!.toolTip = Localized("create-last-card-field-tooltip")
        topControls.cell(atColumnIndex: 1, rowIndex: 1).contentView!.toolTip = Localized("create-last-card-field-tooltip")
        
        let subjectButton = NSButton(
            title: "use as parent",
            target: nil,
            action: #selector(actionSetParentZettel(_:)))
        topControls.cell(atColumnIndex: 2, rowIndex: 1).contentView = subjectButton
        topControls.cell(atColumnIndex: 2, rowIndex: 1).contentView!.toolTip = Localized("create-last-card-becomes-parent-button-tooltip")

        // Put in review card checkbox
        topControls.cell(atColumnIndex: 0, rowIndex: 2).contentView = NSTextField(
            labelWithString: "review this card")
        isReviewCheckbox = NSButton(title: "", target: nil, action: nil)
        isReviewCheckbox.state = .off
        isReviewCheckbox.setButtonType(.switch)
        isReviewCheckbox.setContentHuggingPriority(.init(245), for: .horizontal)
        topControls.cell(atColumnIndex: 1, rowIndex: 2).contentView = isReviewCheckbox
        topControls.cell(atColumnIndex: 0, rowIndex: 2).contentView!.toolTip = Localized("create-review-card-checkbox-tooltip")
        topControls.cell(atColumnIndex: 1, rowIndex: 2).contentView!.toolTip = Localized("create-review-card-checkbox-tooltip")

        // Add pages in middle
        let pagesView = pages.createView(screen)
        pagesView.toolTip = Localized("create-page-content-tooltip")
        view.addArrangedSubview(pagesView)

        // Add create button on left
        let createButtonView = NSGridView(numberOfColumns: 1, rows: 1)
        view.addArrangedSubview(createButtonView)
        createButtonView.column(at: 0).xPlacement = .trailing
        let createButton = NSButton(
            title: "create zettel",
            target: nil,
            action: #selector(actionCreateZettel(_:)))
        createButtonView.cell(atColumnIndex: 0, rowIndex: 0).contentView = createButton
        createButtonView.cell(atColumnIndex: 0, rowIndex: 0).contentView!.toolTip = Localized("create-create-button-tooltip")
        
        addContextObservers()
        
        selector.set(zettel: Zettel.select(context: appDelegate.context, zid: defaultZettelZid))
    }

    func comboBoxSelectionDidChange() {
        do {
            log.info("Combobox caused change: \(self.debugDescription)")
            try setParentZettelFromComboBox(context: appDelegate.context)
        } catch {
            // TOOD(jyounker): add alert to user
            log.error("cannot set parent from combobox: \(error)")
        }
    }
    
    @IBAction func actionDump(_ sender: Any) {
        log.info("\(self.debugDescription)")
    }
    
    @IBAction func actionSetParentZettelFromComboBox(_ sender: Any) {
        do {
            try setParentZettelFromComboBox(context: appDelegate.context)
        } catch {
            // TOOD(jyounker): add alert to user
            log.error("cannot set parent from combobox: \(error)")
        }
    }
    
    // Only call from main thread.
    func setParentZettelFromComboBox(context c: NSManagedObjectContext) throws {
        self.parentZid = selector.selection
        if self.parentZid == nil {
            clearPreviousZettel()
        } else {
            try setPreviousZettelFromParent(context: c)
        }
    }

    @IBAction func actionSetParentZettel(_ sender: Any) {
        do {
            let c = appDelegate.context
            try setParentZettelFromPrevious(context: c)
        } catch {
            // TODO(jyounker): add alert to user
            log.error("cannot set previous as parent: \(error)")
        }
    }

    // Only call from main thread.
    func setParentZettelFromPrevious(context c: NSManagedObjectContext) throws {
        guard let previous = Zettel.select(context: c, zid: previousZid) else {
            clearParentZettel()
            return
        }
        parentZid = previous.zid!
        selector.set(zettel: previous)  // This does all the magic.
    }

    @IBAction func actionCreateZettel(_ sender: Any) {
        do {
            // On the main thread we get the definition.
            let zd = try zettelDef()
            // On the background thread we create it.
            appDelegate.unitOfWork().execute(with: .createZettel) {
                c in
                _ = try self.createZettel(context: c, zettelDef: zd)
                // And after we create it, we update the UI on the main thread.
                DispatchQueue.main.async {
                    self.newBlankZettel()
                    guard let pz = zd.parentZid else {
                        self.selector.clear()
                        return
                    }
                    let p = Zettel.select(context: appDelegate.context, zid: pz)
                    self.selector.set(zettel: p)
                }
            }
        } catch (ZettelCreateFailure.noTitle) {
            log.error("cannot create zettel: no discernable title")
            return
        } catch (ZettelCreateFailure.noFrontPage) {
            log.error("cannot create zettel: no front page")
            return
        } catch {
            log.error("cannot create zettel: \(error)")
            return
        }
    }
    
    @IBAction func actionSetParentToDefault(_ sender: Any) {
        let c = appDelegate.context
        guard let z = Zettel.select(context: c, zid: defaultZettelZid) else {
            clearParentZettel()
            return
        }
        setParentZettelValues(parent: z)
    }
    
    // Only call from main thread.
    func clearParentZettel() {
        parentZid = nil
        selector.clear()
    }
    
    func clearPreviousZettel() {
        previousZettelTitle.stringValue = ""
        previousZid = nil
    }
    
    func newBlankZettel() {
        pages.clear()
    }

    func setParentZettelValues(parent p: Zettel) {
        parentZid = p.zid!
        selector.set(zettel: p)
    }

    func setPreviousZettelFromParent(context c: NSManagedObjectContext) throws {
        setPreviousZettelValues(previous: try lastChildZettel(context: c, parentZid: parentZid))
    }

    func setPreviousZettelValues(previous: Zettel?) {
        guard let p = previous else {
            previousZettelTitle.stringValue = ""
            previousZid = nil
            return
        }
        previousZettelTitle.stringValue = p.title!
        previousZid = p.zid!
    }
    
    @IBAction func actionToggleReview(_ sender: Any) {
        if isReviewCheckbox.state == .on {
            isReviewCheckbox.state = .off
        } else {
            isReviewCheckbox.state = .on
        }
    }

    struct ZettelDef {
        let zid: Zid
        let parentZid:Zid?
        let title:String
        let frontPage:NSAttributedString
        let backPage:NSAttributedString?
        let reviewable:Bool
    }

    func zettelDef() throws -> ZettelDef {
        func backPage() -> NSAttributedString? {
            if !pages.hasBackPage() {
                return nil
            }
            return (pages.backPage().copy() as! NSAttributedString)
        }

        if !pages.hasFrontPage() {
            throw ZettelCreateFailure.noFrontPage
        }
        let frontPage = pages.frontPage().copy() as! NSAttributedString
        guard let title = pages.title() else {
            throw ZettelCreateFailure.noTitle
        }
        return ZettelDef(
            zid: Zid(),
            parentZid: parentZid ?? defaultZettelZid,
            title: title,
            frontPage: frontPage,
            backPage: backPage(),
            reviewable: (isReviewCheckbox.state == .on))
    }

    enum ZettelCreateFailure: Error {
        case noFrontPage
        case noTitle
    }
    
    // MARK: - Create zettel
    func createZettel(context c: NSManagedObjectContext, zettelDef zd: ZettelDef) throws -> Zid {
        
        // create new blank zettel, open transaction
        let z = Zettel(context: c)
        z.zid = zd.zid
        z.title = zd.title
        if zd.reviewable {
            z.set(tag: "review")
        }

        // Bring into our context.
        let parent = Zettel.select(context: c, zid: zd.parentZid)
        
        let pz = try parentOrInbox(context: c, parent: parent)
        // We need to get the new seq before we assign values to parent and parentZid, otherwise
        // we include the new child in the calculations.
        let seq = try pz.nextChildSeq(context: c)
        z.setParentage(zettel: pz)
        z.seq = seq

        // create front page
        let front = try Page.from(
            context: c, zid: z.zid!, name: .front, rtfd: zd.frontPage)
        z.addToPages(front)

        // if back page, create back page
        if let backPage = zd.backPage {
            let back = try Page.from(
                context: c, zid: z.zid!, name: .back, rtfd: backPage)
            z.addToPages(back)
        }
        return z.zid!
    }

    // Note that as a happy side-effect this method never allows us to create a top-level parent.
    func parentOrInbox(context c: NSManagedObjectContext, parent: Zettel?) throws -> Zettel {
        guard let p = parent else {
            return try Kasten.getFixed(context: c, zettel: .Inbox)
        }
        return p
    }
}

enum ZettelCreateError: Error {
    case parentMissing
}

// MARK: - Model updates
extension CreateController {
    func addContextObservers() {
        let context = appDelegate.context
            // Add Observer
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(
            self,
            selector: #selector(contextDidSave),
            name: NSNotification.Name.NSManagedObjectContextDidSave,
            object: context)
        
        selectionObserver = selector.observe(\.selection, options: [.old, .new]) {
            [weak self] object, change in self?.comboBoxSelectionDidChange()
        }
    }

    @objc func contextDidSave(notification: NSNotification) {
        guard let userInfo = notification.userInfo else {
            return
        }

        var parentWasDeleted = false
        var parentWasChanged = false
        var childWasAdded = false
        var childWasChanged = false

        let parentZid = self.parentZid

        if let deletes = userInfo[NSDeletedObjectsKey] as? Set<NSManagedObject> {
            for x in deletes {
                if let z = x as? Zettel {
                    if z.zid == parentZid {
                        parentWasDeleted = true
                    }
                }
            }
        }
        
        if let updates = userInfo[NSUpdatedObjectsKey] as? Set<NSManagedObject> {
            for x in updates {
                if let z = x as? Zettel {
                    if z.zid == parentZid {
                        parentWasChanged = true
                    }
                    if z.parentZid == parentZid {
                        childWasChanged = true
                    }
                }
            }
        }

        if let inserts = userInfo[NSInsertedObjectsKey] as? Set<NSManagedObject> {
            for x in inserts {
                if let z = x as? Zettel {
                    if z.parentZid == parentZid {
                        childWasAdded = true
                    }
                }
            }
        }

        DispatchQueue.main.async {
            let c = appDelegate.context

            if parentWasDeleted {
                self.clearParentZettel()
                self.clearPreviousZettel()
            }

            if parentWasChanged {
                do {
                    try self.setParentZettelFromComboBox(context: c)
                } catch {
                    self.log.info("cannot update parent: \(error)")
                }
            }
            if childWasAdded || childWasChanged {
                // update previous
            }
        }
    }
    
}

// MARK: - Navigation
extension CreateController {
    @IBAction func actionSelectSearchBar(_ sender: Any) {
        view.window?.makeFirstResponder(selector)
    }

    // TODO(jyounker): Replace these two with proper first responder handling.
    @IBAction func actionSelectFrontPage(_ sender: Any) {
        pages.actionSelectFrontPage(sender)
    }

    @IBAction func actionSelectBackPage(_ sender: Any) {
        pages.actionSelectBackPage(sender)
    }
    
    @IBAction func actionSingleClickLink(_ sender: Any) {
        guard let ozid = sender as? ObjectiveZid else {
            return
        }
        NSApplication.shared.sendAction(
            #selector(MasterViewController.actionSelectBrowseTab(_:)), to: nil, from: self)
        NSApplication.shared.sendAction(
            #selector(BrowseController.actionSingleClickLink(_:)), to: nil, from: ozid)
    }

    @IBAction func actionDoubleClickLink(_ sender: Any) {
        print("DOUBLE CLICK LINK (create)")
    }
}

enum PageCreateError: Error {
    case NoFrontPage
    case CreateNoTitle
}

