//
//  AppDelegate.swift
//  zk
//
//  Created by Jeff Younker on 12/25/19.
//  Copyright © 2019 The Blobshop. All rights reserved.
//

import Cocoa

import Logging

@NSApplicationMain
    class AppDelegate: NSObject, NSApplicationDelegate {
    let log = Logger(label: "com.theblobshop.zk.AppDelegate")

    // Global debug switch. See DebugController.
    var debug = DebugController()

    let container = Container(name: "zk", authorName: "com.theblobshop.zk.macos")
    var window: NSWindow!
    var controller: MasterViewController!
    @objc dynamic var reviewSummary: ReviewSummary = ReviewSummary(new: 0, learning: 0, learned: 0)
    var summaryUpdater = SummaryUpdater()
    let timekeeper = Timekeeper()
    
    func load() {
        let windowSize = Dimensions(x: 200, y: 200)
        let screen = Scaler.from(NSScreen.main!)
        window = NSWindow(
            contentRect: screen.rect(windowSize),
            styleMask: [.titled, .resizable, .miniaturizable, .closable],
            backing: .buffered,
            defer: false)
        controller = MasterViewController()
        controller.configure(screen: screen)
        window.contentView = controller.createView(screen)
        window.contentViewController = controller
        window.makeKeyAndOrderFront(window)
    }

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        let c = appDelegate.context
        log.info("initialize kasten if needed")
        try! Kasten.initDefaultZettel(context: c)
        log.info("repair kasten if needed")
        try! Kasten.repair(context: c)
        try! c.save()
        load()
        // Ensure that the browser is populated.
        controller.browse.actionPerformSearchFromSearchBar(self)
    }

    func applicationWillTerminate(_ aNotification: Notification) {
    }

    // MARK: - Core Data stack

    func unitOfWork() -> UnitOfWork {
        return container.unitOfWork()
    }
    var context: NSManagedObjectContext { get { return container.context } }
    var persistentContainer: NSPersistentContainer { get { return container.persistentContainer } }

    // MARK: - Core Data Saving and Undo support
    
    @IBAction func saveAction(_ sender: AnyObject?) {
        // Performs the save action for the application, which is to send the save: message to the application's managed object context. Any encountered errors are presented to the user.
        let context = self.context

        if !context.commitEditing() {
            NSLog("\(NSStringFromClass(type(of: self))) unable to commit editing before saving")
        }
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Customize this code block to include application-specific recovery steps.
                let nserror = error as NSError
                NSApplication.shared.presentError(nserror)
            }
        }
    }

    func windowWillReturnUndoManager(window: NSWindow) -> UndoManager? {
        // Returns the NSUndoManager for the application. In this case, the manager returned is that of the managed object context for the application.
        return context.undoManager
    }

    func applicationShouldTerminate(_ sender: NSApplication) -> NSApplication.TerminateReply {
        // Save changes in the application's managed object context before the application terminates.
        let context = self.context
        
        if !context.commitEditing() {
            NSLog("\(NSStringFromClass(type(of: self))) unable to commit editing to terminate")
            return .terminateCancel
        }
        
        if !context.hasChanges {
            return .terminateNow
        }
        
        do {
            try context.save()
         } catch {
            let nserror = error as NSError

            // Customize this code block to include application-specific recovery steps.
            let result = sender.presentError(nserror)
            if (result) {
                return .terminateCancel
            }
            
            let question = NSLocalizedString("Could not save changes while quitting. Quit anyway?", comment: "Quit without saves error question message")
            let info = NSLocalizedString("Quitting now will lose any changes you have made since the last successful save", comment: "Quit without saves error question info");
            let quitButton = NSLocalizedString("Quit anyway", comment: "Quit anyway button title")
            let cancelButton = NSLocalizedString("Cancel", comment: "Cancel button title")
            let alert = NSAlert()
            alert.messageText = question
            alert.informativeText = info
            alert.addButton(withTitle: quitButton)
            alert.addButton(withTitle: cancelButton)
            
            let answer = alert.runModal()
            if answer == .alertSecondButtonReturn {
                return .terminateCancel
            }
        }
        // If we got here, it is time to quit.
        return .terminateNow
    }
}
