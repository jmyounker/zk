//
//  MasterViewController.swift
//  zk
//
//  Created by Jeff Younker on 12/25/19.
//  Copyright © 2019 The Blobshop. All rights reserved.
//

import Cocoa
import Foundation

var cardSize = Dimensions(x: 105, y:75)
var cardMinSize = Dimensions(x: 105, y:30)
var windowSize = Dimensions(x: 150, y: 150)
var outlineSize = Dimensions(x: 30, y: 90)

class MasterViewController: NSViewController {
    var tabView: NSTabView {
        get { return self.view as! NSTabView }
        set(x) { self.view = x }
    }
    var tabViews: Dictionary<String, NSTabViewItem> = [:]
    var screen: Scaler!
    var browse: BrowseController!
    var create: CreateController!
    var review: ReviewController!
    var debug: DebugController!
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }

    required init(coder: NSCoder) {
        super.init(coder: coder)!
    }
    
    func configure(screen: Scaler) {
        self.debug = appDelegate.debug
        self.screen = screen
        browse = BrowseController()
        create = CreateController()
        review = ReviewController()
        browse.configure(screen: screen)
        create.configure(screen: screen)
        review.configure(screen: screen)
    }
    
    func createView(_ screen: Scaler) -> NSView {
        tabView = NSTabView()  // automaticalll sets tab view

        // Add browse tab.
        browse.createView()
        let browseItem = createTabItem(name: "browse", controller: browse)
        browseItem.toolTip = Localized("master-browse-tab-tooltip")
        tabView.addTabViewItem(browseItem)

        // Add create tab.
        create.createView()
        let createItem = createTabItem(name: "create", controller: create)
        createItem.toolTip = Localized("master-create-tab-tooltip")
        tabView.addTabViewItem(createItem)

        // Add review tab.
        review.createView()
        let reviewItem = createTabItem(name: "review", controller: review)
        reviewItem.toolTip = Localized("master-review-tab-tooltip")
        tabView.addTabViewItem(reviewItem)

        return tabView
    }
    
    func createTabItem(name: String, controller: NSViewController) -> NSTabViewItem {
        let tab = NSTabViewItem()
        tab.label = name
        tab.viewController = controller
        self.tabViews[name] = tab
        return tab
    }
}

// MARK: - Navigation
extension MasterViewController {
    @IBAction func actionSelectBrowseTab(_ sender: Any) {
        let tab = tabViews["browse"]!
        tabView.selectTabViewItem(tab)
        tab.view!.window!.makeFirstResponder(tab.viewController)
    }

    @IBAction func actionSelectCreateTab(_ sender: Any) {
        let tab = tabViews["create"]!
        tabView.selectTabViewItem(tab)
        tab.view!.window!.makeFirstResponder(tab.viewController)
    }

    @IBAction func actionSelectReviewTab(_ sender: Any) {
        let tab = tabViews["review"]!
        tabView.selectTabViewItem(tab)
        tab.view!.window!.makeFirstResponder(tab.viewController)
    }
    
}

extension MasterViewController {
    @IBAction func actionToggleDebug(_ sender: Any) {
        debug.isOn = !debug.isOn
        debug.show()
    }
}
