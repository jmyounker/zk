//
//  Time.swift
//  zk
//
//  Created by Jeff Younker on 12/26/19.
//  Copyright © 2019 The Blobshop. All rights reserved.
//

import Foundation

class Timekeeper {
    func now() -> Timestamp {
        return Timestamp(Date().timeIntervalSince1970)
    }
    func timezone() -> TimeZone {
        return TimeZone.current
    }
}

typealias Timestamp = Int64

extension Timestamp {
    func dump() -> String {
        let d = Date(timeIntervalSince1970: Double(self))
        let f = DateFormatter()
        f.timeZone = TimeZone(abbreviation: "GMT")
        f.locale = NSLocale.current
        f.dateFormat = "yyyMMdd HH:mm"
        return f.string(from: d)
    }
}
