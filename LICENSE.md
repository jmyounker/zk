License
=

Copyright 2019, Jeff Younker, The Blobshop

You can build this application for your own use. You can make
changes and build them for your own use. You cannot redistribute
this application in any form even for internal organizational
use.

"Note Taking or Spaced Repition (Flashcard) Application" will be
referred to as NTSPFA from here on.

You can use this code or portions of this code in your own
personal projects within the following limitations:

* You include attribution and this license statement.
* It is not to be used in a distributed NTSPFA.
* You do not redistribute this build artifacts in any form.
* You do not charge for access to the build artifacts in any form.
* You do not run a NTSPFA or SaaS based on this application or
  using this application.
* Interoperability of data is fine. No application is an island.

TL;DR
==

If you want to use it for yourself, and you're willing to build
the damn thing yourself, you're welcome to do so.

If you want to do something unrelated using chunks of this code,
you can do so.

If you want to make money off of my work, please contact me.

