//
//  OutlineSearchTests.swift
//  zkTests
//
//  Created by Jeff Younker on 12/11/19.
//  Copyright © 2019 The Blobshop. All rights reserved.
//

import Foundation
import XCTest
@testable import zk

class OutlineSearchTests: XCTestCase {
    var z1: Zettel!
    var z11: Zettel!
    var z111: Zettel!
    var z1111: Zettel!
    var z12: Zettel!
    var tempdir: TempDir!
    var container: NSPersistentContainer!
    var context: NSManagedObjectContext!
    var kasten: Kasten!

    override func setUp() {
        tempdir = TempDir()
        container = FullModelFixture.newContainer(file: tempdir.file("model"))
        let c = container.viewContext
        z1 = zettel(context: c, zid: Zid(), title: "t1", parent: nil)
        z11 = zettel(context: c, zid: Zid(), title: "t11", parent: z1)
        z111 = zettel(context: c, zid: Zid(), title: "t111", parent: z11)
        z1111 = zettel(context: c, zid: Zid(), title: "t1111", parent: z111)
        z12 = zettel(context: c, zid: Zid(), title: "t11", parent: z1)
        save(c)
        kasten = Kasten(context: container.viewContext, timekeeper: Timekeeper())
        context = c
    }

    override func tearDown() {
        if tempdir != nil {
            tempdir.close()
        }
    }

    func testMissingParents() {
        struct TestCase {
            let found: Array<Zettel>
            let want: Array<Zettel>
        }
        let testCases = [
            TestCase(found: [], want: []),
            TestCase(found: [z1, z11], want: []),
            TestCase(found: [z11, z1], want: []),
            TestCase(found: [z11, z111], want: [z1]),
            TestCase(found: [z1], want: []),
            TestCase(found: [z11], want: [z1]),
            TestCase(found: [z111], want: [z1, z11]),
        ]
        for tc in testCases {
            do {
                let recovered = try missingParents(kasten, tc.found)
                XCTAssertEqual(Set(recovered.map{ $0.zid }), Set(tc.want.map{ $0.zid }))
            } catch {
                XCTFail()
            }
        }
    }

    func testCompleteResults() {
        struct TestCase {
            let found: Array<Zettel>
            let wantRoots: Array<Zettel>
        }
        let testCases = [
            TestCase(found: [], wantRoots: []),
            TestCase(found: [z1, z11], wantRoots: [z1]),
            TestCase(found: [z11, z1], wantRoots: [z1]),
            TestCase(found: [z1111, z1], wantRoots: [z1]),
            // This is the case that fails with original code.
            TestCase(found: [z111, z1], wantRoots: [z1]),
            TestCase(found: [z1], wantRoots: [z1]),
            TestCase(found: [z11], wantRoots: [z1]),
            TestCase(found: [z11, z12], wantRoots: [z1]),
            TestCase(found: [z111, z12], wantRoots: [z1]),
        ]
        for tc in testCases {
            do {
                let recovered = try completeResults(kasten, tc.found)
                print(recovered.roots.map{ $0.zettel.parent })
                print(tc.wantRoots.map{ $0.parent })
                XCTAssertEqual(Set(recovered.roots.map{ $0.zettel.zid }), Set(tc.wantRoots.map{ $0.zid }))
            } catch {
                XCTFail()
            }
        }
    }

}
