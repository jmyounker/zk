//
//  DragAndDropTests.swift
//  zkTests
//
//  Created by Jeff Younker on 5/10/20.
//  Copyright © 2020 The Blobshop. All rights reserved.
//

import XCTest
import CoreData
@testable import zk

class DragAndDropTests: XCTestCase {

    let zof = ZettelOutlineView(
        screen: zk.Scaler(x: 200, y: 200), frame: zk.Dimensions(x: 30, y: 40))

    func testSelectFrom() {
        // at root from zero when nothing
        //  (name: "n1", parent: nil, seq: 0)
        // [], []
        struct ZettelFixture {
            let title: String
            let parent: String?
            let seq: Int64
        }
        struct TestCase{
            let zetteln: Array<ZettelFixture>
            let parent: String?
            let seq: Int64
            let expected: Array<String>
        }
        let testCases = [
            TestCase(zetteln: [], parent: nil, seq: 0, expected: []),
        ]
        // at root from zero when something
        
        // at root from when something less than something
        // at root from when something greater than all something
        // not at root from zero when nothing
        // not at root from zero when something
        // not at root from when less than something
        // not at root from when greater than all something

        for tc in testCases {
            FullModelFixture.withOneShotContext{ c in
                var zByTitle = [String: Zettel]()
                for zf in tc.zetteln {
                    let parent: Optional<Zettel>
                    if let p = zf.parent {
                        parent = zByTitle[p]
                    } else {
                        parent = nil
                    }
                    let z = zettel(
                        context: c,
                        title: zf.title,
                        parent: parent,
                        seq: zf.seq)
                }
                let s = try Zettel.select(context: c, parent: nil, from: tc.seq)
                XCTAssertEqual(s,tc.expected)
            }
        }
    }
    
    func testShiftZetteln() {
        // at root from zero when nothing zero
        // at root from zero when nothing positive
        // at root from zero when nothing negative
        // at root from zero when something zero
        // at root from zero when something positive
        // at root from zero when something negative
        // at root when
        // shift
        
    }

    func testInsertZetteln() {
        
    }

    func testRemoveZetteln() {
        
    }
}

