//
//  ModelFixtures.swift
//  zkTests
//
//  Created by Jeff Younker on 1/4/20.
//  Copyright © 2020 The Blobshop. All rights reserved.
//

import CoreData
@testable import zk

class FullModelFixture {
    static func newContainer(file: URL) -> NSPersistentContainer {
        let container = NSPersistentContainer(name: "zk")
        let description = NSPersistentStoreDescription(url: file)
        description.type = NSSQLiteStoreType
        container.persistentStoreDescriptions = [description]
        container.loadPersistentStores(
          completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }
    
    static func withOneShotContext(persist: Bool = false, f: (NSManagedObjectContext) throws ->()) {
        withOneShotContainer(persist: persist) { (container)  in
            try f(container.viewContext)
        }
    }

    static func withOneShotContainer(persist: Bool = false, f: (NSPersistentContainer) throws ->()) {
        do {
            try withTempdir(persist: persist) { (tempdir) in
                try f(newContainer(file: tempdir.file("oneshotdb")))
            }
        } catch {
            fatalError()
        }
    }
}

class FastModelFixture {
    lazy var persistentContainer: NSPersistentContainer = { newContainer() }()
    
    func newContainer() -> NSPersistentContainer {
        let container = NSPersistentContainer(name: "zk")
        let description = NSPersistentStoreDescription()
        description.type = NSInMemoryStoreType
        container.persistentStoreDescriptions = [description]
        container.loadPersistentStores(
          completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }
}

class MockKasten: Kasten {
    let zettelByZid: [UUID: Zettel]
    
    init(zetteln: Array<Zettel>) {
        zettelByZid = indexByUniqueKey(zetteln) { $0.zid! }
        super.init(timekeeper: Timekeeper())
    }

    func selectZetteln(_ query: ZettelQuery) throws -> Array<Zettel> {
        return withoutNil(query.zids.map{ zettelByZid[$0] })
    }
}

class TempDir {
    let persist: Bool
    
    init(persist: Bool = false) {
        self.persist = persist
    }

    lazy var path: URL = {
        do {
            return try createTempdir()
        } catch {
            fatalError("cannot create tempdir: \(error)")
        }
    }()
    
    func createTempdir() throws -> URL {
        let userTempdir = URL(fileURLWithPath: NSTemporaryDirectory(), isDirectory: true)
        let tempdir = userTempdir.appendingPathComponent(ProcessInfo().globallyUniqueString)
        try FileManager.default.createDirectory(atPath: tempdir.path, withIntermediateDirectories: false, attributes: nil)
        return tempdir
    }
    
    func file(_ filename: String) -> URL {
        return path.appendingPathComponent(filename)
    }
    
    func close() {
        if persist {
            return
        }
        do {
            try FileManager.default.removeItem(at: path)
        } catch {
        }
    }

    deinit {
        close()
    }
}

func withTempdir(persist: Bool=false, f: (TempDir) throws ->()) rethrows {
    let t = TempDir(persist: persist)
    try f(t)
}

func zettel(context: NSManagedObjectContext, zid: Zid, title: String, parent: Zettel?) -> Zettel {
    let z = Zettel(context: context)
    z.zid = zid
    z.title = title
    z.parentZid = parent?.zid ?? nil
    z.parent = parent
    return z
}

func zettel(zid: Zid, title: String, parent: Zid?) -> Zettel {
    let z = Zettel()
    z.zid = zid
    z.title = title
    z.parentZid = parent
    return z
}

func zettel(context c: NSManagedObjectContext, title: String) -> Zettel {
    let z = Zettel(context: c)
    z.zid = Zid()
    z.title = title
    return z
}

func zettel(context c: NSManagedObjectContext, title: String, tags: Array<String>) -> Zettel {
    let z = Zettel(context: c)
    z.zid = Zid()
    z.title = title
    z.tagNames = tags
    return z
}

