//
//  LeitnerReviewSchedule.swift
//  zkTests
//
//  Created by Jeff Younker on 3/21/20.
//  Copyright © 2020 The Blobshop. All rights reserved.
//

import XCTest
import CoreData
@testable import zk

class LeitnerReviewSchedule: XCTestCase {

    func testTestEnvironmentFirstRequestEmpty() {
        FullModelFixture.withOneShotContext() { context in
            let request: NSFetchRequest<FirstReview> = FirstReview.fetchRequest()
            request.returnsObjectsAsFaults = false
            XCTAssertEqual(count(context, request), 0)
        }
    }
    
    func testTestEnvironmentReviewEventEmpty() {
        FullModelFixture.withOneShotContext() { context in
            let request: NSFetchRequest<ReviewEvent> = ReviewEvent.fetchRequest()
            request.returnsObjectsAsFaults = false
            XCTAssertEqual(count(context, request), 0)
        }
    }

    func testInitialCallGeneratesFirstReviewRecord() {
        for outcome in [LearningStatus.LEARNED, LearningStatus.NOT_LEARNED] {
            FullModelFixture.withOneShotContext() { context in
                let z = zettel(context: context, title: "Z1")
                save(context)
                let now = Timekeeper().now()

                try learn(context: context, zettel: z, at: now, outcome: outcome)
                
                let request: NSFetchRequest<FirstReview> = FirstReview.fetchRequest()
                request.predicate = nil
                request.returnsObjectsAsFaults = false
                XCTAssertEqual(count(context, request), 1)
            }
        }
    }

    func testSubsequentCallDoesNotGenerateFirstReviewRecord() {
        for outcome in [LearningStatus.LEARNED, LearningStatus.NOT_LEARNED] {
            FullModelFixture.withOneShotContext() { context in
                let z = zettel(context: context, title: "Z1")
                let now = Timekeeper().now()
                try learn(context: context, zettel: z, at: now, outcome: outcome)
                save(context)

                try learn(context: context, zettel: z, at: now, outcome: outcome)

                let request: NSFetchRequest<FirstReview> = FirstReview.fetchRequest()
                request.predicate = nil
                request.returnsObjectsAsFaults = false
                XCTAssertEqual(count(context, request), 1)
            }
        }
    }

    func testFirstReviewHasExpectedValues() {
        for outcome in [LearningStatus.LEARNED, LearningStatus.NOT_LEARNED] {
            FullModelFixture.withOneShotContext() { context in
                let z = zettel(context: context, title: "Z1")
                let now = Timekeeper().now()
                save(context)

                try learn(context: context, zettel: z, at: now, outcome: outcome)
                
                let request: NSFetchRequest<FirstReview> = FirstReview.fetchRequest()
                request.predicate = nil
                request.returnsObjectsAsFaults = false
                let fr = fetch(context, request)[0]
                XCTAssertEqual(fr.at, now)
                XCTAssertEqual(fr.zid!, z.zid!)
                XCTAssertEqual(fr.zettel!, z)
            }
        }
    }
    
    func testFirstReviewLearnedGeneratesExpectedReviewEvent() {
        FullModelFixture.withOneShotContext() { context in
            let z = zettel(context: context, title: "Z1")
            save(context)

            let now = Timekeeper().now()
            try learn(context: context, zettel: z, at: now, outcome: .LEARNED)
            
            let request: NSFetchRequest<ReviewEvent> = ReviewEvent.fetchRequest()
            request.predicate = nil
            request.returnsObjectsAsFaults = false
            let re = fetch(context, request)[0]
            
            XCTAssertEqual(re.zid, z.zid!)
            XCTAssertEqual(re.lastReviewTime, now)
            XCTAssertEqual(re.nextReviewTime, now + leitnerBins[0])
            XCTAssertEqual(re.outcome, LearningStatus.LEARNED.asDouble())
            XCTAssertEqual(re.zettel!, z)
            XCTAssertEqual(re.leitnerBin, 0)
        }
    }

    func testFirstReviewNotLearnedGeneratesExpectedReviewEvent() {
        FullModelFixture.withOneShotContext() { context in
            let z = zettel(context: context, title: "Z1")
            save(context)

            let now = Timekeeper().now()
            try learn(context: context, zettel: z, at: now, outcome: .NOT_LEARNED)
            
            let request: NSFetchRequest<ReviewEvent> = ReviewEvent.fetchRequest()
            request.predicate = nil
            request.returnsObjectsAsFaults = false
            let re = fetch(context, request)[0]
            XCTAssertEqual(re.zid, z.zid!)
            XCTAssertEqual(re.lastReviewTime, now)
            XCTAssertEqual(re.nextReviewTime, now)
            XCTAssertEqual(re.zettel!, z)
            XCTAssertEqual(re.outcome, LearningStatus.NOT_LEARNED.asDouble())
            XCTAssertEqual(re.leitnerBin, 0)
        }
    }

    func testGetLastReviewGetsTheMostRecentReview() {
        FullModelFixture.withOneShotContext() { context in
            let z = zettel(context: context, title: "Z1")
            let now = Timekeeper().now()
            try learn(context: context, zettel: z, at: now, outcome: .LEARNED)
            try learn(context: context, zettel: z, at: now, outcome: .LEARNED)
            save(context)
            
            try learn(context: context, zettel: z, at: now + 1, outcome: .NOT_LEARNED)
            let re = try z.latestReviewEvent()!
            XCTAssertEqual(re.zid, z.zid!)
            XCTAssertEqual(re.lastReviewTime, now + 1)
            XCTAssertEqual(re.nextReviewTime, now + 1)
            XCTAssertEqual(re.zettel!, z)
            XCTAssertEqual(re.outcome, LearningStatus.NOT_LEARNED.asDouble())
            XCTAssertEqual(re.leitnerBin, 0)
        }
    }

    func testFailureFromFirstBinDoesNotUnderflow() {
        FullModelFixture.withOneShotContext() { context in
            let z = zettel(context: context, title: "Z1")
            let now = Timekeeper().now()
            try learn(context: context, zettel: z, at: now, outcome: .NOT_LEARNED)
            save(context)

            try learn(context: context, zettel: z, at: now + 1, outcome: .NOT_LEARNED)
            let re = try z.latestReviewEvent()!
            XCTAssertEqual(re.zid, z.zid!)
            XCTAssertEqual(re.lastReviewTime, now + 1)
            XCTAssertEqual(re.nextReviewTime, now + 1)
            XCTAssertEqual(re.zettel!, z)
            XCTAssertEqual(re.outcome, LearningStatus.NOT_LEARNED.asDouble())
            XCTAssertEqual(re.leitnerBin, 0)
        }
    }
    
    func testVerifyStateChanges() {
        struct Action {
            let at: Timestamp
            let outcome: LearningStatus
            let nextReview: Timestamp
            let bin: Int
        }

        let schedule: Array<Timestamp> = [4, 16, 64]
        let testRuns: Array<Array<Action>> = [
            [
                Action(
                    at: Timestamp(100),
                    outcome: .LEARNED,
                    nextReview: 100 + 4,
                    bin: 0),
                Action(
                    at: Timestamp(105),
                    outcome: .LEARNED,
                    nextReview: 105 + 16,
                    bin: 1),
            ], [
                Action(
                    at: Timestamp(100),
                    outcome: .LEARNED,
                    nextReview: 100 + 4,
                    bin: 0),
                Action(
                    at: Timestamp(105),
                    outcome: .LEARNED,
                    nextReview: 105 + 16,
                    bin: 1),
                Action(
                    at: Timestamp(122),
                    outcome: .LEARNED,
                    nextReview: 122 + 64,
                    bin: 2),
                Action(
                    at: Timestamp(186),
                    outcome: .LEARNED,
                    nextReview: 186 + 64,
                    bin: 2),
            ], [
                Action(
                    at: Timestamp(100),
                    outcome: .LEARNED,
                    nextReview: 100 + 4,
                    bin: 0),
                Action(
                    at: Timestamp(105),
                    outcome: .LEARNED,
                    nextReview: 105 + 16,
                    bin: 1),
                Action(
                    at: Timestamp(122),
                    outcome: .LEARNED,
                    nextReview: 122 + 64,
                    bin: 2),
                Action(
                    at: Timestamp(186),
                    outcome: .NOT_LEARNED,
                    nextReview: 186,
                    bin: 1),
                Action(
                    at: Timestamp(187),
                    outcome: .NOT_LEARNED,
                    nextReview: 187,
                    bin: 0),
            ], [
                Action(
                    at: Timestamp(100),
                    outcome: .LEARNED,
                    nextReview: 100 + 4,
                    bin: 0),
                Action(
                    at: Timestamp(105),
                    outcome: .LEARNED,
                    nextReview: 105 + 16,
                    bin: 1),
                Action(
                    at: Timestamp(122),
                    outcome: .NOT_LEARNED,
                    nextReview: 122,
                    bin: 0),
            ], [
                Action(
                    at: Timestamp(100),
                    outcome: .LEARNED,
                    nextReview: 100 + 4,
                    bin: 0),
                Action(
                    at: Timestamp(105),
                    outcome: .LEARNED,
                    nextReview: 105 + 16,
                    bin: 1),
                Action(
                    at: Timestamp(122),
                    outcome: .NOT_LEARNED,
                    nextReview: 122,
                    bin: 0),
                Action(
                    at: Timestamp(123),
                    outcome: .LEARNED,
                    nextReview: 123 + 4,
                    bin: 0),
            ], [
                Action(
                    at: Timestamp(100),
                    outcome: .NOT_LEARNED,
                    nextReview: 100,
                    bin: 0),
                Action(
                    at: Timestamp(101),
                    outcome: .NOT_LEARNED,
                    nextReview: 101,
                    bin: 0),
            ], [
                Action(
                    at: Timestamp(100),
                    outcome: .NOT_LEARNED,
                    nextReview: 100,
                    bin: 0),
                Action(
                    at: Timestamp(101),
                    outcome: .LEARNED,
                    nextReview: 101 + 4,
                    bin: 0),
            ]
        ]
        for testRun in testRuns {
            FullModelFixture.withOneShotContext() { context in
                let z = zettel(context: context, title: "Z1")
                save(context)
                for action in testRun {
                    try learn(schedule: schedule, context: context, zettel: z, at: action.at, outcome: action.outcome)
                    save(context)
                    let event = try z.latestReviewEvent()!
                    XCTAssertEqual(event.nextReviewTime, action.nextReview)
                    XCTAssertEqual(event.leitnerBin, action.bin)
                }
            }
        }
    }

    func testVerifyBinChanges() {
        // start with base
        // increment -- creates
    }
}
