//
//  ReviewSummaryTests.swift
//  zkTests
//
//  Created by Jeff Younker on 4/3/20.
//  Copyright © 2020 The Blobshop. All rights reserved.
//

import XCTest
import CoreData
@testable import zk

class ReviewSummaryTests: XCTestCase {
    struct ZettelFixture {
        let title: String
        let tags: Array<String>
    }

    func testNewUntaggedZettelNotIncludedInSummary() {
        FullModelFixture.withOneShotContext() { c in  // context
            _ = zettel(context: c, title: "z1")
            save(c)
            let now = Timestamp(100)
            let summary = try ReviewEngine.reviewSummary(context: c, at: now)
            let expected = ReviewSummary(new: 0, learning: 0, learned: 0)
            XCTAssertTrue(summary == expected)
        }
    }

    func testNewReviewZettelIncludedInSummary() {
        FullModelFixture.withOneShotContext() { c in  // context
            _ = zettel(context: c, title: "z1", tags: ["review"])
            save(c)
            let now = Timestamp(100)
            let summary = try ReviewEngine.reviewSummary(context: c, at: now)
            let expected = ReviewSummary(new: 1, learning: 0, learned: 0)
            XCTAssertTrue(summary == expected)
        }
    }

    func testNewReviewZettelnAreDistinctInSummary() {
        FullModelFixture.withOneShotContext() { c in  // context
            _ = zettel(context: c, title: "z1", tags: ["review"])
            _ = zettel(context: c, title: "z2", tags: ["review"])
            save(c)
            let now = Timestamp(100)
            let summary = try ReviewEngine.reviewSummary(context: c, at: now)
            let expected = ReviewSummary(new: 2, learning: 0, learned: 0)
            XCTAssertTrue(summary == expected)
        }
    }

    func testUntaggedDoNotShowUpAsLearning() {
        FullModelFixture.withOneShotContext() { c in  // context
            _ = zettel(context: c, title: "z1")
            save(c)
            let now = Timestamp(100)
            let reviewTimes = try ReviewEngine.selectLearning(context: c, at: now)
            XCTAssertEqual(reviewTimes.count, 0)
        }
    }
    
    func testSelectNewFindsNew() {
        struct TestCase {
            let zetteln: Array<ZettelFixture>
            let count: Int
        }
        let testCases = [
            TestCase(
                zetteln: [ZettelFixture(title:"z1", tags:[])],
                count: 0),
            TestCase(
                zetteln: [ZettelFixture(title:"z1", tags:["review"])],
                count: 1),
            TestCase(
                zetteln: [
                    ZettelFixture(title:"z1", tags:["review"]),
                    ZettelFixture(title:"z2", tags:["review"]),
                ],
                count: 2),
            TestCase(
                zetteln: [
                    ZettelFixture(title:"z1", tags:["review"]),
                    ZettelFixture(title:"z2", tags:[]),
                ],
                count: 1),
        ]
        for tc in testCases {
            FullModelFixture.withOneShotContext() { c in  // context
                for zf in tc.zetteln {
                    _ = zettel(context: c, title: zf.title, tags: zf.tags)
                }
                save(c)
                let reviewTimes = try ReviewEngine.selectNew(context: c)
                XCTAssertEqual(reviewTimes.count, tc.count)
            }
        }
    }

    func testSelectLearning() {
        let schedule = [Timestamp(2), Timestamp(4)]
        struct TestCase {
            let zetteln: Array<LearningFixture>
            let testAt: Timestamp
        }
        struct LearningFixture {
            let zettel: ZettelFixture
            let reviews: Array<ReviewEventFixture>
            let readyAt: Timestamp?
        }
        struct ReviewEventFixture {
            let when: Timestamp
            let outcome: LearningStatus
        }
        let testCases = [
            TestCase(
                zetteln: [
                    LearningFixture(  // Not reviewable, and never reviewed.
                        zettel: ZettelFixture(title: "z1", tags: []),
                        reviews: [],
                        readyAt: nil)],
                testAt: 100),
            TestCase(
                zetteln: [
                    LearningFixture(  // Reviewable, but never reviewed
                        zettel: ZettelFixture(title: "z1", tags: ["review"]),
                        reviews: [],
                        readyAt: nil)],
                testAt: 100),
            TestCase(
                zetteln: [
                    LearningFixture(  // Reviewed once, but not reviewable now.
                        zettel: ZettelFixture(title: "z1", tags: []),
                        reviews: [ReviewEventFixture(when: Timestamp(80), outcome: .LEARNED)],
                        readyAt: nil)],
                testAt: 100),
            TestCase(
                zetteln: [
                    LearningFixture(  // Reviewable, and reviewed once.
                        zettel: ZettelFixture(title: "z1", tags: ["review"]),
                        reviews: [ReviewEventFixture(when: Timestamp(80), outcome: .LEARNED)],
                        readyAt: 82)],
                testAt: 100),
            TestCase( // Ensure we pick up the latest review by time.
                zetteln: [
                    LearningFixture(  // Reviewable, and reviewed twice.
                        zettel: ZettelFixture(title: "z1", tags: ["review"]),
                        reviews: [
                            ReviewEventFixture(when: Timestamp(80), outcome: .LEARNED),
                            ReviewEventFixture(when: Timestamp(82), outcome: .LEARNED)],
                        readyAt: 86)],
                testAt: 100),
            TestCase( // Ensure we pick up the latest review by time with no failure.
                zetteln: [
                    LearningFixture(  // Reviewable, and reviewed twice.
                        zettel: ZettelFixture(title: "z1", tags: ["review"]),
                        reviews: [
                            ReviewEventFixture(when: Timestamp(80), outcome: .LEARNED),
                            ReviewEventFixture(when: Timestamp(82), outcome: .NOT_LEARNED)],
                        readyAt: 82)],
                testAt: 100),
            TestCase( // If there is more than one reviewable and reviewed, then we get all.
                zetteln: [
                    LearningFixture(  // Reviewable, and reviewed once.
                        zettel: ZettelFixture(title: "z1", tags: ["review"]),
                        reviews: [ReviewEventFixture(when: Timestamp(80), outcome: .LEARNED)],
                        readyAt: 82),
                    LearningFixture(  // Reviewable, and reviewed once.
                        zettel: ZettelFixture(title: "z2", tags: ["review"]),
                        reviews: [ReviewEventFixture(when: Timestamp(84), outcome: .LEARNED)],
                        readyAt: 86),
                ],
                testAt: 100),
            TestCase( // With more than once instance we only pick up the reviewable once.
                zetteln: [
                    LearningFixture(  // Not reviewable, and never reviewed.
                        zettel: ZettelFixture(title: "z1", tags: []),
                        reviews: [],
                        readyAt: nil),
                    LearningFixture(  // Reviewable, and reviewed once.
                        zettel: ZettelFixture(title: "z2", tags: ["review"]),
                        reviews: [ReviewEventFixture(when: Timestamp(84), outcome: .LEARNED)],
                        readyAt: 86),
                ],
                testAt: 100),
            TestCase( // We only pick up zettel reviewable before now.
                zetteln: [
                    LearningFixture(  // Reviewable, and reviewed once.
                        zettel: ZettelFixture(title: "z1", tags: ["review"]),
                        reviews: [ReviewEventFixture(when: Timestamp(80), outcome: .LEARNED)],
                        readyAt: nil),
                    ],
                testAt: 81),
            TestCase( // We pick up zettel which are reviewable now.
                zetteln: [
                    LearningFixture(  // Reviewable, and reviewed once.
                        zettel: ZettelFixture(title: "z1", tags: ["review"]),
                        reviews: [ReviewEventFixture(when: Timestamp(80), outcome: .LEARNED)],
                        readyAt: 82),
                    ],
                testAt: 82),
            TestCase( // If there is more than one reviewable and reviewed, then we get all.
                zetteln: [
                    LearningFixture(  // Reviewable, and reviewed once.
                        zettel: ZettelFixture(title: "z1", tags: ["review"]),
                        reviews: [ReviewEventFixture(when: Timestamp(80), outcome: .LEARNED)],
                        readyAt: 82),
                    LearningFixture(  // Reviewable, and reviewed once.
                        zettel: ZettelFixture(title: "z2", tags: ["review"]),
                        reviews: [ReviewEventFixture(when: Timestamp(84), outcome: .LEARNED)],
                        readyAt: nil),
                ],
                testAt: 83),
        ]
        for tc in testCases {
            FullModelFixture.withOneShotContext() { c in  // context
                var expected: Array<ReviewTime> = []
                for zf in tc.zetteln {
                    let z = zettel(context: c, title: zf.zettel.title)
                    z.tagNames = zf.zettel.tags
                    for re in zf.reviews {
                        try learn(
                            schedule: schedule,
                            context: c,
                            zettel: z,
                            at: re.when,
                            outcome: re.outcome)
                    }
                    if let when = zf.readyAt {
                        expected.append(ReviewTime(at: when, zid: z.zid!))
                    }
                }
                save(c)
                
                let now = Timestamp(tc.testAt)
                let t = try ReviewEngine.selectLearning(context: c, at: now)
                XCTAssertEqual(Set(t), Set(expected))
            }
        }
    }

    func testTotalReviewable() {
        struct TestCase {
            let zetteln: Array<LearningFixture>
            let reviewable: Int
        }
        struct LearningFixture {
            let zettel: ZettelFixture
            let reviews: Array<ReviewEventFixture>
        }
        struct ReviewEventFixture {
            let when: Timestamp
            let outcome: LearningStatus
        }
        let testCases = [
            TestCase(
                zetteln: [
                    LearningFixture(  // Not reviewable, and never reviewed.
                        zettel: ZettelFixture(title: "z1", tags: []),
                        reviews: [])],
                reviewable: 0),
            TestCase(
                zetteln: [
                    LearningFixture(  // Reviewable, and never reviewed.
                        zettel: ZettelFixture(title: "z1", tags: ["review"]),
                        reviews: [])],
                reviewable: 1),
        TestCase(
            zetteln: [
                LearningFixture(  // Reviewable, and reviewed.
                    zettel: ZettelFixture(title: "z1", tags: ["review"]),
                    reviews: [ReviewEventFixture(when: 80, outcome: .LEARNED)])],
            reviewable: 1),
        TestCase(
            zetteln: [
                LearningFixture(  // No longer reviewable, and reviewed.
                    zettel: ZettelFixture(title: "z1", tags: []),
                    reviews: [ReviewEventFixture(when: 80, outcome: .LEARNED)])],
            reviewable: 0),
        TestCase(
            zetteln: [ // One reviewable, and one not.
                LearningFixture(
                    zettel: ZettelFixture(title: "z1", tags: []),
                    reviews: []),
                LearningFixture(  // One reviewable, and one not.
                    zettel: ZettelFixture(title: "z2", tags: ["review"]),
                    reviews: []),
                ],
            reviewable: 1),
        TestCase(
            zetteln: [ // Both reviewable
                LearningFixture(
                    zettel: ZettelFixture(title: "z1", tags: ["review"]),
                    reviews: []),
                LearningFixture(  // One reviewable, and one not.
                    zettel: ZettelFixture(title: "z2", tags: ["review"]),
                    reviews: []),
                ],
            reviewable: 2),
        ]
        for tc in testCases {
            FullModelFixture.withOneShotContext() { c in  // context
                for zf in tc.zetteln {
                    let z = zettel(context: c, title: zf.zettel.title, tags: zf.zettel.tags)
                    for re in zf.reviews {
                        try learn(context: c, zettel: z, at: re.when, outcome: re.outcome)
                    }
                }
                save(c)
                XCTAssertEqual(try ReviewEngine.totalReviewable(context: c), tc.reviewable)
            }
        }
    }

    func testSummary() {
        let s = [Timestamp(2), Timestamp(4)]
        struct TestCase {
            let zetteln: Array<LearningFixture>
            let summaryTime: Timestamp
            let summary: ReviewSummary
        }
        struct LearningFixture {
            let zettel: ZettelFixture
            let reviews: Array<ReviewEventFixture>
        }
        struct ReviewEventFixture {
            let when: Timestamp
            let outcome: LearningStatus
        }
        let testCases = [
            TestCase(
                zetteln: [],
                summaryTime: Timestamp(100),
                summary: ReviewSummary(new: 0, learning: 0, learned: 0)),
            TestCase(
                zetteln: [
                    LearningFixture(
                        zettel: ZettelFixture(title: "z1", tags: []),
                        reviews: []),
                ],
                summaryTime: Timestamp(100),
                summary: ReviewSummary(new: 0, learning: 0, learned: 0)),
            TestCase(
                zetteln: [
                    LearningFixture(
                        zettel: ZettelFixture(title: "z1", tags: ["review"]),
                        reviews: []),
                ],
                summaryTime: Timestamp(100),
                summary: ReviewSummary(new: 1, learning: 0, learned: 0)),
            TestCase(
                zetteln: [
                    LearningFixture(
                        zettel: ZettelFixture(title: "z1", tags: ["review"]),
                        reviews: [ReviewEventFixture(when: Timestamp(80), outcome: .LEARNED)]),
                ],
                summaryTime: Timestamp(100),
                summary: ReviewSummary(new: 0, learning: 1, learned: 0)),
            TestCase(
                zetteln: [
                    LearningFixture(  // Not reviewable, and never reviewed.
                        zettel: ZettelFixture(title: "z1", tags: ["review"]),
                        reviews: [ReviewEventFixture(when: Timestamp(80), outcome: .LEARNED)]),
                ],
                summaryTime: Timestamp(70),
                summary: ReviewSummary(new: 0, learning: 0, learned: 1)),
            TestCase(
                zetteln: [
                    LearningFixture(  // Not reviewable, and never reviewed.
                        zettel: ZettelFixture(title: "z1", tags: ["review"]),
                        reviews: [ReviewEventFixture(when: Timestamp(80), outcome: .LEARNED)]),
                    LearningFixture(  // Not reviewable, and never reviewed.
                        zettel: ZettelFixture(title: "z2", tags: ["review"]),
                        reviews: [ReviewEventFixture(when: Timestamp(100), outcome: .LEARNED)]),
                ],
                summaryTime: Timestamp(90),
                summary: ReviewSummary(new: 0, learning: 1, learned: 1)),
            TestCase(
                zetteln: [
                    LearningFixture(  // Not reviewable, and never reviewed.
                        zettel: ZettelFixture(title: "z1", tags: ["review"]),
                        reviews: [ReviewEventFixture(when: Timestamp(80), outcome: .LEARNED)]),
                    LearningFixture(  // Not reviewable, and never reviewed.
                        zettel: ZettelFixture(title: "z2", tags: ["review"]),
                        reviews: [ReviewEventFixture(when: Timestamp(100), outcome: .LEARNED)]),
                    LearningFixture(  // Not reviewable, and never reviewed.
                        zettel: ZettelFixture(title: "z3", tags: ["review"]),
                        reviews: []),
                ],
                summaryTime: Timestamp(90),
                summary: ReviewSummary(new: 1, learning: 1, learned: 1)),
        ]
        for tc in testCases {
            FullModelFixture.withOneShotContext() { c in  // context
                for zf in tc.zetteln {
                    let z = zettel(context: c, title: zf.zettel.title, tags: zf.zettel.tags)
                    for re in zf.reviews {
                        try learn(schedule: s, context: c, zettel: z, at: re.when, outcome: re.outcome)
                    }
                }
                save(c)
                XCTAssertTrue(
                    try ReviewEngine.reviewSummary(context: c, at: tc.summaryTime) == tc.summary)
            }
        }
    }
    struct ZettelTrial {
        let at: Timestamp
        let outcome: LearningStatus
    }
    
    struct ZettelAndTrials {
        let title: String
        let trials: Array<ZettelTrial>
    }
    
    struct TestCase {
        let zettelnTrials: Array<ZettelAndTrials>
        let expectedSummary: ReviewSummary
    }
}
