//
//  ReviewCardTests.swift
//  zkTests
//
//  Created by Jeff Younker on 1/31/20.
//  Copyright © 2020 The Blobshop. All rights reserved.
//

import XCTest
import CoreData
@testable import zk

class ReviewCardTests: XCTestCase {
    
    func testQandAExtractionWithSentence() {
        struct TestCase {
            let x: String
            let y1: String?
            let y2: String?
        }
        let testCases = [
            TestCase(x: "W1 W2.", y1: nil, y2: nil),
            TestCase(x: "W1 W2.\nA1 A2", y1: "W1 W2.", y2: "A1 A2"),
            TestCase(x: "W1 W2.\nA1 A2\n", y1: "W1 W2.", y2: "A1 A2\n"),
            TestCase(x: "W1 W2.\n\nA1 A2", y1: "W1 W2.", y2: "A1 A2"),
            TestCase(x: "W1 W2.  \nA1 A2", y1: "W1 W2.  ", y2: "A1 A2"),
            TestCase(x: "W1 W2.\nA1 A2\nA3", y1: "W1 W2.", y2: "A1 A2\nA3"),
        ]
        for tc in testCases {
            let ns = NSAttributedString(string: tc.x)
            let (y1, y2) = extractQuestionAndAnswer(ns)
            XCTAssertEqual(y1?.string, tc.y1)
            XCTAssertEqual(y2?.string, tc.y2)
        }
    }
}
