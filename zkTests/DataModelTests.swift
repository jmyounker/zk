//
//  DataModelTests.swift
//  zkTests
//
//  Created by Jeff Younker on 12/19/19.
//  Copyright © 2019 The Blobshop. All rights reserved.
//

import XCTest
import CoreData
@testable import zk

class DataModelTests: XCTestCase {
    var model: FastModelFixture!
    var fullModel: FullModelFixture!
    
    override func setUp() {
        model = FastModelFixture()
        fullModel = FullModelFixture()
    }

    func testCreateAndRecover() {
        // Sanity test showing that the fixture works.
        let context = model.persistentContainer.viewContext
        let z1 = zettel(context: context, path: [1], parent: nil)
        save(context)
        let request: NSFetchRequest<Zettel> = Zettel.fetchRequest()
        request.predicate = NSPredicate(format: "title == %@", z1.title!)
        request.returnsObjectsAsFaults = false
        let result = fetch(context, request)
        XCTAssertEqual(result.count, 1)
        XCTAssertEqual(Set([z1]), Set(result))
    }

    func testSelectZettelUsingTitleQueryOnSingleEntry() {
        let context = model.persistentContainer.viewContext
        let z1 = zettel(context: context, path: [1], parent: nil)
        save(context)
        let query = ZettelQuery(title: z1.title)
        let result = fetch(context, query.zettelFetchRequest())
        XCTAssertEqual(result.count, 1)
        XCTAssertEqual(Set([z1]), Set(result))
    }

    func testQueryZettel() {
        let context = model.persistentContainer.viewContext
        let z1 = zettel(context: context, path: [1], parent: nil)
        let z2 = zettel(context: context, path: [2], parent: nil)
        save(context)
        struct TestCase {
            let query: ZettelQuery
            let want: Set<Zettel>
        }
        let testCases = [
            // No restrictions returns everything.
            TestCase(query: ZettelQuery(), want: Set([z1, z2])),
            // Title restrictions alone work as expected.
            TestCase(query: ZettelQuery(title: "t"), want: Set([z1, z2])),
            TestCase(query: ZettelQuery(title: z1.title), want: Set([z1])),
            TestCase(query: ZettelQuery(title: "x"), want: Set()),
            // Zid restrictions work as expected
            TestCase(query: ZettelQuery(zids: [z1.zid!]), want: Set([z1])),
            TestCase(query: ZettelQuery(zids: [z1.zid!, z2.zid!]), want: Set([z1, z2])),
            // Multiple queries are combined with logical and.
            TestCase(query: ZettelQuery(zids: [z1.zid!, z2.zid!], title: z1.title), want: Set([z1])),
        ]
        for tc in testCases {
            let query = tc.query  // First letter of test titles
            let got = Set(fetch(context, query.zettelFetchRequest()))
            XCTAssertEqual(got, tc.want)
        }
    }

    func testFullText() {
        let context = model.persistentContainer.viewContext
        let z1 = zettel(context: context, path: [1], parent: nil)
        let z2 = zettel(context: context, path: [2], parent: nil)
        // z3 will match z1.title by page contents
        let z3 = zettel(context: context, path: [3], parent: nil)
        let _ = page(context: context, zettel: z1, name: "front", content: " xy ")
        let _ = page(context: context, zettel: z2, name: "back", content: " yz ")
        let _ = page(context: context, zettel: z3, name: "front", content: " t1 ")
        save(context)
        struct TestCase {
            let query: ZettelQuery
            let want: Set<Zettel>
        }
        let testCases = [
            // No restrictions returns everything.
            TestCase(query: ZettelQuery(fullText: "UNMATCHED"), want: Set([])),
            TestCase(query: ZettelQuery(fullText: "t1"), want: Set([z1, z3])),
            TestCase(query: ZettelQuery(fullText: "x"), want: Set([z1])),
            TestCase(query: ZettelQuery(fullText: "y"), want: Set([z1, z2])),
            TestCase(query: ZettelQuery(fullText: "z"), want: Set([z2])),
        ]
        for tc in testCases {
            let query = tc.query  // First letter of test titles
            let got = Set(fetch(context, query.zettelFetchRequest()))
            XCTAssertEqual(got, tc.want)
        }
    }

    func testZettelByParent() {
        let context = model.persistentContainer.viewContext
        let z1 = zettel(context: context, path: [1], parent: nil)
        let z11 = zettel(context: context, path: [1, 1], parent: nil)
        let z12 = zettel(context: context, path: [1, 2], parent: nil)
        let z2 = zettel(context: context, path: [2], parent: nil)
        z1.addToChildren(z11)
        z11.parent_zid = z1.zid!
        z1.addToChildren(z12)
        z12.parent_zid = z1.zid!
        save(context)
        struct TestCase {
            let query: ZettelQuery
            let want: Set<Zettel>
        }
        let testCases = [
            TestCase(query: ZettelQuery(parent: z2.zid), want: Set([])),
            TestCase(query: ZettelQuery(parent: z1.zid), want: Set([z11, z12])),
        ]
        for tc in testCases {
            let query = tc.query  // First letter of test titles
            let got = Set(fetch(context, query.zettelFetchRequest()))
            XCTAssertEqual(got, tc.want)
        }
    }

    func testQueryZettelViaTags() {
        let context = model.persistentContainer.viewContext
        let z1 = zettel(context: context, path: [1], parent: nil)
        let z2 = zettel(context: context, path: [2], parent: nil)
        let z3 = zettel(context: context, path: [3], parent: nil)
        let t1 = tag(context: context, name: "T1")
        let t2 = tag(context: context, name: "T2")
        z1.addToTags(t1)  // has only t1
        z2.addToTags(t2)  // has only t2
        z3.addToTags(t1)  // has both t1 and t2
        z3.addToTags(t2)
        save(context)
        struct TestCase {
            let query: ZettelQuery
            let want: Set<Zettel>
        }
        let testCases = [
            TestCase(query: ZettelQuery(tags: ["T4"]), want: Set([])),
            TestCase(query: ZettelQuery(tags: ["T1"]), want: Set([z1, z3])),
            TestCase(query: ZettelQuery(tags: ["T2"]), want: Set([z2, z3])),
            TestCase(query: ZettelQuery(tags: ["T1", "T2"]), want: Set([z3])),
            // Tags are AND'd with other search criteria.
            TestCase(query: ZettelQuery(tags: ["T1"], title: z1.title), want: Set([z1])),
        ]
        for tc in testCases {
            let query = tc.query  // First letter of test titles
            let got = Set(fetch(context, query.zettelFetchRequest()))
            XCTAssertEqual(got, tc.want)
        }
    }
}

// MARK: - Fixtures

extension DataModelTests {
    /// Create a test zettel.
    ///
    /// - Parameter context: The context containing the zettel.
    /// - Parameter path: The path part of the name. Should be parent path: with the sequence number appended.
    /// - Parameter zettel: The parent zettel. Root zettel have a nil parent.
    /// - Returns: The new, but unsaved, zettel.
    func zettel(context: NSManagedObjectContext, path: Array<Int>, parent: Zettel?) -> Zettel {
        let name = path.map{"\($0)"}.joined()
        let z = Zettel(context: context)
        z.zid = UUID()
        z.title = "t\(name)"
        z.seq = Int64(path[path.count - 1])
        z.parent = parent
        z.parentZid = parent?.zid!
        return z
    }

    /// Create a test zettel.
    ///
    /// - Parameter context: The context containing the zettel.
    /// - Parameter name: The zettel title
    /// - Returns: The new, but unsaved, zettel.
    func zettel(context: NSManagedObjectContext, name: String) -> Zettel {
        let z = Zettel(context: context)
        z.zid = UUID()
        z.title = "\(name)"
        z.seq = 0
        return z
    }

    /// Create a page.
    ///
    /// - Parameter context: The context containing the page.
    /// - Parameter zettel: The zettel containing the page.
    /// - Parameter name: The page's name (front or back).
    /// - Parameter content: The page content.
    /// - Returns: The new, but unsaved, page.
    func page(context: NSManagedObjectContext, zettel: Zettel, name: String, content: String) -> Page {
        let p = Page(context: context)
        p.zid = zettel.zid
        p.name = name
        zettel.addToPages(p)
        p.content_type = ContentType.rtfd.stringValue()
        p.content = content
        return p
    }
    
    /// Create a test tag.
    ///
    /// - Parameter context: The context containing the tag.
    /// - Parameter name: The tag's name.
    /// - Returns: The new, but unsaved, tag.
    func tag(context: NSManagedObjectContext, name: TagName) -> Tag {
        let t = Tag(context: context)
        t.name = name
        return t
    }

    /// Create a ReviewEvent.
    ///
    /// - Parameter context: The context containing the tag.
    /// - Parameter associated: The zettel associated with this event.
    /// - Parameter nextReviewTime: When this event is due for review.
    /// - Returns: The new, but unsaved, ReviewParams
    func reviewEvent(
        context: NSManagedObjectContext,
        associated zettel: Zettel,
        nextReviewTime: Timestamp) -> ReviewEvent {
        let re = ReviewEvent(context: context)
        re.zid = zettel.zid
        return re
    }

}

// MARK: - Categories

extension DataModelTests {

    func testFindCategoryViaTags() {
        FullModelFixture.withOneShotContext { c in
            let p1 = zettel(context: c, name: "p1")
            p1.category = "C1"
            let p1c1 = zettel(context: c, name: "p1c1")
            p1c1.parent = p1
            p1c1.parentZid = p1.zid
            let query = ZettelQuery(tags: ["C1"])
            let req = query.zettelFetchRequest()
            let n = count(c, req)
            XCTAssertEqual(n, 1)
        }
    }
}
// MARK: - Tag

extension DataModelTests {
    func testAddTags() {
        struct TestCase {
            let exist: Set<TagName>
            let start: Set<TagName>
            let set: Set<TagName>
            let want: Set<TagName>
        }
        let testCases = [
            TestCase(exist: [], start: [], set: ["T1"], want: ["T1"]),
            TestCase(exist: ["T1"], start: [], set: ["T1"], want: ["T1"]),
            TestCase(exist: ["T1"], start: [], set: ["T1"], want: ["T1"]),
            TestCase(exist: ["T1"], start: [], set: ["T1", "T2"], want: ["T1", "T2"]),
            TestCase(exist: ["T1"], start: ["T1"], set: [], want: []),
            TestCase(exist: ["T1", "T2"], start: ["T1", "T2"], set: [], want: []),
            TestCase(exist: ["T1", "T2"], start: ["T1", "T2"], set: ["T1"], want: ["T1"]),
            TestCase(exist: ["T1", "T2"], start: ["T1", "T2"], set: ["T1", "T2", "T3"], want: ["T1", "T2", "T3"]),
        ]
        for tc in testCases {
            // Each run gets a new DB.
            let context = model.newContainer().viewContext
            // Create zettel and initial tags.
            let z = zettel(context: context, path: [1], parent: nil)
            _ = tc.exist.map{ tag(context: context, name: $0) }
            save(context)
            z.tagNames = tc.start.map{ $0 }
            save(context)
            z.tagNames = tc.set.map{ $0 }
            save(context)
            XCTAssertEqual(Set((z.tags as! Set<Tag>).map{ $0.name  }), tc.want)
        }
    }

    func testTagCreation() {
        struct TestCase {
            let exist: Set<TagName>
            let start: Set<TagName>?
            let set: Set<TagName>?
            let want: Int
        }
        let testCases = [
            TestCase(exist: [], start: nil, set: nil, want: 0),
            TestCase(exist: ["T1"], start: nil, set: nil, want: 1),
            TestCase(exist: ["T1", "T2"], start: nil, set: nil, want: 2),
            TestCase(exist: [], start: [], set: [], want: 0),
            TestCase(exist: [], start: ["T1"], set: nil, want: 1),
            TestCase(exist: [], start: nil, set: ["T1"], want: 1),
            TestCase(exist: ["T1"], start: nil, set: ["T1"], want: 1),
            TestCase(exist: ["T1"], start: [], set: [], want: 1),
            TestCase(exist: ["T1"], start: nil, set: ["T2"], want: 2),
            TestCase(exist: ["T1"], start: ["T1"], set: ["T2"], want: 2),
            TestCase(exist: ["T1"], start: ["T1"], set: ["T1", "T2"], want: 2),
            TestCase(exist: [], start: ["T1", "T2"], set: [], want: 2),
        ]
        for tc in testCases {
            // Each run gets a new DB.
            let context = model.newContainer().viewContext
            // Create zettel and initial tags.
            let z = zettel(context: context, path: [1], parent: nil)
            _ = tc.exist.map{ tag(context: context, name: $0) }
            save(context)
            if let start = tc.start {
                z.tagNames = start.map{ $0 }
            }
            save(context)
            if let set = tc.set {
                z.tagNames = set.map{ $0 }
            }
            save(context)
            let n = count(context, Tag.fetchRequest() as NSFetchRequest<Tag>)
            XCTAssertEqual(n, tc.want)
        }
    }
}

// MARK: - Review

extension DataModelTests {
    func testNewZettelCount() {
        struct TestCase {
            let zetteln: Array<TestZettel>
            let want: Int
        }
        struct TestZettel {
            let name: String
            let tags: Array<TagName>
            let hasParams: Bool
        }
        let testCases = [
            TestCase(
                zetteln: [],
                want: 0),
            TestCase(
                zetteln: [
                    TestZettel(name: "a", tags: ["review"], hasParams: false),
                ],
                want: 1),
            TestCase(
                zetteln: [
                    TestZettel(name: "a", tags: ["review"], hasParams: true),
                ],
                want: 0),
            TestCase(
                zetteln: [
                    TestZettel(name: "a", tags: ["review"], hasParams: false),
                    TestZettel(name: "b", tags: ["review"], hasParams: true),
                ],
                want: 1),
            TestCase(
                zetteln: [
                    TestZettel(name: "a", tags: ["review"], hasParams: false),
                    TestZettel(name: "b", tags: ["review"], hasParams: false),
                    TestZettel(name: "c", tags: ["review"], hasParams: true),
                ],
                want: 2),
            TestCase(
                zetteln: [
                    TestZettel(name: "a", tags: [], hasParams: false),
                ],
                want: 0),
            TestCase(
                zetteln: [
                    TestZettel(name: "a", tags: ["review", "other"], hasParams: false),
                ],
                want: 1),
        ]
        func testCase(_ tc: TestCase) {
            let context = model.newContainer().viewContext
            for tcz in tc.zetteln {
                let z = zettel(context: context, name: tcz.name)
                z.tagNames = tcz.tags
            }
            save(context)
            XCTAssertEqual(try ReviewEngine.countNew(context: context), tc.want)
        }
        for tc in testCases {
            testCase(tc)
        }
    }
    
    func testNewZettelExclude() {
        struct TestCase {
            let zetteln: Array<String>
            let except: Array<String>
            let want: Int
        }
        let testCases = [
            TestCase(
                zetteln: ["a", "b"],
                except: [],
                want: 2),
            TestCase(
                zetteln: ["a", "b"],
                except: ["a"],
                want: 1),
            TestCase(
                zetteln: ["a", "b"],
                except: ["a", "b"],
                want: 0),
            // Note that test cases for missing excepts which are not in zettel are missing.
            // missing.
        ]
        func testCase(_ tc: TestCase) {
            let context = model.newContainer().viewContext
            var zettelByName = Dictionary<String, Zettel>()
            for name in tc.zetteln {
                let z = zettel(context: context, name: name)
                z.tagNames = ["review"]
                zettelByName[name] = z
            }
            save(context)
            XCTAssertEqual(try ReviewEngine.countNew(context: context), tc.want)
        }
        for tc in testCases {
            testCase(tc)
        }
    }


    func testLearningCount() {
        struct TestCase {
            let time: Timestamp
            let reviewEvents: Array<ReviewEvents>
            let want: Int
        }
        struct ReviewEvents {
            let title: String
            let nextReviewTimes: Array<Timestamp>
        }
        let t = Timestamp(100)
        let testCases = [
            TestCase(
                time: 10,
                reviewEvents: [
                    ReviewEvents(title: "a", nextReviewTimes: [11])
                ],
                want: 1),
            TestCase(
                time: 10,
                reviewEvents: [
                    ReviewEvents(title: "a", nextReviewTimes: [10])
                ],
                want: 1),
            TestCase(
                time: 10,
                reviewEvents: [
                    ReviewEvents(title: "a", nextReviewTimes: [9])
                ],
                want: 0),
            TestCase(
                time: 10,
                reviewEvents: [
                    ReviewEvents(title: "a", nextReviewTimes: [9, 11])
                ],
                want: 1),
            TestCase(
                time: 10,
                reviewEvents: [
                    ReviewEvents(title: "a", nextReviewTimes: [8, 9])
                ],
                want: 0),
            TestCase(
                time: 10,
                reviewEvents: [
                    ReviewEvents(title: "a", nextReviewTimes: [12, 11])
                ],
                want: 1),
            TestCase(
                time: 10,
                reviewEvents: [
                    ReviewEvents(title: "a", nextReviewTimes: [12, 11]),
                    ReviewEvents(title: "b", nextReviewTimes: [12, 11])
                ],
                want: 2),
        ]
        func testCase(_ tc: TestCase) {
            FullModelFixture.withOneShotContext() { (context) in
                for rev in tc.reviewEvents {
                    let z = zettel(context: context, name: rev.title)
                    for nrt in rev.nextReviewTimes {
                        _ = reviewEvent(context: context, associated: z, nextReviewTime: Timestamp(nrt))
                    }
                }
                save(context)
                XCTAssertEqual(try ReviewEngine.countLearning(context: context, at: tc.time), tc.want)
                }
        }
        for tc in testCases {
            testCase(tc)
        }
    }
}

// MARK: - Helpers

func fetch<X>(_ context: NSManagedObjectContext, _ request: NSFetchRequest<X>) -> [X] {
    do {
        return try context.fetch(request)
    } catch {
        XCTFail()
        fatalError()
    }
}

func count<X>(_ context: NSManagedObjectContext, _ request: NSFetchRequest<X>) -> Int {
    do {
        return try context.count(for: request)
    } catch {
        XCTFail()
        fatalError()
    }
}

func save(_ context: NSManagedObjectContext) {
    do {
        try context.save()
    } catch {
        XCTFail()
        fatalError()
    }
}

func randomString(length: Int) -> String {
  let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
  return String((0..<length).map{ _ in letters.randomElement()! })
}
