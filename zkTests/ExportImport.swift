//
//  ExportImport.swift
//  zkTests
//
//  Created by Jeff Younker on 8/21/20.
//  Copyright © 2020 The Blobshop. All rights reserved.
//

import XCTest
import CoreData
import Foundation
@testable import zk

class ExportImportTests: XCTestCase {
    
    func testImportSingleZettel() {
        let now = Timekeeper().now()
        let z1 = Zid()
        FullModelFixture.withOneShotContext() { c in  // context
            let ek = ExportKasten(
                zetteln: [
                    ExportZettel(
                        zid: z1.uuidString,
                        parentZid: nil,
                        seq: 0,
                        createdAt: now,
                        modifiedAt: now + 1,
                        title: "T1",
                        tagNames: [],
                        pages: [
                            PageName.front.rawValue: ExportPage(
                                contentType: ContentType.rtfd.stringValue(),
                                version: PageVersion.v1.stringValue(),
                                content: uuencodedRtfd(string: "T1"),
                                searchable: "T1") ]
                        ),
            ])
            try importZettel(context: c, zetteln: ek.zetteln)
            let z = Zettel.select(context: c, zid: z1)!
            let ez = ek.zetteln[0]
            XCTAssert(equals(zettel: z, exportZettel: ez))
        }
    }

    func testImportLinkedZettel() {
        let now = Timekeeper().now()
        let z1 = Zid()
        let z2 = Zid()
        FullModelFixture.withOneShotContext() { c in  // context
            let ek = ExportKasten(
                zetteln: [
                    ExportZettel(
                        zid: z1.uuidString,
                        parentZid: nil,
                        seq: 0,
                        createdAt: now,
                        modifiedAt: now + 1,
                        title: "T1",
                        tagNames: [],
                        pages: [
                            PageName.front.rawValue: ExportPage(
                                contentType: ContentType.rtfd.stringValue(),
                                version: PageVersion.v1.stringValue(),
                                content: uuencodedRtfd(string: "T1"),
                                searchable: "T1") ]
                    ),
                    ExportZettel(
                        zid: z2.uuidString,
                        parentZid: z1.uuidString,
                        seq: 0,
                        createdAt: now,
                        modifiedAt: now + 1,
                        title: "T2",
                        tagNames: [],
                        pages: [
                            PageName.front.rawValue: ExportPage(
                                contentType: ContentType.rtfd.stringValue(),
                                version: PageVersion.v1.stringValue(),
                                content: uuencodedRtfd(string: "T2"),
                                searchable: "T2") ]
                    ),
                ])
            try importZettel(context: c, zetteln: ek.zetteln)
            let z1 = Zettel.select(context: c, zid: z1)!
            let z2 = Zettel.select(context: c, zid: z2)!
            let ez1 = ek.zetteln[0]
            let ez2 = ek.zetteln[1]
            XCTAssert(equals(zettel: z1, exportZettel: ez1))
            XCTAssert(equals(zettel: z2, exportZettel: ez2))
        }
    }
    
    func testImportExportSingleZettel() {
        let now = Timekeeper().now()
        let z1 = Zid()
        FullModelFixture.withOneShotContext() { c in  // context
            let ek = ExportKasten(
                zetteln: [
                    ExportZettel(
                        zid: z1.uuidString,
                        parentZid: nil,
                        seq: 0,
                        createdAt: now,
                        modifiedAt: now + 1,
                        title: "T1",
                        tagNames: [],
                        pages: [
                            PageName.front.rawValue: ExportPage(
                                contentType: ContentType.rtfd.stringValue(),
                                version: PageVersion.v1.stringValue(),
                                content: uuencodedRtfd(string: "T1"),
                                searchable: "T1") ]
                        ),
            ])
            try importZettel(context: c, zetteln: ek.zetteln)
            let rek = try exportKasten(context: c)
            XCTAssertEqual(ek.zetteln, rek.zetteln)
        }
    }

    func testImportExportLinkedZettel() {
        let now = Timekeeper().now()
        let z1 = Zid()
        let z2 = Zid()
        FullModelFixture.withOneShotContext() { c in  // context
            let ek = ExportKasten(
                zetteln: [
                    ExportZettel(
                        zid: z1.uuidString,
                        parentZid: nil,
                        seq: 0,
                        createdAt: now,
                        modifiedAt: now + 1,
                        title: "T1",
                        tagNames: [],
                        pages: [
                            PageName.front.rawValue: ExportPage(
                                contentType: ContentType.rtfd.stringValue(),
                                version: PageVersion.v1.stringValue(),
                                content: uuencodedRtfd(string: "T1"),
                                searchable: "T1") ]
                    ),
                    ExportZettel(
                        zid: z2.uuidString,
                        parentZid: z1.uuidString,
                        seq: 0,
                        createdAt: now,
                        modifiedAt: now + 1,
                        title: "T2",
                        tagNames: [],
                        pages: [
                            PageName.front.rawValue: ExportPage(
                                contentType: ContentType.rtfd.stringValue(),
                                version: PageVersion.v1.stringValue(),
                                content: uuencodedRtfd(string: "T2"),
                                searchable: "T2") ]
                    ),
                ])
            try importZettel(context: c, zetteln: ek.zetteln)
            let rek = try exportKasten(context: c)
            XCTAssertEqual(Set(ek.zetteln), Set(rek.zetteln))
        }
    }

    func testMostBasicEncoding() {
        let ek = ExportKasten(zetteln: [])
        let ekEncoded = try! JSONEncoder().encode(ek)
        XCTAssertEqual(String(data: ekEncoded, encoding: .utf8)!, "{\"zetteln\":[]}")
    }

    func testMostBasicDecoding() {
        let ekJson = "{\"zetteln\":[]}".data(using: .utf8)!
        let ekEncoded = try! JSONDecoder().decode(ExportKasten.self, from: ekJson)
        XCTAssertEqual(ekEncoded, ExportKasten(zetteln: []))
    }
}



func equals(zettel z: Zettel, exportZettel ez: ExportZettel) -> Bool {
    if z.zid!.uuidString != ez.zid {
        return false
    }
    if ((z.parentZid?.uuidString ?? nil) != ez.parentZid) {
        return false
    }
    if ((z.parent?.zid!.uuidString ?? nil) != ez.parentZid) {
        return false
    }
    if z.seq != ez.seq {
        return false
    }
    if z.createdAt != ez.createdAt {
        return false
    }
    if z.modifiedAt != ez.modifiedAt {
        return false
    }
    if z.title != ez.title {
        return false
    }
    if Set<String>(z.tagNames) != Set<String>(ez.tagNames) {
        return false
    }
    let pagesByName = z.pagesByName
    if Set<String>(pagesByName.keys.map{ $0.rawValue}) != Set<String>(ez.pages.keys) {
        return false
    }
    for (name, ep) in ez.pages {
        let p = pagesByName[PageName(rawValue: name)!]!
        if !equals(page: p, zid: z.zid!, name: name, exportPage: ep) {
            return false
        }
    }
    return true
 }

func equals(page p: Page, zid: Zid, name: String, exportPage ep: ExportPage) -> Bool {
    if p.zid != zid {
        return false
    }
    if p.zettel!.zid != zid {
        return false
    }
    if p.name! != name {
        return false
    }
    if p.contentType.rawValue != ep.contentType {
        return false
    }
    if p.content != ep.searchable {
        return false
    }
    if p.content_enriched != ep.content {
        return false
    }
    return true
}

