//
//  ReviewStateTests.swift
//  zkTests
//
//  Created by Jeff Younker on 1/17/20.
//  Copyright © 2020 The Blobshop. All rights reserved.
//

import XCTest
@testable import zk

class ReviewStateTests: XCTestCase {
    func testReviewState() {
        var rs = Dictionary<ReviewPanelUiState, Int>()
        rs[.noSession] = 1
        rs[.showingAnswer] = 2
        XCTAssertEqual(rs[.noSession]!, 1)
        XCTAssertEqual(rs[.showingAnswer]!, 2)
    }
}
