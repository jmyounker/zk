PRIVACY POLICY
===

The Blobshop does not own your data. You own your data. I don't want
to know what is in your notes. The ZK program uses no globally shared resources.

What data about you that may incidentally be collected (e.g.
through crash dumps) will never be shared and never be sold.

- Jeff Younker